/**融合指挥信息 */

export default {
  data() {
    return {
      port: 14321, // 端口信息
      waitVerifyCallback: false, // 校验是否返回
      verifyResult: false, // 验证结果
      videoCall: false,// 视频呼叫
      scallHandler: null,// 呼叫处理
      name: "100000",
      pwd: "100000",
      ip: "10.39.235.53",
      callNo: "", // 呼叫终端
      callType: "", //呼叫类型 呼叫：call;视频呼叫：videoCall;查看视频:video;发送消息:sendMsg
      sendMsg: "",//发送消息内容
      x: 0,// x轴
      y: 0,// y 轴

    }
  },
  created() {
  },
  components: {
  },
  mounted() {
  },
  beforeDestroy() {
  },
  methods: {

    /**计算大小 */
    calcPosition() {


      this.left = 5120 * OU.info.transform.scaleX * 0.45
      this.top = 1440 * OU.info.transform.scaleX * 0.45

      // console.log('ou-->', OU)

      // this.left = (window.innerWidth - document.body.clientWidth * OU.info.transform.scaleX) / 2
      // this.top = (window.innerHeight - document.body.clientHeight * OU.info.transform.scaleX)

      // const parent = document.getElementById("main-body").parentElement
      // const parentRect = parent.getBoundingClientRect()

      // this.left += parentRect.left * OU.info.transform.scaleX
      // this.left -= parentRect.left

      // this.top += parentRect.top * OU.info.transform.scaleX
      // this.top -= parentRect.top

      this.x = parseInt(this.left)
      this.y = parseInt(this.top)

      console.log('top-y', this.y)
      console.log('left-x', this.x)

    },

    /**呼叫调度 */
    // callNo: String, // 呼叫终端
    // callType: String, //呼叫类型 呼叫：call;视频呼叫：videoCall;查看视频:video;发送消息:sendMsg
    // sendMsg 发送消息内容
    callDispatch(callNo, callType, sendMsg) {
      this.callNo = callNo;
      this.callType = callType;
      this.sendMsg = sendMsg;

      if (!this.callNo || !this.callType) {
        return;
      }

      this.calcPosition()
      /** 初始化插件*/
      this.initPlugin();

    },

    initPlugin() {
      
      let self = this;
      try {
        var wsImpl = window.WebSocket || window.MozWebSocket;
        if (self.port > 65535) {
          self.port = 1024
        }
        window.ws = new wsImpl("ws://localhost:" + self.port);
        console.log(window.ws);
        ws.onopen = function () {
          console.log("ws open");
          ws.onmessage = function (evt) {
            console.log("ws message");
            //回调方法
            self.callbackInfo(evt.data);
          };
          self.sendMethod("VerifyWebSoket", []);
          self.waitVerifyCallback = true;
          setTimeout(function () {
            if (!self.verifyResult) {
              ws.onmessage = null;
              ws.close();
              self.waitVerifyCallback = false;
            }
          }, 1000);
        };
        ws.onclose = function () {
          console.log("ws close");
        };
        ws.onerror = function (event) {
          console.log(event);

        };
      } catch (error) {
        console.error(error);
        return;
      }
    },


    /**
     * 方法回调
     */
    callbackInfo(msg) {
      let self = this;
      console.log('msg--->', msg)
      msg = msg.replace(/\n/g, "");
      let obj = JSON.parse(msg);
      switch (obj.cmd) {
        case "Login":
          if (obj.type == "LoginSuc") { // 登录成功
            /**终端呼叫 */
            // this.$nextTick(() => {
            self.call()
            // })
          } else if (obj.type == "Loginfail") {
          }
          break;

        case "SingleCall":
          break;
        case "Msg":
          
          if (obj.type == "MsgSendResult") {
            if (obj.msg.result === "0" || obj.msg.result === 0) {
              this.sendMsgSuccess()
            } else {
              this.sendMsgFail()
            }
          }
          break;
        case "Wsoket":
          if (obj.type == "WsoketResult") {
            switch (obj.msg.method) {
              case "VerifyWebSoket":
                //验证唯一ID，不变
                if (obj.msg.result === "34c61afbc8b6470f9b349a1e1c2986be") {
                  // $("#connStatus").css("background-color", "green");
                  self.verifyResult = true;
                  self.loginSystem()
                }
                break;
            }
          }
      }
    },

    /**
     * 呼叫终端
     */
    call() {
      let self = this;
      // 呼叫类型 呼叫：call; 视频呼叫：videoCall; 查看视频: video; 发送消息: sendMsg
      if (self.callType === "call") {// 单个呼叫
        /**设置呼叫界面 */
        self.setCallPanelLocation();
        self.singleCall()

      } else if (self.callType === "videoCall") { //视频呼叫
        self.setVideoWindow()
        self.singleVideoCall()

      } else if (self.callType === "video") { //视频查看
        self.setVideoWindow()
        self.showVideByNo()

      } else if (self.callType === "sendMsg") { //发送消息
        self.sendMSGOne()
      }
    },

    /**登录系统 */
    loginSystem() {
      let self = this;
      self.excuteFucOfPlugin("Login", [self.name, self.pwd, self.ip]);
    },

    /**注销登录 */
    destrUct() {
      let self = this;
      self.excuteFucOfPlugin("LoginOut", []);
    },

    /**设置界面模式 */
    setHgjsSurfaceModel() {
      let self = this;
      self.excuteFucOfPlugin("SetSurfaceModel", [1, 0, 1]);
    },

    /**设置呼叫界面 */
    setCallPanelLocation() {
      let self = this;
      self.excuteFucOfPlugin("SetCallPanelLocation", [self.x, self.y]);
    },


    /** 发起单呼*/
    singleCall() {
      let self = this;
      let callNo = self.callNo;
      self.excuteFucOfPlugin("SingleCallByNo", [callNo, 0]);
    },


    /** 设置视频窗口 */
    setVideoWindow() {
      let self = this;
      self.excuteFucOfPlugin("SetVideoWindow", [self.x, self.y, 400, 400]);
    },



    /**发起视频呼叫 */
    singleVideoCall() {
      let self = this;
      let callNo = self.callNo;
      self.excuteFucOfPlugin("SingleCallByNo", [callNo, 1]);

    },

    /**关闭视频呼叫 */
    closeSingleCall() {
      let self = this;
      self.excuteFucOfPlugin("CloseSingleCall", [self.scallHandler]);
      if (videoCall) {
        self.excuteFucOfPlugin("CloseVideoCall", []);
      }
    },

    /**查看视频 */
    showVideByNo() {
      let self = this;
      var callNo = self.callNo;
      self.excuteFucOfPlugin("ShowVideo", [callNo]);
    },

    /**发送消息 */
    sendMSGOne() {
      let self = this;
      let msgType = 0;
      let pcDst = self.callNo;
      let curDate = new Date().getTime();
      var SRCDN = self.name;
      let msgid = curDate + "00_" + pcDst + "_" + SRCDN
      let InfoType = 0;
      let RecvCfm = 0;
      let chMsgContent = self.sendMsg;
      self.excuteFucOfPlugin("SendMSGForV5", [
        msgType,
        pcDst,
        msgid,
        InfoType,
        RecvCfm,
        chMsgContent,
        "",
        0,
      ])
    },

    /**执行方法 */
    excuteFucOfPlugin(method, paras) {
      let self = this;
      if (self.verifyResult && ws.readyState === WebSocket.OPEN) {
        console.log("method:" + method + " paras:" + paras);
        self.sendMethod(method, paras);
      } else {
        console.error(ws.readyState);
      }
    },

    /**
     *发送执行方法
     */
    sendMethod(method, paras) {
      let jobj = {
        method: method,
        paramArray: paras,
      };
      let jStr = JSON.stringify(jobj);
      console.log(jStr);
      ws.send(jStr);
    }


  }
}

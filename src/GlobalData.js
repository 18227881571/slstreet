export default {
    imgUrl: '../src/screen/1600385025259147265/',
    //事件中枢基础地址
    sjzsBaseUrl: 'http://10.39.235.53:31601',
  
    //配置数据加载状态
    loadConfigFinished: true,
    //街道名称
    street: '东升街道',
    //街道配置
    streetConfig: {
      黄龙溪镇: {
        code: '510116005',
        center: [103.9270, 30.5706],
      },
      永安镇: {
        code: '510116112',
        center: [103.9270, 30.5706],
      },
      彭镇: {
        code: '510116108',
        center: [103.9270, 30.5706],
      },
      黄水镇: {
        code: '510116',
        center: [103.9270, 30.5706],
      },
      黄甲街道: {
        code: '510116004',
        center: [103.9270, 30.5706],
      },
      怡心街道: {
        code: '510116111',
        center: [103.9270, 30.5706],
      },
      九江街道: {
        code: '510116107',
        center: [103.9270, 30.5706],
      },
      西航港街道: {
        code: '510116110',
        center: [103.9270, 30.5706],
      },
      东升街道: {
        code: '510116001',
        center: [103.9270, 30.5706],
      },
    },
    //全局Iframe信息
    iframeInfo: {
      url: null,
      width: null,
      height: null,
      transform: ''
    },
    //事件详情
    eventInfo: null,
  
    //融合指挥消息框
    dispatchSendMsgShow: false,
    //融合指挥
    rhzhData: {
      callNo: null, //终端号码,
      callType: null// 终端类型
    },
    streetMapConfig:[
      {
        "name": "东升街道",
        "code": "东升街道",
        "areaCode": "510116001",
        "zxdjd": "103.92912193144383",
        "zxdwd": "30.580902218288873",
        "mapZoom": "5",
        "mapAreaCode": "510116001"
      },
      {
        "name": "西航港街道",
        "code": "西航港街道",
        "areaCode": "510116110",
        "zxdjd": "103.9885961532194",
        "zxdwd": "30.551741408920982",
        "mapZoom": "4",
        "mapAreaCode": "510116002"
      },
      {
        "name": "九江街道",
        "code": "九江街道",
        "areaCode": "510116107",
        "zxdjd": "103.91649335142729",
        "zxdwd": "30.642791558151984",
        "mapZoom": "4",
        "mapAreaCode": "510116005"
      },
      {
        "name": "怡心街道",
        "code": "怡心街道",
        "areaCode": "510116111",
        "zxdjd": "104.0047999933368",
        "zxdwd": "30.48915514815016",
        "mapZoom": "4",
        "mapAreaCode": "510116017"
      },
      {
        "name": "黄甲街道",
        "code": "黄甲街道",
        "areaCode": "510116004",
        "zxdjd": "103.96844852132747",
        "zxdwd": "30.487611925281843",
        "mapZoom": "4",
        "mapAreaCode": "510116006"
      },
      {
        "name": "黄水镇",
        "code": "黄水镇",
        "areaCode": "510116002",
        "zxdjd": "103.88350655189475",
        "zxdwd": "30.526962096420988",
        "mapZoom": "4",
        "mapAreaCode": "510116115"
      },
      {
        "name": "彭镇",
        "code": "彭镇",
        "areaCode": "510116108",
        "zxdjd": "103.84749801830063",
        "zxdwd": "30.587833665115895",
        "mapZoom": "4",
        "mapAreaCode": "510116108"
      },
      {
        "name": "永安镇",
        "code": "永安镇",
        "areaCode": "510116112",
        "zxdjd": "103.95658579071943",
        "zxdwd": "30.419304184503844",
        "mapZoom": "4",
        "mapAreaCode": "510116112"
      },
      {
        "name": "黄龙溪镇",
        "code": "黄龙溪镇",
        "areaCode": "510116005",
        "zxdjd": "103.96876010445833",
        "zxdwd": "30.337170434067595",
        "mapZoom": "4",
        "mapAreaCode": "510116111"
      }
    ],
    showTopWarning:false,
    showZbtx: false,
    showPopup:false,
  }
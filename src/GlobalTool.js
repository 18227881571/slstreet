import GlobalData from './GlobalData'

export default {
  //生成UUID
  guid() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
      const r = (Math.random() * 16) | 0
      const v = c === 'x' ? r : (r & 0x3) | 0x8
      return v.toString(16)
    })
  },
  /**
   * 动态加载css文件
   * @param {*} url
   * @param {*} isCache
   */
  loadCSS(url, isCache = false) {
    const element = document.createElement("link")
    element.setAttribute("rel", "stylesheet")
    element.setAttribute("type", "text/css")

    if (isCache) {
      element.setAttribute("href", url + "?t=" + new Date().getTime())
    } else {
      element.setAttribute("href", url)
    }
    document.head.appendChild(element)
  },
  /**
   * 动态加载js文件
   * @param {*} src
   * @param {*} callback
   *   loadScript("",function(){
   *   console.log("加载成功")
   * })
   * var that = this; 在方法里面使用that
   */
  loadJS(src, isCache = false, callback = null) {
    const script = document.createElement("script")
    script.type = "text/JavaScript"

    if (isCache) {
      script.src = src + "?t=" + new Date().getTime()
    } else {
      script.src = src
    }
    if (script.addEventListener && callback) {
      script.addEventListener("load", callback, false)
    }
    document.head.appendChild(script);
  },
  /**
  * 打开新的窗口
  */
  openWindowAsDialog(name, url, width, height, transform) {
    const swidth = window.screen.availWidth
    const sheight = window.screen.availHeight
    width = width || swidth
    height = height || sheight
    const scale = Math.min(swidth / width, sheight / height)
    width = width * scale
    height = height * scale

    const top = (sheight - height) / 2
    const left = (swidth - width) / 2

    const tmpOption = [
      'width=' + width,
      'height=' + height,
      'top=' + top,
      'left=' + left,
      'resizable=yes',
      'status=yes',
      'menubar=no',
      'scrollbars=yes',
      'location=no',
      'toolbar=no',
      'fullscreen=yes'
    ]
    const newWin = window.top.open(url, name, tmpOption.join(','))
  },
  /**
  * 打开新的窗口
  */
  openWindow(name, url) {
    window.top.open(url, name)
  },
  /**
  * 打开新的窗口
  */
  openIFrame(name, url, width, height, transform) {
    GlobalData.iframeInfo.url = url
    GlobalData.iframeInfo.width = parseInt(width || 0)
    GlobalData.iframeInfo.height = parseInt(height || 0)
    GlobalData.iframeInfo.transform = transform || ''
  },
  /**
   * 处理树形结构数据
   */
  dealTree(tree) {
    tree.forEach(item => {
      if (!item.children || item.children.length <= 0) {
        item.children = null
      } else {
        this.dealTree(item.children)
      }
    })
  },

}
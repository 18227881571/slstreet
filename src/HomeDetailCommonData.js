export default {
    ztgl: [{
      name: '派出所',
      number: 19,
      unit: '个'
    }, {
      name: '警员数',
      number: 31,
      unit: '人'
    }, {
      name: '天网数',
      number: 50,
      unit: '件'
    }, {
      name: '车辆卡口',
      number: 58,
      unit: '个'
    }],
  
    getbjlqschartoption(dataList) {
      const option = {
        tooltip: {
          trigger: 'axis',
          textStyle: {
            fontSize: 18,
          },
          axisPointer: {
            type: 'cross',
            label: {
              backgroundColor: '#6a7985',
              color: '#FFF',
              fontSize: 18,
            },
          },
        },
        legend: {
          top: '1%',
          // left: 'center',
          right: '0',
          itemGap: 30,
          itemHeight: 15,
          itemWidth: 30,
          icon: 'circle',
          // color: ['#29F1FA', '#FF6767','#FAB439'],
          // data: ['总扫码人数','红码','黄码'],
          textStyle: {
            color: '#ffffff',
            fontSize: 18,
            lineHeight: 15,
          },
          lineStyle: {
            width: 5,
          },
        },
        grid: {
          top: '15%',
          left: '0%',
          right: '5%',
          bottom: '3%',
          containLabel: true,
        },
        xAxis: [
          {
            type: 'category',
            // boundaryGap: false,
            // splitNumber: 4,
            data: dataList.map(i => i.name),
            axisLabel: {
              show: true,
              // origin: 10,
              // rotate: 20, // 调整数值改变倾斜的幅度（范围-90到90）
              textStyle: {
                color: '#ffffff',
                fontSize: 18,
                padding: 5,
              },
  
              formatter: function (value) {
                let valueTxt = ''
                if (value.length > 3) {
                  valueTxt = value.substring(0, 3) + '..'
                } else {
                  valueTxt = value
                }
                return valueTxt
              },
            },
            axisLine: {
              lineStyle: {
                color: 'rgba(255,255,255,0.5)',
                width: 2.5,
              },
            },
          },
        ],
        yAxis: [
          {
            type: 'value',
            name: '单位：件  ',
            nameTextStyle: {
              color: "#ffffff",
              lineHeight: 30,
              fontSize: 16,
            },
            splitNumber: 4,
            minInterval: 1,
            splitLine: {
              //网格线
              lineStyle: {
                type: 'dashed', //设置网格线类型 dotted：虚线 solid:实线
                color: 'rgba(102,102,102,0.8)',
                width: 2.5,
              },
              show: true,
            },
            axisLabel: {
              show: true,
              textStyle: {
                color: '#fff',
                fontSize: 18,
              },
            },
          },
        ],
        series: [
          {
            type: 'bar',
            name: '街道办件量',
            // stack: 'Total',
            emphasis: {
              focus: 'series',
            },
            label: {
              show: false,
              position: 'top',
              fontSize: 16,
              color: '#1DCAD4',
            },
            smooth: true,
            symbolSize: 15, //原点大小
            color: '#25A6FF',
            barWidth: 30,
            itemStyle: {
              // 柱条的描边颜色（borderColor）、宽度（borderWidth）、样式（borderType）；
              // borderType: 'solid',
              // borderColor:'#1fe2ff',
              // // 阴影（shadowBlur、shadowColor、shadowOffsetX、shadowOffsetY）。
              // shadowBlur: 10,
              barBorderRadius: [15, 15, 0, 0],
              color: function (params) {
                return new echarts.graphic.LinearGradient(0, 0, 0, 1, [
                  {
                    offset: 0,
                    color: '#0042D8',
                  },
                  {
                    offset: 1,
                    color: '#25A6FF',
                  },
                ])
              },
            },
            data: dataList,
          }
        ],
      }
      return option
    },
  
  
  
    // 近期办结量
    get_nearNumberoption(dataList) {
      const option = {
        tooltip: {
          trigger: 'axis',
          textStyle: {
            fontSize: 18,
          },
          axisPointer: {
            type: 'cross',
            label: {
              backgroundColor: '#6a7985',
              color: '#FFF',
              fontSize: 18,
            },
          },
        },
        legend: {
          top: '1%',
          // left: 'center',
          right: '0',
          itemGap: 30,
          itemHeight: 15,
          itemWidth: 30,
          // color: ['#29F1FA', '#FF6767','#FAB439'],
          // data: ['总扫码人数','红码','黄码'],
          textStyle: {
            color: '#ffffff',
            fontSize: 18,
            lineHeight: 15,
          },
          lineStyle: {
            width: 5,
          },
        },
        grid: {
          top: '15%',
          left: '1%',
          right: '5%',
          bottom: '3%',
          containLabel: true,
        },
        xAxis: [
          {
            type: 'category',
            boundaryGap: false,
            data: dataList.map(i => i.name),
            axisLabel: {
              show: true,
              textStyle: {
                color: '#ffffff',
                fontSize: 18,
                padding: 5,
              },
            },
            axisLine: {
              lineStyle: {
                color: 'rgba(255,255,255,0.5)',
                width: 2.5,
              },
            },
          },
        ],
        yAxis: [
          {
            type: 'value',
            name: '数量/件  ',
            nameTextStyle: {
              color: "#ffffff",
              lineHeight: 30,
              fontSize: 16,
            },
            splitNumber: 4,
            minInterval: 1,
            splitLine: {
              //网格线
              lineStyle: {
                type: 'dashed', //设置网格线类型 dotted：虚线 solid:实线
                color: 'rgba(102,102,102,0.8)',
                width: 2.5,
              },
              show: true,
            },
            axisLabel: {
              show: true,
              textStyle: {
                color: '#fff',
                fontSize: 18,
              },
            },
          },
        ],
        series: [
          {
            type: 'line',
            name: '办件量',
            nameTextStyle: {
              color: "#ffffff",
              lineHeight: 30,
              fontSize: 16,
            },
            // stack: 'Total',
            emphasis: {
              focus: 'series',
            },
            label: {
              show: false,
              position: 'top',
              fontSize: 18,
              color: '#1DCAD4',
            },
            smooth: true,
            symbolSize: 15, //原点大小
            color: '#00FFB4',
            // lineStyle: {
            //   width: 4,
            //   color: '#30EDF5',
            // },
            itemStyle: {
              normal: {
                color: '#00FFB4', // 原点颜色
                lineStyle: {
                  color: '#00FFB4', //折线颜色
                  width: 5, // 0.1的线条是非常细的了
                },
              },
              borderColor: '#00FFB4',
              borderWidth: 3,
            },
            areaStyle: {
              //区域填充样式
              normal: {
                //线性渐变，前4个参数分别是x0,y0,x2,y2(范围0~1);相当于图形包围盒中的百分比。如果最后一个参数是‘true’，则该四个值是绝对像素位置。
                color: new echarts.graphic.LinearGradient(
                  0,
                  0,
                  0,
                  1,
                  [
                    {
                      offset: 0,
                      color: 'rgba(0, 255, 180, 0.5)',
                    },
                    {
                      offset: 1,
                      color: 'rgba(0,255,255,0)',
                    },
                  ],
                  false,
                ),
                shadowColor: 'rgba(0,255,255,0.5)', //阴影颜色
              },
            },
            data: dataList,
          }
        ],
      }
      return option
    },
  
  
  
    // 业务类型统计
    get_BusinessTypeStatisticalOption(dataList) {
  
      const color = ['#42CC7C', '#858BFF', '#F56767', '#FFCB2C', '#2C97FF', '#179B4F', '#6853AB', '#B01B1B', '#B1880A', '#0C57A0', '#87B77E', '#D297FF', '#FFB78B', '#FFF600', '#2CFFFD', '#B6DD00', '#FF85E1', '#FF8942', '#EAFF00', '#42FF96']
      const option = {
        tooltip: {
          trigger: 'item',
          formatter: '{a} <br/>{b}:{c}件  占比{d}%',
        },
        legend: {
          orient: 'vertical',
          right: '10%',
          top: '25%',
          textStyle: {
            color: '#ffffff',
            fontSize: 18,
          }
        },
        series: [{
          name: '办件类型',
          type: 'pie',
          radius: '50%',
          radius: ['40%', '70%'],
          center: ['30%', '50%'],
          color: color,
          data: dataList,
          label: {
            // formatter: '{a} <br/>{b}:{c}人 占比({d}%)'
            formatter: (params) => {
              const id = params.dataIndex + 1
              if (params.name !== '') {
                return `${params.name}${params.percent === undefined ? 0 : params.percent}%`
                // return `{icon${id}|${params.name}${params.percent === undefined ? 0 : params.percent}+'%'}`
              }
              return ''
            },
            // rich: {
            //   icon1: {
            //     width: 35,
            //     height: 15,
            //     align: 'center',
            //     color: '#FA6400',
            //     fontSize: 18,
            //     // backgroundColor: {
            //     //   image: img1,
            //     // },
            //   },
            //   icon2: {
            //     width: 35,
            //     height: 15,
            //     align: 'center',
            //     color: '#FFB000',
            //     fontSize: 18,
            //     // backgroundColor: {
            //     //   image: img2,
            //     // },
            //   },
            //   icon3: {
            //     width: 35,
            //     height: 15,
            //     align: 'center',
            //     color: '#337EFF',
            //     fontSize: 18,
            //     // backgroundColor: {
            //     //   image: img3,
            //     // },
            //   },
            // },
            textStyle: {
              fontSize: 18,
              color: '#FFFFFF'
            },
  
          },
          emphasis: {
            itemStyle: {
              shadowBlur: 10,
              shadowOffsetX: 0,
              shadowColor: 'rgba(0, 0, 0, 0.5)'
            }
          }
        }]
      }
      return option
    },
  }
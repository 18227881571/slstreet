export default {
    ztgl: [{
      name: '当日',
      number: 0,
      unit: '件'
    }, {
      name: '本周',
      number: 0,
      unit: '件'
    }, {
      name: '本月',
      number: 0,
      unit: '件'
    }],
  
  
    fwqk: [{
      name: '街道办理',
      list: [{
        name: '转办量',
        number: 0,
        unit: '件'
      }, {
        name: '紧急件',
        number: 0,
        unit: '件'
      }, {
        name: '平均回复周期',
        number: 0,
        unit: '天'
      }]
    }, {
      name: '部门办理',
      list: [{
        name: '转办量',
        number: 0,
        unit: '件'
      }, {
        name: '紧急件',
        number: 0,
        unit: '件'
      }, {
        name: '平均回复周期',
        number: 0,
        unit: '天'
      }]
    }, {
      name: '平台公司办理',
      list: [{
        name: '转办量',
        number: 0,
        unit: '件'
      }, {
        name: '紧急件',
        number: 0,
        unit: '件'
      }, {
        name: '平均回复周期',
        number: 0,
        unit: '天'
      }]
    }],
    // 近六月诉求趋势
    getsqqschartoption(dataList) {
      const option = {
        tooltip: {
          trigger: 'axis',
          textStyle: {
            fontSize: 18,
          },
          axisPointer: {
            type: 'cross',
            label: {
              backgroundColor: '#6a7985',
              color: '#FFF',
              fontSize: 18,
            },
          },
        },
        legend: {
          top: '1%',
          // left: 'center',
          right: '0',
          itemGap: 30,
          itemHeight: 15,
          itemWidth: 30,
          // color: ['#29F1FA', '#FF6767','#FAB439'],
          // data: ['总扫码人数','红码','黄码'],
          textStyle: {
            color: '#ffffff',
            fontSize: 18,
            lineHeight: 15,
          },
          lineStyle: {
            width: 5,
          },
        },
        grid: {
          top: '15%',
          left: '3%',
          right: '5%',
          bottom: '3%',
          containLabel: true,
        },
        xAxis: [
          {
            type: 'category',
            boundaryGap: false,
            // splitNumber: 5,
            data: dataList.map(i => i.name),
            axisLabel: {
              show: true,
              textStyle: {
                color: '#ffffff',
                fontSize: 14,
                padding: 10,
              },
              // rotate: 18, // 调整数值改变倾斜的幅度（范围-90到90）
  
            },
            axisLine: {
              lineStyle: {
                color: 'rgba(255,255,255,0.5)',
                width: 1.5,
              },
            },
          },
        ],
        yAxis: [
          {
            type: 'value',
            name: '',
            nameTextStyle: {
              color: "#ffffff",
              lineHeight: 30,
              fontSize: 16,
            },
            splitNumber: 4,
            minInterval: 1,
            splitLine: {
              //网格线
              lineStyle: {
                type: 'dashed', //设置网格线类型 dotted：虚线 solid:实线
                color: 'rgba(102,102,102,0.8)',
                width: 2.5,
              },
              show: true,
            },
            axisLabel: {
              show: true,
              textStyle: {
                color: '#fff',
                fontSize: 18,
              },
            },
          },
        ],
        series: [
          {
            type: 'line',
            name: '受理量',
            // stack: 'Total',
            emphasis: {
              focus: 'series',
            },
            label: {
              show: false,
              position: 'top',
              fontSize: 18,
              color: '#1DCAD4',
            },
            smooth: true,
            symbolSize: 15, //原点大小
            color: '#00FFB4',
            // lineStyle: {
            //   width: 4,
            //   color: '#30EDF5',
            // },
            itemStyle: {
              normal: {
                color: '#00FFB4', // 原点颜色
                lineStyle: {
                  color: '#00FFB4', //折线颜色
                  width: 5, // 0.1的线条是非常细的了
                },
              },
              borderColor: '#00FFB4',
              borderWidth: 3,
            },
            areaStyle: {
              //区域填充样式
              normal: {
                //线性渐变，前4个参数分别是x0,y0,x2,y2(范围0~1);相当于图形包围盒中的百分比。如果最后一个参数是‘true’，则该四个值是绝对像素位置。
                color: new echarts.graphic.LinearGradient(
                  0,
                  0,
                  0,
                  1,
                  [
                    {
                      offset: 0,
                      color: 'rgba(0, 255, 180, 0.5)',
                    },
                    {
                      offset: 1,
                      color: 'rgba(0,255,255,0)',
                    },
                  ],
                  false,
                ),
                shadowColor: 'rgba(0,255,255,0.5)', //阴影颜色
              },
            },
            data: dataList,
          }
        ],
      }
      return option
    },
    // 本月诉求热点类型
    getbysqrdlxchartoption(dataList) {
  
      const color = ['#42CC7C', '#858BFF', '#F56767', '#FFCB2C', '#2C97FF', '#179B4F', '#6853AB', '#B01B1B', '#B1880A', '#0C57A0', '#87B77E', '#D297FF', '#FFB78B', '#FFF600', '#2CFFFD', '#B6DD00', '#FF85E1', '#FF8942', '#EAFF00', '#42FF96']
      const option = {
        tooltip: {
          trigger: 'item',
          // 占比({d}%) {a} <br/>
          formatter: '{b} {c}件 占比{d}%',
        },
        legend: {
          orient: 'vertical',
          right: '10%',
          top: '25%',
          textStyle: {
            color: '#ffffff',
            fontSize: 18,
          },
  
        },
        series: [{
          type: 'pie',
          radius: '50%',
          radius: ['40%', '70%'],
          center: ['30%', '50%'],
          color: color,
          data: dataList,
          label: {
            // 占比({d}%)
            formatter: '{b} {c}件 ',
            // formatter: (params) => {
            //   const id = params.dataIndex + 1
            //   if (params.name !== '') {
            //     // return `${params.name}${params.percent === undefined ? 0 : params.percent}%`
            //     // return `{icon${id}|${params.name}${params.percent === undefined ? 0 : params.percent}+'%'}`
            //   }
            //   return ''
            // },
            // rich: {
            //   icon1: {
            //     width: 35,
            //     height: 15,
            //     align: 'center',
            //     color: '#FA6400',
            //     fontSize: 18,
            //     // backgroundColor: {
            //     //   image: img1,
            //     // },
            //   },
            //   icon2: {
            //     width: 35,
            //     height: 15,
            //     align: 'center',
            //     color: '#FFB000',
            //     fontSize: 18,
            //     // backgroundColor: {
            //     //   image: img2,
            //     // },
            //   },
            //   icon3: {
            //     width: 35,
            //     height: 15,
            //     align: 'center',
            //     color: '#337EFF',
            //     fontSize: 18,
            //     // backgroundColor: {
            //     //   image: img3,
            //     // },
            //   },
            // },
            textStyle: {
              fontSize: 18,
              color: '#FFFFFF'
            },
          },
          emphasis: {
            itemStyle: {
              shadowBlur: 10,
              shadowOffsetX: 0,
              shadowColor: 'rgba(0, 0, 0, 0.5)'
            }
          }
        }]
      }
      return option
    },
    // 重点关注
    getzdgzchartoption(dataList) {
      const color1 = ['#000000', '#000000', '#000000', '#000000', '#000000']
      const color2 = ['#FF6050', '#F7DC71', '#25A6FF', '#71F797', '#7181F7']
      const yData = dataList.map(item => item.value)
      const max = Math.max.apply(null, yData) || 100
      const bgData = []
      for (let i in yData) {
        // bgData.push(dataList[0].value)
        bgData.push(max)
      }
      const option = {
        tooltip: {
          trigger: 'axis',
          // axisPointer: {
          //   type: 'shadow',
          // },
          // '{a} <br/>{b}:{c}台 占比({d}%)'
          textStyle: {
            fontSize: 18,
          },
          axisPointer: {
            type: 'shadow',
            // axios: 'y',
            label: {
              backgroundColor: '#6a7985',
              color: '#FFF',
              fontSize: 18,
            },
          },
          // formatter: '{a}: {b}<br />{b1}: {c}'
          formatter: (val, vol) => {
            return val[0].name + '：' + val[0].value + '件'
            // debugger
            // return list[parseInt(val[0].axisValue)].name + '：' + list[parseInt(val[0].axisValue)].value + '人'
          },
        },
        grid: {
          top: '5%',
          left: '0%',
          right: '3%',
          bottom: '0%',
          containLabel: true,
        },
        xAxis: {
          type: 'value',
          show: false,
          boundaryGap: [0, 0.01],
        },
        yAxis: [
          {
            // //名称
            type: 'category',
            // offset: -20,
            position: 'left',
            inverse: true,
            data: dataList.map(item => item.name),
            axisLabel: {
              show: true,
              inside: true,
              lineHeight: 50,
              padding: [0, 10, 10, 0],
              verticalAlign: 'bottom',
              textStyle: {
                color: '#ffffff',
                fontSize: '14',
                fontWeight: 'bolder',
              },
              formatter: (name, index) => {
                return `{name|${name}}`
                // const id = index + 1
                // if (id < 4) {
                //   return `{name|${name}}`
                // } else {
                //   return `{name|${name}}`
                // }
              },
              rich: {
                name: {
                  width: 150,
                  fontSize: 18,
                  // align: 'left',
                  color: '#ffffff',
                  fontFamily: 'Source Han Sans CN',
                  fontWeight: 'bolder',
                  lineHeight: 30,
                },
              },
            },
            splitLine: {
              show: false,
            },
            axisTick: {
              show: false,
            },
            axisLine: {
              show: false,
            },
          },
        ],
        series: [
          {
            name: '2011',
            type: 'bar',
            barWidth: 14,
            zlevel: 1,
            // data: list.map(item => item.value),
            data: dataList,
            itemStyle: {
              normal: {
                barBorderRadius: [0, 7, 7, 0],
                // color: function (p) {
                //   return colors[p.dataIndex]
                // },
                color: function (params) {
                  const index = params.dataIndex
                  return new echarts.graphic.LinearGradient(0, 0, 1, 0, [
                    {
                      offset: 0,
                      color: color1[index],
                    },
                    {
                      offset: 1,
                      color: color2[index],
                    },
                  ])
                },
              },
            },
            label: {
              show: true,
              position: 'right', // 位置
              color: '#ffffff',
              fontSize: 18,
              distance: 8, // 距离
              padding: [0, 0, 0, 15],
              formatter: '{c} ', // 这里是数据展示的时候显示的数据
            },
          },
          {
            name: '背景',
            type: 'bar',
            barWidth: 14,
            barGap: '-100%',
            // barCategoryGap: 100,
            data: bgData,
            itemStyle: {
              normal: {
                barBorderRadius: [0, 7, 7, 0],
                color: 'rgba(0,0,0,0.3)',
              },
            },
          },
        ],
      }
      return option
    }
  }
import GlobalData from "./GlobalData";
import {
  queryMainEventProcessInfo,
  queryHandleEventProcessInfo,
  getSmallQuestionMsg,
  listEventMedia,
  selectEventRecordInfo
} from "./utils/api";

export default {
  //单位统计
  dwtjConfigList: [
    {
      code: "sj",
      name: "事件相关市属单位",
      icon: GlobalData.imgUrl + "事件相关市属单位图标.png",
      value: 3,
      unit: "个",
    },
    {
      code: "qj",
      name: "事件相关区属单位",
      icon: GlobalData.imgUrl + "事件相关市属单位图标.png",
      value: 3,
      unit: "个",
    },
  ],
  //事件详情
  eventInfo: {
    updateFlag : false,
    eventData: {},
    //事件简介
    eventDesc: null,
    //处置进程
    processList: [],
    //派遣子流程，key：处置部门id
    distributeMap: {},
    //涉及部门
    depList: [],
    //打开子流程的主流程id
    openNodeId: null,
    // 现场信息
    eventMedia: {},
  },
  //事件追踪
  sjgzData: [],

  /**
   * 详情-获取事件详情
   */
  loadEventInfo(id, reportTime) {
    const self = this;
    const param = { id, reportTime };
    selectEventRecordInfo(param).then((res) => {
      const data = res.data || {};
      self.eventInfo.eventData = data;
      self.eventInfo.eventData["mainType"] =
        data.eventTypeName + ">" + data.mainTypeName;
      /**处理事件追踪 */
      self.dealSjgzData(data);
      /**加载事件执法依据 */
      self.loadXqzfyjxx(data.subTypeCode);
    });
  },

  /**
   * 详情- 获取事件政策详情
   */
  loadXqzfyjxx(code) {
    const self = this;
    if (!code) {
      return;
    }
    const param = { code };
    getSmallQuestionMsg(param).then((res) => {
      const data = res.data || {};

      self.eventInfo.eventData = {
        ...self.eventInfo.eventData,
        ...data,
      };
    });
  },

  /**
   * 处理时间追踪
   */
  dealSjgzData(data) {
    this.sjgzData = [
      {
        icon: GlobalData.imgUrl + "事件跟踪-数字1.png",
        content: "受理",
        state: "未开始",
        time: "",
      },
      {
        icon: GlobalData.imgUrl + "事件跟踪-数字2.png",
        content: "派遣",
        state: "未开始",
        time: "",
      },
      {
        icon: GlobalData.imgUrl + "事件跟踪-数字3.png",
        content: "处置",
        state: "未开始",
        time: "",
      },
      {
        icon: GlobalData.imgUrl + "事件跟踪-数字4.png",
        content: "办结",
        state: "未开始",
        time: "",
      },
    ];

    //状态
    const state = data.state;
    if (state === 1) {
      this.sjgzData[0].state = "进行中";
    } else if (state === 3 || state === 4 || state === 16) {
      this.sjgzData[0].state = "完成";
      this.sjgzData[0].time = data.acceptTime;
      this.sjgzData[1].state = "进行中";

      if (state === 4 || state === 16) {
        this.sjgzData[1].state = "完成";
        this.sjgzData[1].time = data.distributeTime;
        this.sjgzData[2].state = "进行中";
      }

      if (state === 16) {
        this.sjgzData[2].state = "完成";
        this.sjgzData[3].state = "完成";
        this.sjgzData[3].time = data.endCaseTime;
      }
    }

    // switch (state) {
    //     case 1:
    //         this.sjgzData[0].state = "进行中"
    //         break
    //     case 3:
    //         this.sjgzData[0].state = "完成"
    //         this.sjgzData[0].time = data.acceptTime
    //         this.sjgzData[1].state = "进行中"
    //         break
    //     case 4:
    //         this.sjgzData[0].state = "完成"
    //         this.sjgzData[0].time = data.acceptTime

    //         this.sjgzData[1].state = "完成"
    //         this.sjgzData[1].time = data.distributeTime

    //         this.sjgzData[2].state = "进行中"
    //         break
    //     case 16:
    //         this.sjgzData[0].state = "完成"
    //         this.sjgzData[0].time = data.acceptTime

    //         this.sjgzData[1].state = "完成"
    //         this.sjgzData[1].time = data.distributeTime

    //         this.sjgzData[2].state = "完成"

    //         this.sjgzData[3].state = "完成"
    //         this.sjgzData[3].time = data.endCaseTime

    //         break

    // }
  },

  /**
   * 详情-现场信息
   */
  loadEventMediaInfo(eventId, reportTime) {
    return new Promise((resovle, reject) => {
      const self = this;
      const param = { eventId, reportTime };
      listEventMedia(param).then((res) => {
        const data = res.data || {};
        self.eventInfo.eventMedia = data;
        resovle(data);
      });
    });
  },
  /**
   * 详情-查询事件主流程
   * 详情-查询事件主流程下的处置信息
   */
  loadEventProcess(id, reportTime) {
    const self = this;
    self.eventInfo.processList = [];
    self.eventInfo.distributeMap = {};
    self.eventInfo.depList = [];
    self.eventInfo.openNodeId = null;

    const promiseList = [
      queryMainEventProcessInfo({ id, reportTime, sortDesc: false }),
      queryHandleEventProcessInfo({
        id,
        reportTime,
        sortDesc: false,
      }),
    ];
    Promise.all(promiseList).then((res) => {
      self.eventInfo.updateFlag = true;
      //涉及部门，key：部门id， value：部门信息
      const depMap = {};
      self.eventInfo.processList = res[0].data || [];
      self.eventInfo.processList.forEach((i) => {
        i.subShow = false;

        if (i.handleDept) {
          depMap[i.handleDeptId] = {
            handleDeptId: i.handleDeptId,
            handleDept: i.handleDept,
            handleTime: i.handleTime,
            action: i.action,
            actionName: i.actionName,
          };
        }
      });
      const cxsjzlcxdczxx = res[1].data || [];
      cxsjzlcxdczxx.forEach((item) => {
        self.eventInfo.distributeMap[item.handleDeptId] = item.handle || [];

        self.eventInfo.distributeMap[item.handleDeptId].forEach((i) => {
          if (i.handleDept) {
            depMap[i.handleDeptId] = {
              handleDeptId: i.handleDeptId,
              handleDept: i.handleDept,
              handleTime: i.handleTime,
              action: i.action,
              actionName: i.actionName,
            };
          }
        });
      });

      for (let i in depMap) {
        self.eventInfo.depList.push(depMap[i]);
      }
      self.eventInfo.updateFlag = false;
    });
  },
};

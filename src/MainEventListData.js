import { getEventList } from "./utils/api";
import GlobalData from "./GlobalData";
export default {
  //表格筛选配置
  loading: true,
  filterConfigList: [
    {
      field: "taskNumber",
      value: null,
      name: "任务号",
      widgetType: "input",
      type: "fixed",
    },
    {
      field: "state",
      value: null,
      name: "状态",
      widgetType: "select",
      type: "fixed",
      options: [],
      dataSetCode: "sjj_sjzs_hqsjztlb",
    },
    {
      field: "eventLevelCode",
      value: null,
      name: "紧急程度",
      widgetType: "select",
      type: "fixed",
      options: [],
      dataSetCode: "sjj_sjzs_hqjjcdlb",
    },
    {
      field: "eventTypeCode",
      value: null,
      name: "事件类型",
      widgetType: "select",
      type: "more",
      options: [
        { id: "EVENT", name: "重点工单" },
        { id: "xtsj", name: "协同事件" },
      ],
      dataSetCode: "sjj_sjzs_hqsjlxlb",
    },
    {
      field: "smallTypeCode",
      value: [],
      name: "问题类型",
      widgetType: "cascader",
      type: "more",
      options: [],
      dataSetCode: "sjj_sjzs_hqwtlblb",
      props: { value: "code", label: "name", children: "children" },
    },
    {
      field: "eventSrcCode",
      value: null,
      name: "事件来源",
      widgetType: "select",
      type: "more",
      options: [],
      dataSetCode: "sjj_sjzs_hqsjly_qdlb",
    },
    {
      field: "deptCode",
      value: null,
      name: "处置单位",
      widgetType: "select",
      type: "more",
      options: [],
      dataSetCode: "sjj_sjzs_hqzzjgsxjg",
    },
    {
      field: "districtCode",
      value: null,
      name: "区域",
      widgetType: "select",
      type: "more",
      options: [],
      dataSetCode: "sjj_sjzs_hqhzqysxjg",
      props: { value: "code", label: "name" },
    },
    // { field: 'streetCode', value: null, name: '区域', widgetType: 'cascader', type: 'more', options: [], dataSetCode: 'sjj_sjzs_hqhzqysxjg', props: { value: 'code', label: 'name', children: 'children' } },
    {
      field: "problemDescription",
      value: null,
      name: "问题描述",
      widgetType: "input",
      type: "more",
    },
    {
      field: "addressDescription",
      value: null,
      name: "地址描述",
      widgetType: "input",
      type: "more",
    },
    {
      field: "reporterPhone",
      value: null,
      name: "上报人",
      widgetType: "input",
      type: "more",
    },
  ],
  //表格信息
  tabInfo: {
    colConfig: [
      { name: "任务号", prop: "taskNumber" },
      { name: "紧急程度", prop: "eventLevelName" },
      { name: "事件状态", prop: "stateName" },
      { name: "事件来源", prop: "appChannelName" },
      { name: "事件类型", prop: "eventTypeName" },
      { name: "问题大类", prop: "mainTypeName" },
      { name: "问题小类", prop: "subTypeName" },
      { name: "问题描述", prop: "eventDesc" },
      { name: "区域", prop: "districtName" },
      { name: "街道", prop: "streetName" },
      { name: "社区", prop: "communityName" },
      { name: "网格", prop: "gridName" },
      { name: "地址描述", prop: "address" },
      { name: "上报时间", prop: "reportTime" },
      { name: "上报人", prop: "reporter" },
    ],
    currentPage: 1,
    pageSize: 20,
    total: 0,
    list: [],
  },
  /**
   * 重置筛选内容
   */
  resetFilter() {
    this.filterConfigList.forEach((i) => {
      switch (i.field) {
        case "taskNumber": //任务号
        case "state": //状态
        case "eventLevelCode": //紧急程度
        case "eventTypeCode": //事件类型
        case "eventSrcCode": //事件来源
        case "deptCode": //处置单位
        case "problemDescription": //问题描述
        case "addressDescription": //地址描述
        case "reporterPhone": //上报人
          i.value = null;
          break;
        case "smallTypeCode": //问题类型
        case "districtCode": //区域
          i.value = null;
          break;
      }
    });
  },
  /**
   * 加载数据
   */
  loadData() {
    const self = this;
    if (self.tabInfo.currentPage <= 1) {
      self.tabInfo.total = 0;
    }
    self.tabInfo.list = [];

    const param = {
      pageSize: self.tabInfo.pageSize,
      currentPage: self.tabInfo.currentPage,
      startTime: dayjs().format("YYYY-01-01 00:00:00"),
      endTime: dayjs().format("YYYY-MM-DD 23:59:59"),
      label: "",
      districtCode: GlobalData.streetConfig[GlobalData.street].code
    };
    let eventTypeCode = null;
    self.filterConfigList.forEach((i) => {
      if (i.value === null || i.value === "" || i.value === undefined) {
        return;
      }
      switch (i.field) {
        case "taskNumber": //任务号
          param[i.field] = i.value;
          break;
        case "state": //状态
          param[i.field] = [i.value];
          break;
        case "eventLevelCode": //紧急程度
          param[i.field] = [i.value];
          break;
        case "eventTypeCode": //事件类型
          eventTypeCode = i.value;
          break;
        case "smallTypeCode": //问题类型
          if (i.value.length > 0) {
            param[i.field] = [i.value[i.value.length - 1]];
          }
          break;
        case "eventSrcCode": //事件来源
          param[i.field] = [i.value];
          break;
        case "deptCode": //处置单位
          param[i.field] = [i.value];
          break;
        case "districtCode": //区域
          // if (i.value.length > 0) {
          //     // param[i.field] = [i.value[i.value.length - 1]]
          //     param[i.field] = [i.value]
          // } else {
          //     param[i.field] = undefined
          // }
          param[i.field] = i.value;
          // console.log('检查区域传参', param[i.field])
          break;
        case "problemDescription": //问题描述
          param[i.field] = i.value;
          break;
        case "addressDescription": //地址描述
          param[i.field] = i.value;
          break;
        case "reporterPhone": //上报人
          param[i.field] = i.value;
          break;
      }
    });
    switch (eventTypeCode) {
      case "EVENT": //重点工单, 查询重点工单相关的事件，这个条件必填 eventTypeCode: ["collaborative"]，这样返回的就是重点工单，并且条件搜索需要禁用事件类型条件，不能人为修改，与其他条件搜索正常组合
        param.eventTypeCode = ["collaborative"];
        break;
      case "xtsj": //协同事件,查询协同事件 distributeDeptCode传固定值，每次必传，eventSrcCode传固定值 每次必传，其他查询条件任意组合 ，需要禁用事件来源的查询条件，不可人为修改
        param.eventSrcCode = ["16513087312to9I8X2D6QOTOht5hmH"]; //市城运中心
        param.distributeDeptCode = "202210201729431032707199748681728"; //市平台
        delete param.eventSrcCode;
        break;
    }

    getEventList(param).then((res) => {
      const data = res.data || {};
      self.tabInfo.list = data.records || [];
      self.tabInfo.total = parseInt(data.totalCount);
      // console.log('新旧事件列表对比 - 新列表', self.tabInfo.list)
      self.loading = false;
    });
  },
  loadDataForTopInter(code) {
    const self = this;
    if (self.tabInfo.currentPage <= 1) {
      self.tabInfo.total = 0;
    }
    self.tabInfo.list = [];

    const param = {
      pageSize: self.tabInfo.pageSize,
      currentPage: self.tabInfo.currentPage,
      startTime: dayjs().format("YYYY-01-01 00:00:00"),
      endTime: dayjs().format("YYYY-MM-DD 23:59:59"),
      label: "",
      smallTypeCode: code,
      districtCode: GlobalData.streetConfig[GlobalData.street].code
    };

    getEventList(param).then((res) => {
      const data = res.data || {};
      self.tabInfo.list = data.records || [];
      self.tabInfo.total = parseInt(data.totalCount);
      // console.log('新旧事件列表对比 - 新列表', self.tabInfo.list)
      self.loading = false;
    });
  },
};

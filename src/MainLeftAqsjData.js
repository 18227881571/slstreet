export default {
    //菜单
    aqsjTabList: [
      // // {
      // //   id: 'today',
      // //   name: '本日'
      // // },
      // {
      //   id: 'week',
      //   name: '本周'
      // },
      // {
      //   id: 'month',
      //   name: '本月'
      // },
      // {
      //   id: 'year',
      //   name: '本年'
      // }
    ],
    //柱状图配置
    barConfig:  {
      // //颜色配置
      // color: [
      //   {
        // 		type: 'linear',
        // 		x: 0,
        // 		y: 0,
        // 		x2: 0,
        // 		y2: 1,
        // 		colorStops: [
      //       {
      //   			offset: 0, color: 'rgba(32, 196, 183, 1)' // 0% 处的颜色
        // 			},
      //       {
      //   			offset: 1, color: 'rgba(0, 67, 130, 1)' // 100% 处的颜色
        // 			}
      //   	],
        // 	global: false // 缺省为 false
          // }, '#d1c568'],
      // grid: {
      //   show: false,
      //   top: 10,
      //   left: 60,
      //   right: 30,
      //   bottom: 60,
      //   containLabel: false
      // },
      // xAxis: {
      //   //字号
      //   fontSize: 14,
      //   // 坐标轴名称
      //   name: '',
      //   //名字颜色
      //   nameColor: '#FFFFFF',
      //   // 文字角度
      //   textAngle: 0,
      //   // 轴反转
      //   reversalX: false,
      //   // 文字角度
      //   textInterval: 0,
      //   // x轴 坐标文字颜色
      //   Xcolor: '#FFFFFF',
      //   //X轴线的颜色
      //   lineColorX: 'rgba(255, 255, 255, 0.3)',
      //   //是否显示X分割线
      //   isShowSplitLineX: false,
      //   //X分割线颜色
      //   splitLineColorX: 'rgba(255, 255, 255, 0.3)',
      //   //X轴宽度
      //   xWidth: 2,
      //   //X分割线宽度
      //   xSplitWidth: 2,
      //   //留白策略
      //   boundaryGap: true
      // },
      // yAxis: {
      //   //字号
      //   fontSize: 14,
      //   // 坐标轴是否显示
      //   isShowY: true,
      //   // 坐标轴名称
      //   name: '',
      //   //名字颜色
      //   nameColorY: '#FFFFFF',
      //   // 轴反转
      //   reversalY: false,
      //   // y轴 坐标文字颜色,
      //   Ycolor: '#FFFFFF',
      //   //X轴线的颜色
      //   lineColorY: '#FFFFFF',
      //   //是否显示X分割线
      //   isShowSplitLineY: true,
      //   //X分割线颜色
      //   splitLineColorY: 'rgba(255, 255, 255, 0.3)',
      //   //Y轴宽度
      //   yWidth: 2,
      //   //Y分割线宽度
      //   ySplitWidth: 2,
      //   //Y分割线样式
      //   ySplitType: 'dashed',
      //   //最小值
      //   min: 10
      // },
      // legend: {
      //   //是否显示图例
      //   show: false,
      //   //图例组件离容器左侧的距离, 值可以是像 20 这样的具体像素值，可以是像 '20%' 这样相对于容器高宽的百分比，也可以是 'left', 'center', 'right'。
      //   left: 'right',
      //   //图例组件离容器上侧侧的距离, 值可以是像 20 这样的具体像素值，可以是像 '20%' 这样相对于容器高宽的百分比，也可以是 'top', 'middle', 'bottom'
      //   top: '5%',
      //   //图例列表的布局朝向。 'horizontal' 'vertical'
      //   orient: 'horizontal',
      //   //图例标记的图形宽度
      //   itemWidth: 6,
      //   //图例标记的图形高度
      //   itemHeight: 6,
      //   //图例项的 icon。'circle', 'rect', 'roundRect', 'triangle', 'diamond', 'pin', 'arrow', 'none' 可以通过 'image://url' 设置为图片，其中 URL 为图片的链接，或者 dataURI。
      //   icon: 'rect',
      //   //文字颜色
      //   color: '#FFFFFF',
      //   //字号
      //   fontSize: 10,
      // },
      // tooltip: {
      //   //是否显示提示框组件。
      //   show: true,
      //   //触发类型， 'item'、 'axis'、'none'
      //   trigger: 'axis',
      //   //颜色
      //   color: '#FFFFFF',
      //   //字号
      //   fontSize: 14,
      //   //背景颜色
      //   backgroundColor: 'rgba(34, 62, 97, 0.8)',
      // },
      // //线配置
      // barStyle: {
      //   //线颜色
      //   color: null,
      //   //宽度
      //   width: 20,
      // },
    },
    barData: {
      // today: [
      // 	{
      //   	type: '安全事件TOP5',
      //   	name: '110警情',
      //   	value: 20,
      // 	},
      // 	{
      //  	 	type: '安全事件TOP5',
      //  	 	name: '消防安全',
      //  	 	value: 34,
      // 	},
      // 	{
      //   	type: '安全事件TOP5',
      //   	name: '危化品安全',
      //   	value: 50,
      // 	},
      // 	{
      //   	type: '安全事件TOP5',
      //   	name: '燃气安全',
      //   	value: 30,
      // 	},
      // 	{
      //   	type: '安全事件TOP5',
      //   	name: '钢瓶安全',
      //   	value: 45,
      // 	}
        // ],
      // week: [
      // 	{
      //   	type: '安全事件TOP5',
      //   	name: '110警情',
      //   	value: 140,
      // 	},
      // 	{
      //  	 	type: '安全事件TOP5',
      //  	 	name: '消防安全',
      //  	 	value: 351,
      // 	},
      // 	{
      //   	type: '安全事件TOP5',
      //   	name: '危化品安全',
      //   	value: 500,
      // 	},
      // 	{
      //   	type: '安全事件TOP5',
      //   	name: '燃气安全',
      //   	value: 300,
      // 	},
      // 	{
      //   	type: '安全事件TOP5',
      //   	name: '钢瓶安全',
      //   	value: 450,
      // 	}
        // ],
      // year: [
      // 	{
      //   	type: '安全事件TOP5',
      //   	name: '110警情',
      //   	value: 20,
      // 	},
      // 	{
      //  	 	type: '安全事件TOP5',
      //  	 	name: '消防安全',
      //  	 	value: 34,
      // 	},
      // 	{
      //   	type: '安全事件TOP5',
      //   	name: '危化品安全',
      //   	value: 50,
      // 	},
      // 	{
      //   	type: '安全事件TOP5',
      //   	name: '燃气安全',
      //   	value: 30,
      // 	},
      // 	{
      //   	type: '安全事件TOP5',
      //   	name: '钢瓶安全',
      //   	value: 45,
      // 	}
        // ],
    }
  }
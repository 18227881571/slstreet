import GlobalData from './GlobalData'

export default {
	//风险预警配置
  fxyjList:[
    // {
    //   type: 'aqi',
    //   name: '空气质量',
    //   color: 'green-num',
    // 	bgColor: '',
    // 	aqi: 33,
    // 	aqiDesc: '优'
    // },
    // {
    //   icon: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABYAAAAWCAYAAADEtGw7AAAAAXNSR0IArs4c6QAABJxJREFUSEuNlVtsVFUUhv9/7XNmpsOlClHQIA8moJEY0GKQFKFFhKLwonRKRU20kUowoDExAXkYJCEkGGiCgbRGwEg1lIsQjJpKobQhBeUSExDL1VigQKLIrZ2eM2cvswfHIJfAfjj75Jydb///2mutTdzjUFU2A6aUzO5U9dx87E/tGwE1IhijgFFFQOCDx/rxO94Lt0HVlANK0u5T9UeS4U8XdGBCsMUQuxFhacJDdwhMyUaoiQTv3BXsoCkyOnZM45f64l1CZwG6CeBoCrc8/QCXqapsAOjWHTivryv04zuCc9abYUpLmW09q0Wi+jmB4QpkAXggdhY/LOOdg5OAzTvae1r7Z6m/3BbsoBsAcQoaf49mC7EcgA/AKtBDoAtAfwINclJmlJQgyoto7NAhYm3LLWAHdXF34PiJaKWQMwGEAEQV9IwUZyM7FcR8t1DBTZ0XWdlZhChN2q0no7Wi7Pc/sIOS1Np9mizsHW0kMFmBIK/WeDo22wNDQ6eojcqEij6lik3TH/emff1rdqUIx4RGxtwCXghw8KHsFgBT/7UdU4V6gnFBBBpBC4kdQcYr82LZbwFMJJFV6BmPsr+g0LyWeoTd/4EbGtSkUow+PRAuUmCBKnpI5KC+YGxoISRaAGwfcNwrOzck/AqWKSP6nrWsBDHKKuvmFnnVaVXJgd2Li8/itrCYoq1QWBDiZmM4Luugqi1UND3a4U86PjisJ1Dhi77SHeGIIdsIFJK4EDf+iPdHsvO6YndgpC5oDX4EdALgrEGEUhKEUN+zrQC3t3f6ZUMHBvWgg6K8O4t2X7AHQIECFLB60XOxuvIGNXSPDSlGc7dnXqCwEQp1WxmRcWEWEGNbCTYV0i+7qME6KiqMoLwrwtGEYI8CcZcxUHxf83z8xbxIptMq6TRt1Q8ZV9+TAfT4xpZ0BYz5hrsANF26HJ/UtzCzjsrpIkwFIX7zPN0LRUEu44DLvmLUqsmJ9jyPM2vVr6tmWLmte7EC8wCcspGuMIbLADRnuxMTTDLzJRSVBlqeseZoTGzOPoBAFTEjqKqfUrC6ZKd6zaV0lYlcjKdu06QNMg0CjAcQF+aK4XA8SjzTbTJrBKgQ0YogNEc8z+7V60pdfseEWLz15YKP8iHNN7UcuGT9tUYfPGgoqwNrfwbQB8AfUJyhYLQIUpnQtseMOGgiDyWwtGl68sOboTnFo9Z1fUJqvz0zer3lPjxbn5mg1m4jESdAtVgfRXaJ58tuZ98CERSeGMxrezW55HbQHPjJNdcOq7HTDr3R58iwBo0dTjEYvvbqRKv8hkASxFUqPHf6LqUI/O2BVQfeTG7OxbQEkUvVm/s6h352tTMERpx6u/d5pFWKHoLZX81w2OorTwQRdygwgEDkbggD7IqsV3W8OnHixoO63WXBQauu7SZ0Y8es3suRVs+1RqRpB626Moe5zqktqloKsq7jXK8v3L872b9xAz644nIxKJutYonxtD60Ji42mm/Ilwyl+OzsZIdz4oC5OdcDaO92peWy4r6aK2MpqFXF/a6KSBwkTNVfc5Kni2rV3z8T2ZKFMM3p6zl6L+Mf0jhFMq+o644AAAAASUVORK5CYII=',
    //   name: '社会安全',
    //   list: [
    //   	{
    //   		name: '刑事案件',
    //       value: 0,
    //       valueField: ''
    // 		},
    //   	{
    //   		name: '交通警情',
    //       value: 5,
    //       valueField: ''
    // 		}
    //   ]
    // },
    // {
    //   icon: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABcAAAAZCAYAAADaILXQAAAAAXNSR0IArs4c6QAABRRJREFUSEuFlW1sU1UYx//PubftVmTAdCSYALLYjW0JSwQHiCBTdFKRGIRKwKAJKnFhgRt8wUFCjQT4QGAZsmQ6F0XBuc2IMwgiOhWIQQcBogMiDhDkbRMGlK5v9zx6znrHGBXuh/bcnnN+z/95LQEAMxMAIiKp3u/0MLNQZ5zz6j4RscNy1gqq4Dcdvh08KUSBHZjobSRpVO8RMxsnAdd9QEzZcS6lMuAobLvKuXYchu9uam1hdmVdwV3DB9JldedUJw9qH4DQGKK4Vt56iUsJ6MzLpC3Ki1Th6XGdmY5cxmEivD9yEFUeucQLATyfl0njWtt5CBnYbgjMyBlIbRp+6CKXE/HC2D00XFnsHUPHA8fowQ4uAni9S1JJwWAKHWqXLQB9UphFFQcv2huIaGRhlnhcn1eX953nMjBXCtBTDw6hr+uZjQCR3Tc0LWfZm7Bx/7ihdFjt7Wnn/u4EhnWEcNzvo+iBCzw+RugYN5j+0ALVoR9O22UAKsFYNXmYsayZ2SwmSjjZbwCEMtb8l72ciCZOHipK9h7nwXG33M62mFs8go42n7IXQ3CoeKhZ44jT8G0n7EXEqGDgENlikt9HV1OFZnub3MUk3/WPMLdua7PfEeDHpmabDzWd5BGmlC2GIWeXDHd9G2QWQSKp4Y3HbAvgdVqp4NWzfK7y5mY21fvpTHjmFdL1+no2IrlIaxuFLnWx8U/2kUTXsz46s/VYfGKCMH1mjuv13qI0/MPfbsABnI8K44EF+XTuoyOcA9teGXEZpQtyqUMpym8ABQLd+ejbPL1rXK/Vx8aDMQtM6xiIEeCWjB/d0vSfG41IViu8Zhf6x0ViKRuJirLC9BMK+nbyrvLCeVfr3kWg4av3xSwCVFhUElWWDUk09a0i1w69/0v0OcFUx0CnBF4rH+v+YFY9GwWt0J0YDKYeGxq+/KeIBSIHrkaBgOCSlRPSdqr9ZbujqwlYmnT1ROc1d94GP0WhZlJyDKTqaA23vo9YzD1wo3ssoCUOKq2a4v51ya5ovk34GUAGgzs8Hk/21euhdI/hrpLAFxumeDYHgyz6eqDh87+JWOCesKgqUWWkPDgLKcfW+L1n5u+IVAF4FUAsZkifK0GriGgugTbVPOl5IdjMZrC4uzecR8NnN4UtRo9yXYIKAsANsPXZdG/F7KbIKxJcTcAFTyJeIN1mboJRBaKyumnpu/9Xub8xbNGtcFVuBsBfbpvpfWZaQ+QJENcKQS81zUjbUdIYDrCE3BnwNiolKsHqu2EWpJMHrfyRT8MWblSLo1xVgtrvksxzds/pt3V0NXv3L6DwpC1djwri79SuBD6XduzlPXMHXu5JcJAFVkD/A2HMJqVcd6iKmQPXfZI0YNsSpQde7Pee+rHo41AhM21mIE/nhmhCguxOIY0CpEV27g9kXlGGNLygNmyBU8IdAxIEw5a09uj89Dcctwtqw2sAjiZsHDAFGkEwmXGSgN/Zlm9qeHZ1yOJbw9I78coDmwBTAnWC5Iq2s/2PIx+EANnZ1aHFDKwHEAXgUt5IYI2GD9kYWgSgAkCkT1hu7g2GJIKre6xwjQCX/70w4597N15/msFNybDGAaQz8JWGZ1aGlgC8NkXMb2k8JwnJeMUkoxYuu9yQ5hhIbgAwQJcxYZ6G96sIjSLww8QcAXUPs9s+DAYLWwhOk+AMAE3XFmccy1h3pQiGMZ6lbLlmZey9M+hOhrQLTGiAUPHvOf5fOf4LXJOKDBPf3TQAAAAASUVORK5CYII=',
    //   name: '自然灾害',
    //   list: [
    //   	{
    //   		name: '高温预警',
    //       value: 0,
    //       valueField: ''
    // 		},
    //   	{
    //   		name: '大风预警',
    //       value: 0,
    //       valueField: ''
    // 		}
    //   ]
    // },
    // {
    //   icon: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAAXCAYAAAALHW+jAAAAAXNSR0IArs4c6QAAA01JREFUSEuVlU1oXGUUhp/33plpJiaNIOlPqi4q3TRIxWjBipqIxbpw02oWrdJNIf6kkiDWggsnS4lCUkkxBV1o04W2UBAaIUhKbEvBJEYkFvwH2w62NmUaZ2oyc++R786dsVQ7OgdmMff77nPe853zfleTZok7c+w242EZJYSoI0KiN5Y8eH/D7ZrVud/tsIldgMkt1h9m5feuAc9o7lLoHoQOCPj186L3SkBS8JXOZsMA8ARLwDdAEGesyY6rKRmsBtbHm3M6+WtQBBLAYOdd/r6PzfxuySWpGWYmSTaVtdawFM4BbcBVjf9UBgp6t633RyYnLdHVJVfC/wK6TeM/BjOI+4EFHfuuVFYo9e/Y4A/VC8yYefd+H86CbYoUfjRfBfY9v9EfrhfoSj/8bQS8LwK+N1eMFVrfC5tSw5lJS2TqKNkBR78uzQJl4DtfLlea0v/qg6mh0WlL9nREY1AzMqCMFLqSm2eKsxjlkgfOVIG9b25JjdSr0GUdOLM8A3FT9p1cihSaaWSwK9V74IStWEjzn2Ozthn1PKBi/+eFdUnfnwbWRApfmvjTTboLV+YPgEsgA/+WVjQCFDnE/VpjmGPktPtEFegW5XzoQjVcHW2JdlfNH/8jpx3Hr1cyOcRVTxoQtjowXv+XrjjPO1d9kZAdLEk9hHQ6u5bvActp2yeFirmdW14bf7bxbQd66mhhzIyd8VE4SEXF9aSp/dPu9M/bj+XXFkKdA1qqXn70SMHRo8whDJ/a2djnFh85kp8QeuLv7FWgu+gem9qVnnporLA5KU65myY+hSvq+DAfCLxYAWYc96QWwx6P1d14mk5lAuNiCJ9JPClYV6nCQibU/kF+AOONm+/CSutvNd03ZKkcxS++hU9Hz+85dG0L5neBuZEJkF2QanwODLOQNXhKy0iYdKVBpaPze1oWImDbwfwesFHgPLDSAm3N7r1tmox5ZOTOtxxmQrL2jKUWVuXPCu4GrTALJ7IvN2/vzJhrLNxxoLBZCk7HI/FbQ8LvOP9i44WOUUs2Z6NmVOPyRrz5bi2vendxLCSaAmR66/IrTfuJgHHWlqE/thrhc+ZzaHHvytP/UHeTytaRS03LpfR+g2JTw+LgxZ62gmP9BXEyk07tn/0eAAAAAElFTkSuQmCC',
    //   name: '城市交通',
    //   list: [
    //   	{
    //   		name: '拥堵指数',
    //       value: 1.4,
    //       valueField: ''
    // 		},
    //   	{
    //   		name: '大客流预警',
    //       value: 0,
    //       valueField: ''
    // 		}
    //   ]
    // }
  ]
}
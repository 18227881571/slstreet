export default {
    get_sxproData(xlist, ylist) {
      const option = {
        tooltip: {
          trigger: 'axis',
          textStyle: {
            fontSize: 18,
          },
          axisPointer: {
            type: 'cross',
            label: {
              backgroundColor: '#6a7985',
              color: '#FFF',
              fontSize: 18,
            },
          },
        },
        legend: {
          show: false
        },
        grid: {
          top: '10px',
          left: '10%',
          right: '0%',
          bottom: '7%',
          containLabel: true,
        },
        xAxis: [
          {
            type: 'category',
            boundaryGap: true,
            data: xlist,
            axisLabel: {
              show: true,
              textStyle: {
                color: '#ffffff',
                fontSize: 14,
                padding: 10,
              },
            },
            axisTick: {
              show: false
            },
            axisLine: {
              show: false,
              lineStyle: {
                color: 'rgba(255,255,255,0.5)',
                width: 1.5,
              },
            },
          },
        ],
        yAxis: [
          {
            type: 'value',
            name: '',
            nameTextStyle: {
              color: "#ffffff",
              // lineHeight: 30,
              fontSize: 12,
            },
            splitNumber: 3,
            minInterval: 1,
            splitLine: {
              //网格线
              lineStyle: {
                type: 'solid', //设置网格线类型 dotted：虚线 solid:实线
                color: 'rgba(102,102,102,0.8)',
                width: 2.5,
              },
              show: true,
            },
            axisLabel: {
              show: true,
              textStyle: {
                color: '#fff',
                fontSize: 12,
              },
            },
          },
        ],
        series: [
          {
            type: 'line',
            name: '受理量',
            // stack: 'Total',
            emphasis: {
              focus: 'series',
            },
            label: {
              show: false,
              position: 'top',
              fontSize: 18,
              color: '#1DCAD4',
            },
            smooth: true,
            symbolSize: 10, //原点大小
            color: '#00FFB4',
            itemStyle: {
              normal: {
                color: '#00FFB4', // 原点颜色
                lineStyle: {
                  color: '#00FFB4', //折线颜色
                  width: 3, // 0.1的线条是非常细的了
                },
              },
              borderColor: '#00FFB4',
              borderWidth: 2,
            },
            areaStyle: {
              //区域填充样式
              normal: {
                //线性渐变，前4个参数分别是x0,y0,x2,y2(范围0~1);相当于图形包围盒中的百分比。如果最后一个参数是‘true’，则该四个值是绝对像素位置。
                color: new echarts.graphic.LinearGradient(
                  0,
                  0,
                  0,
                  1,
                  [
                    {
                      offset: 0,
                      color: 'rgba(0, 255, 180, 0.5)',
                    },
                    {
                      offset: 1,
                      color: 'rgba(0,255,255,0)',
                    },
                  ],
                  false,
                ),
                shadowColor: 'rgba(0,255,255,0.5)', //阴影颜色
              },
            },
            data: ylist,
          }
        ],
      }
      return option
    },
    getGaugeChartOption(value, startColor, endColor) {
      const showValue = value >= 500 ? 500 : value
  
      const option = {
        series: [
          {
            type: 'gauge',
            max: 500,
            progress: {
              show: true,
              width: 6,
              itemStyle: {
                color: {
                  type: 'linear',
                  x: 0,
                  y: 0,
                  x2: 0,
                  y2: 1,
                  colorStops: [
                    {
                      offset: 0,
                      color: endColor, // 0% 处的颜色
                    },
                    {
                      offset: 1,
                      color: startColor, // 100% 处的颜色
                    },
                  ],
                  global: false, // 缺省为 false
                },
              },
            },
            axisLine: {
              show: false,
              lineStyle: {
                width: 6,
              },
            },
            pointer: {
              show: false,
            },
            axisTick: {
              show: false,
            },
            splitLine: {
              length: 1,
              lineStyle: {
                width: 2,
                color: '#194280',
              },
            },
            axisLabel: {
              show: false,
            },
  
            title: {
              show: false,
            },
            detail: {
              show: false,
              valueAnimation: true,
              fontSize: 40,
              color: [
                [0, startColor],
                [1, endColor],
              ],
              // [
              //   [0.1, 'red'], // 0~10% 红轴
              //   [1, 'blue'], // 20~30% 蓝轴
              // ],
              offsetCenter: [0, '50%'],
              formater: value,
            },
            data: [
              {
                value: showValue,
              },
            ],
          },
        ],
      }
      return option
    }
  }
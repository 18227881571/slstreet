export default {
  configData: {
    //Tab配置
    tabList: [
      {
        id: 'today',
        name: '当日'
      },
      {
        id: 'month',
        name: '当月'
      },
      {
        id: 'year',
        name: '当年'
      }
    ],
    //事件概览
    sjglConfig: [
      {
        name: '上报数量',
        icon: '/src/screen/1600385025259147265/上报数量.png',
        value: '2569',
        unit: '件',
        valueField: 'total'
      },
      {
        name: '已办结',
        icon: '/src/screen/1600385025259147265/办结数量.png',
        value: '1469',
        unit: '件',
        valueField: 'finnishCount'
      },
      {
        name: '处理中',
        icon: '/src/screen/1600385025259147265/受理数量.png',
        value: '69',
        unit: '件',
        valueField: 'handlingCount'
      },
      {
        name: '待签收',
        icon: '/src/screen/1600385025259147265/待受理.png',
        value: '0',
        unit: '件',
        valueField: 'waitSignCount'
      }
    ],

    //办件趋势配置
    bjqsConfig: {
      //颜色配置
      color: ['#09de2f', '#d1c568'],
      grid: {
        show: false,
        left: 80,
        right: 30,
        top: 20,
        bottom: 20,
      },
      xAxis: {
        //字号
        fontSize: 14,
        // 坐标轴名称
        name: '',
        //名字颜色
        nameColor: '#FFFFFF',
        // 文字角度
        textAngle: 0,
        // 轴反转
        reversalX: false,
        // 文字角度
        textInterval: 0,
        // x轴 坐标文字颜色
        Xcolor: '#FFFFFF',
        //X轴线的颜色
        lineColorX: 'rgba(255, 255, 255, 0.3)',
        //是否显示X分割线
        isShowSplitLineX: false,
        //X分割线颜色
        splitLineColorX: 'rgba(255, 255, 255, 0.3)',
        //X轴宽度
        xWidth: 1,
        //X分割线宽度
        xSplitWidth: 1,
      },
      yAxis: {
        //字号
        fontSize: 14,
        // 坐标轴是否显示
        isShowY: true,
        // 坐标轴名称
        name: '',
        //名字颜色
        nameColorY: '#FFFFFF',
        // 轴反转
        reversalY: false,
        // y轴 坐标文字颜色,
        Ycolor: '#FFFFFF',
        //X轴线的颜色
        lineColorY: 'rgba(255, 255, 255, 0.3)',
        //是否显示X分割线
        isShowSplitLineY: true,
        //X分割线颜色
        splitLineColorY: 'rgba(255, 255, 255, 0.3)',
        //Y轴宽度
        yWidth: 2,
        //Y分割线宽度
        ySplitWidth: 2,
        //Y分割线样式
        ySplitType: 'dashed',
        //最小值
        min: 10
      },
      legend: {
        //是否显示图例
        show: false,
        //图例组件离容器左侧的距离, 值可以是像 20 这样的具体像素值，可以是像 '20%' 这样相对于容器高宽的百分比，也可以是 'left', 'center', 'right'。
        left: 'right',
        //图例组件离容器上侧侧的距离, 值可以是像 20 这样的具体像素值，可以是像 '20%' 这样相对于容器高宽的百分比，也可以是 'top', 'middle', 'bottom'
        top: '5%',
        //图例列表的布局朝向。 'horizontal' 'vertical'
        orient: 'horizontal',
        //图例标记的图形宽度
        itemWidth: 6,
        //图例标记的图形高度
        itemHeight: 6,
        //图例项的 icon。'circle', 'rect', 'roundRect', 'triangle', 'diamond', 'pin', 'arrow', 'none' 可以通过 'image://url' 设置为图片，其中 URL 为图片的链接，或者 dataURI。
        icon: 'rect',
        //文字颜色
        color: '#FFFFFF',
        //字号
        fontSize: 14,
      },
      tooltip: {
        //是否显示提示框组件。
        show: true,
        //触发类型， 'item'、 'axis'、'none'
        trigger: 'axis',
        //颜色
        color: '#FFFFFF',
        //字号
        fontSize: 14,
        //背景颜色
        backgroundColor: 'rgba(34, 62, 97, 0.8)',
      },
      //线配置
      lineStyle: {
        //线颜色
        color: null,
        //宽度
        width: 2,
      },
      series: {
        symbolSize: 12,
        smooth: 0.6,
      }
    },
    //办件趋势数据
    bjqsData: [
      // {
      //   type: '办件情况',
      //   name: '05月',
      //   value: 0,
      // },
      // {
      //   type: '办件情况',
      //   name: '06月',
      //   value: 0,
      // },
      // {
      //   type: '办件情况',
      //   name: '07月',
      //   value: 0,
      // },
      // {
      //   type: '办件情况',
      //   name: '08月',
      //   value: 0,
      // },
      // {
      //   type: '办件情况',
      //   name: '09月',
      //   value: 0,
      // },
      // {
      //   type: '办件情况',
      //   name: '10月',
      //   value: 0,
      // },
    ],
    //分类配置
    flzbConfig: {
      //颜色配置
      color: ['#25a6ff', '#a6daff', '#0edf9d', '#f17272', '#ffd141'],
      grid: {
        show: true,
        left: 0,
        right: 0,
        top: 0,
        bottom: 50,
      },
      xAxis: {
        //字号
        fontSize: 14,
        // 坐标轴名称
        name: '',
        //名字颜色
        nameColor: '#FFFFFF',
        // 文字角度
        textAngle: 0,
        // 轴反转
        reversalX: false,
        // 文字角度
        textInterval: 0,
        // x轴 坐标文字颜色
        Xcolor: '#FFFFFF',
        //X轴线的颜色
        lineColorX: '#FFFFFF',
        //是否显示X分割线
        isShowSplitLineX: false,
        //X分割线颜色
        splitLineColorX: '#FFFFFF',
        //X轴宽度
        xWidth: 1,
        //X分割线宽度
        xSplitWidth: 1,
        //留白策略
        boundaryGap: true
      },
      yAxis: {
        //字号
        fontSize: 14,
        // 坐标轴是否显示
        isShowY: true,
        // 坐标轴名称
        name: '',
        //名字颜色
        nameColorY: '#FFFFFF',
        // 轴反转
        reversalY: false,
        // y轴 坐标文字颜色,
        Ycolor: '#FFFFFF',
        //X轴线的颜色
        lineColorY: '#FFFFFF',
        //是否显示X分割线
        isShowSplitLineY: true,
        //X分割线颜色
        splitLineColorY: '#FFFFFF',
        //Y轴宽度
        yWidth: 1,
        //Y分割线宽度
        ySplitWidth: 1,
      },
      legend: {
        //是否显示图例
        show: true,
        //图例组件离容器左侧的距离, 值可以是像 20 这样的具体像素值，可以是像 '20%' 这样相对于容器高宽的百分比，也可以是 'left', 'center', 'right'。
        left: 260,
        //图例组件离容器上侧侧的距离, 值可以是像 20 这样的具体像素值，可以是像 '20%' 这样相对于容器高宽的百分比，也可以是 'top', 'middle', 'bottom'
        top: 20,
        //图例宽度
        legendWidth: 120,
        //图例高度
        legendHeight: 134,
        //图例列表的布局朝向。 'horizontal' 'vertical'
        orient: 'vertical',
        //图例标记的图形宽度
        itemWidth: 14,
        //图例标记的图形高度
        itemHeight: 14,
        //图例项的 icon。'circle', 'rect', 'roundRect', 'triangle', 'diamond', 'pin', 'arrow', 'none' 可以通过 'image://url' 设置为图片，其中 URL 为图片的链接，或者 dataURI。
        icon: 'circle',
        //文字颜色
        color: '#FFFFFF',
        //字号
        fontSize: 14,
        //最多数目
        maxCount: -1
      },
      tooltip: {
        //触发类型， 'item'、 'axis'、'none'
        trigger: 'item',
        //是否显示提示框组件。
        show: true,
        //颜色
        color: '#FFFFFF',
        //字号
        fontSize: 14,
        //背景颜色
        backgroundColor: 'rgba(34, 62, 97, 0.8)',
        // formatter: function () {
        //   return `<h1>我显示出来了吧</h1>`
        // },
      },
      //线配置
      lineStyle: {
        //线颜色
        color: null,
        //宽度
        width: 1,
      },
      series: {
        //半径
        radius: ['60%', '90%'],
        center: ['30%', '50%'],
        //组件离容器左侧的距离。值可以是像 20 这样的具体像素值，可以是像 '20%' 这样相对于容器高宽的百分比，也可以是 'left', 'center', 'right'
        left: 'left',
        //组件离容器右侧的距离。 值可以是像 20 这样的具体像素值，可以是像 '20%' 这样相对于容器高宽的百分比。
        right: 0,
        // 组件离容器上侧的距离。值可以是像 20 这样的具体像素值，可以是像 '20%' 这样相对于容器高宽的百分比，也可以是 'top', 'middle', 'bottom'。
        top: 0,
        // 组件离容器下侧的距离。值可以是像 20 这样的具体像素值，可以是像 '20%' 这样相对于容器高宽的百分比。
        bottom: 0,
        //标签配置
        label: {
          show: false,
          position: '',
          alignTo: '',
          color: '',
          fontSize: 14,
          fontWeight: '',
          textAlign: '',
          lineHeight: 14,
        },
      },
    },
    //分类占比数据
    flzbData: [
      // {
      //   name: '企业健康官',
      //   value: 0,
      // },
      // {
      //   name: '生态环城局',
      //   value: 0,
      // },
      // {
      //   name: '安全生产',
      //   value: 0,
      // },
      // {
      //   name: '城市内涝',
      //   value: 0,
      // },
      // {
      //   name: '网格员',
      //   value: 0,
      // },
    ]
  }
}
import GlobalData from './GlobalData'

export default {
  zbtxList: [
    {
      type: 'dbld',
      title: '值班领导',
    //   icon: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADYAAAA2CAYAAACMRWrdAAAAAXNSR0IArs4c6QAACcJJREFUaEPdmnlsXNUVxr9z33uzb/HYHttJbBzAJAFiYighBBKW0kUCugiQurK2KiFpGxaxREUpooAUSiRCE1TCEoraitBWBdSWlsUJKWvi1CaLayCOHWJ7vGXsmfEs7917q/u8KCFeUzx28v7waMZvRvf3vnPOPefcQwAgpSwBcB2ACgAO9dkJeKUBfAJgCxEdogGo2wHMAtANIAOAFJgQ/a/T9WIMcmBt6tUJIA/AZwAeU2A/B7AYwBsAXgSQrotGNfWFBZGIqQSdpmBUF40aA+vkAFwDVnc5gHcV2IaBxa8hog4pJRFRP8wlb7ngTjnQa04vuIBBSLmzqL5UmZ9yJXvNUsoCAGuUxal3mwDEAawmopS6r+qWrfNg0UOQWALAPU0VS4Hwb+jyvppNy/YpGCmlWuuvAPjVm6cAJOuiWF1ZRMlFN26bnxXYSkC+BLgErOkIRoBOgCaBTgfDsvefXbq3tk16F0RsMO+gYom6aHR1ZVFRsuqGbS9KiWstnd5uC9DTXIct93S7NAuuol55s27Ji4mwpea5pdfVtrV5F0QiCsxngwmBJGO4iy7coi2sKG4EZKQ1j93Q6Uezw5RegMT0ApMsa1AyP47S4m7xHEDRXQ2t5fKda7kQWMvYgGIDYHfSpdXOqjKthROMg/ns+rQh+zTBNDD5hQQPO9QCUCHs/7oEEWeCu0zyzO4UmzUJs6aJl8i3LskIgUdHBBOAs7mQ/TCtI64BOsTxgzl0xjSDKJHiPMOlrbzfYJrDyVg2IwQ/nt9mRBywXBb8pe3ieQZkRge76GVX1WkzWgRgNBey648XTCniYERgRLGOZAqJrBkqC/kjAZfT5FI0tyeSVlsy45zld/t8TiObMSdm6keDbWaAWfPJ4RK5/er08Ip9AWA2lEHEM1LE2xOpW78xr/Lqswu+UuhznuHQKSAkrJTJOz/tTO7cWH3wH9t3fNYWLg/6lXrjNs+pANMYUcoU3E9ET99StbyiwPN1jQFCeemApxJBMgKlTNn72t6OjXc9U/Ov8KyAL2uNEy7nYDrALUA3BX/ptvPvnz3DtSjb71cKSeWdg7mnjcmImAJ8o77r8Z8++cFfw+V5/nGZZa7BHE6DdTV2xzevuvDmhaXB72YsaTGCPpKJSQlBZMNaT1Tvv/PZrU314Rku95jK5RJMmWAsaWa/PL+w+IGr5q5njLyQQ0qN6D5SgusaaW096XeuWrttTTji80wrMLehae37u3vW3nbBNUvKw8tNLjkRBreu0eKCbaZCyvSDf6+/7fXd7S0hr+EYdRvIpWIOQ9O6mmK9z92x9J6ysOcKS0iucrnxRDoJCI2IvVLX+sC6LXXbxgwkOQXTGetqjyefX7n0wUjAtcgSUhDAxgnGdUba1o871j30uw9fCZfmBbMmHzlByTlYS09844ql988MuZdNBEwV6ypC/nNP9MHfvFxbHS4aI/TnFEyZYnN3z5obFv/gzJmhG63x+5gtqgTMp7d/vOL12tamkN/hnDY+ZkfFeDZzxYJZ5d9bPOdxEOyyfaxL+ZdOxGKp7EcrX3j/br9bN8bMTXOpmALQdMZi0Z74mu8v+dmssPdqi0uLRtnHbKEkJDFi1ftafvH71xveCxW6vXysDCTXYCrpzZqCF3gN54qvnfOIz2mczqVUQeDIrGNQRJWRkMaIHehI/PHRP727KZQf8I0Jpb6dc7AB1RJ92cyccCB43cVn3B30OBdKe6OWAx0iRUkgUu09KZo6Ey9s+FvNC6GAyz3uEmYqwOzsXmcsm7JMzjn/5pJ5l5flB65wOfQ5jMgHSGFx2Z1IZep2Nna8/PaOhj2hgoCPq2J2vN2VqQAbtDO1YSvIWFusF7qGoqK8vEjQFbA48UOHe2LKF50+j9MXdLqzaUv1L8d/TQWYCiDKpOKJdB9MLgoLQ8GQ1+ljmm5v1pZyIkvynkw21XU4rtp+8PtcHhVVx+VfU+FjCirRm06p1wvOnHN+STh4kddpzNU0lkdEBqnen93fhMU570pmzPqWrp7t7+3Z/4GC8ik/Gysi5hrMDvVdifjC+eVnzT2l5Cduw5inAoTqlqjY0V+Iqb+DKX9/AFH/T5nmvvoDLU/u2tu4OxT2+ceEy5Up2lA9ifjFVWdfOatgxkpGpKuIZ4fC/nqLCSFjFhftusYKGaOQSqNUtFQqMkZMSGl91nF4/ds1H70aCo4BlwswzdC0WHdvz4XnVn6rODxjpZBDFfNQt42ItFgi8ec3X3r1ocuuufK+kM/3bdm/vx3ZkSOVL7Z2HV7/zs7av4TyAkE+UiI86WA6Y6m+dOrU2bPnnVo6ey1gp1GDbYChjZiImGlZB5pbWteXlhSvNHT9FNn/AI7M/ge/Z37afPCuTw8e3Of2uNwYzucmHUxlGlnLPH9h1cMOw6gcZrEKTiiwvky6+t0d7z+w+LxF93ucrktGuzdrmrUf7Kq51+EYIW+cVDClViLVd0pZ+bmRSPHDwyg1qBi3TbEntmH3m689c9ZlX70pFAwt/5wpHrmH2cpFo633Hmhq3On2uT3HqDapYEzTUvF47/zKc1c5XZ4rR1BgaMHRjpY7Du5v2DF7TsV5kYKSX4+yG9sKZ9J9r+6t3bnO7fcHID5XdE4qmN2Dl6K8ovJRphlzcazPDJRZ9rnV4UPNDcuzyZ6YwxsMzSyt2EBEM0ZQWYCICW7WNzbU3qmBjq3CJw1M/bAQ3DDc7kjZ3I1ELH/URVrZukMN/7lH0w0Ht8zszIpzHmG6Y8HoD0N0RpvqbzXNVEpjTDuqRptUMItbhtvnzyupeApE/hHAOIg0qy/xh9aGbU9488tnJjsbDxVXLF2he3zfwdEhf9A6+6OjlPHuloYfmalEXNO1ow9GcgHmj4wKZi9SCt6UjkUfS0Y/qfVGTqt0hSK3E9PKRngYQ2Dx6FSAgVuG4fO7Cip+C6KACutHtLCPinIqGEgp+yQ3d5NmnEVEnoFgM9y4hQJjkLI33dHwY9NMxDXkUjFwixk+vzN8+ibYddbYXd+h5HD0AmVQsUSm6+NbxJSAMbdfzz9jnSQWBKQqE0cbeFELHsw2xriPdJKix+r87yohUjlU7IgnLh0eNyZjgodBUrZPjWcce004eAycQU/kqFadBY+/9J3Ynfb599hg4ziqneTD9YlhjZ6bTORw3R6HqKoo2i+BohNpHIKAtpqGtjnHjEMAGHaApSMon8lqbHg7/8Ie//H9kIMLd0EP3TTaAMtRI0dfun77mRaEGjkKnyAjR1062LIPN1+0Z7iRoxN2SEwSVu96dune4YbETraxvl/abaOTeRBzJoBVJ9no7Do7tTlJhp2zABoGh53/B2qRqWlaSOhxAAAAAElFTkSuQmCC',
      name: '刘继刚',
      dept: '党工委委员、人大工委主任'
    },
    {
      type: 'zhz',
      title: '指挥长',
      icon: '',
      name: '',
      dept: ''
    },
    {
      type: 'zzzbz',
      title: '专职值班长',
      icon: '',
      name: '唐丹',
      dept: '行政审批局党组书记'
    },
    {
      type: 'zby',
      title: '值班员',
      icon: '',
      nameList:['薛  涛', '何  源', '', '']
    }
  ],
  // zbtxList:[ {
  //   "总值班室": "",
  //   "值班日期": "2023-03-03",
  //   "值班室电话": "",
  //   "电话": "13981890866",
  //   "值班领导": "刘继刚",
  //   "职务": "党工委委员、人大工委主任",
  //   "值班人员": "薛  涛,何  源",
  //   "街道": "东升街道"
  // },],
}
import GlobalData from './GlobalData';

export default {
  //公共服务
  ggfwList: [
    // {
    //   code: 'ywtb',
    //   name: '一网通办',
    //   icon: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB8AAAAfCAYAAAAfrhY5AAAAAXNSR0IArs4c6QAAB6pJREFUWEeNV2twVdUV/r597iXhJqESi1AKtIKUhzD5UbA6Io+WkZf0ASUZqaIVVLSEtI42CFWu1ZExjA5NBBpACpXKEAQHsUiNNFQBmUCwlAEVtVahqDzK4yY3uY9zVmedu096ExLqmbk/7tn77m/tb63vW+sSnTw1Is4MQEh6n4jkJv6DsTSYQuAGAH0BfA1ACEAzgNMA3gdQ6wLbh3Tnv/RYe4ZHUjqCYfuXIsLNgCkm3cNfSF4ohNkk5gIY0lmg7d7HQLzipfDcsJ48LCJG1/US7X/fBlyBdwPOODJ9+Kz8ACIrCQwEEESuB+hvgk9wnq67AHQ9bNddgazKgSkf3IMxZUEvlB1AK7gCZwKkd+BLN2rAxXZjGoBG79+gk8cVwAnWmB0I8Z54LBnRi0faB9AKHhUxUdLb+7lbaYSlAL4KqOLpPs39MRpuEHg94PFuAN0BJADkADhLcOKNvdmQHYAPHrz42wl3CYAFAFL2wMtqwt5O6VWq9RMisDHimDkjejOu63UnpY8jUiWQHwNIAuiSKUozakxffhhclAFw7afpYgg32dwpxR0BK5hnKdYAjBFUjr/WKdO0NQChGCBjAVcrvPYT9wEQz9szwwLZk0454ycPZEL3+wA7T0hhOuk1APi2LZrO8iuidQGcJHgM9DZM6R96MVuWep5WeEMDnBEjmNr+sTuXwMogBQIsnDrAWVJXJ8oYsOW4+xtAnszKX0d1pbcmRJ7oakJLNHrdpBQuzvhBGy37TDQgpAFsPZ5eJ8Bd9tBzrjhFxYP4b65/T64Oiau3/pbNYUd0q0QcIarvGBKaq4B60FCA7eWTHXWQ2w0fSh+m3HcBfN1qfvHMIc5v+cKR5GyCa/4P3RlwcNrs4aFXonWi1e0/Q89Ajs2AqFI6okvpHTeO6bVHUk8DeNTfI3g3//3QSK44lHoVxNSvBE5UnikK/arwI4TnW9pbXUYLqAMbDW7/+0Opmzxin92fImUUnzuQOgXgG1egPJAUNeMQnAFwwYPUEfi7wHzmuskj5TdGTkKEUauSgImaGnGKi+lW1EuvENPHART4l6eU8en9ycAy27PmW6EvK1vinbibb60iqFp0U5eHfAmRMqNGfMe73qZkyd7kSHFQn8Xwaj62J9m+46jPqqQCuZ0lcV4E37S+rfnOLspWvUdHhX/52L5EfwV96ubcjzWQhXsSAx3BBBrzkJWyXz8gdvGR3YnO2l29MVzJZMv2c1cVXOrR1NwrCZNriP7icb4AwwFcIpASYLB1sRMArslIElsEyCXxEwJGMm6oQfvBAniHpW+2gmcWKWcc4YPLxue83FkX0fd310luQTOkajITpbsSmyAotk7m052xL59CBVO7DlxTv3eB4A3O2dmiNOiC3yAIrl09MWf2fQcl3DsGiY71WyWCQjq6GfTzqDch5c6/xK7JkfBRq2Hd63dHLRM/iMtv4NNOopp3/rnlMxF/Mgm6044Nt+VOmbRDcr5Xj1Q0Sk+rWIHaGEhUjK7N3imFyXTiHQG+k0VpnMRfBbxAeiGqRuyjTIhhBIJV/Om2lhpAZmSBn/PS3vCt0/M+jyrA4swNrxRAyfZkkeu5B2xBthhgcs2Putb5v9fgO3pUFVNfjt8u5Es2at2mxVErwDqQB3dMyz3+w21SkAoj+Xo9Usg6TOW0uZjulK3NT1DwuMV48bXpXWfpWjNbbhGij4EkLXfKgOOqND23jpM2SLdUuFm9/bpgXLLFQhGcE/AgITcLsGhXSaQyAFSgsXUS2j2O6Vs3Na33wFkW/Jk3SyILvr+paapDvpppcm2lScKIYL9fD6M3xh8WwdIs6rUo/EEhy/kuup48su+OvNV+CjIl7efylpfiuwGMseAVb8+MlI/+U7xMiGWAOiIPCTxDGJVlP0KGATjvHzK2RvLjLc0HARmULZeMUuDRDoYGeG3/rLypCj5jM4xSPnJ943iStVZOYYAV9bMi5SPXNz1IYjkEz9fflVd6XaXkfDSfiRv+2DgB4E4AXzCgsWhd460Qvm6jbz+dZrqacOY/7ols/G61hPt3h6fgRX+ITwNkS+u8Jqw4fE+kvGhtfB4oVTqrwGChcU23lOOeNy6mkNQOd9q/eRDA0DWND3vgUgJpa6+BxQbuFBfxHv3g3oLK62uky9FiJgetaZxI+EH7s5qAFR/MiZQPWhMvI2SZPau1BVvrVtyz//MAbQTFdAdUNy3yIE/Z8Vdjax2J/fwTdMHJn96X57M0oDo2X8DfZU2qFf+8P7+8/6rGMojmnI2A6L8ZbYkpgtpBdVw709aAbAD9Vjbd60H0wK7tgvDncwKnPDHTDWUoKM9CcFWQcyGeOTk3f0HfFbFfgNThsebEA/klAVP9qi+NEs+8fTm4hmcD6LkyNowulwswOqvig5FZ1WhUQJJxL/2of+cIsPT0vPxf96yKlYKsFOCtsGd+ljDpiOPwgriYTnAFgFMdz+U2AK3qHsvjkzzPm6+KFKDrFZqNX5RG8OTZsoLHr66MzROgSnOeZfGBfC8amts7+1OQYeAYJHC0gqpLgx3PTBDIGAhUpzqR2H+p8iUN9xPyBnLSb52/v/Bi4bPxvq7jbgNwrZWvxq37T3uO/DxW2m3vfwEtobglrgXd4AAAAABJRU5ErkJggg==',
    //   list: [
    //     {
    //       name: '当日办件',
    //       value: 1023,
    //       unit: '件',
    //       valueField: '',
    //       unitField: ''
    //     },
    //     {
    //       name: '平均时长',
    //       value: 8.6,
    //       unit: '分',
    //       valueField: '',
    //       unitField: ''
    //     }
    //   ]
    // },
    // {
    //   code: '12345rxhy',
    //   name: '12345热线回应',
    //   icon: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACMAAAAfCAYAAABtYXSPAAAAAXNSR0IArs4c6QAACdNJREFUWEeNVwlwVuUVPed7/xKSIAFMBYcogoBExAVEqCCJggjWVkFgQKhlGagLjgtWcbREpwVb0ZkWrIIFWgWhQWmmUhRECYgbggx7FBCBsAbZAvnX953O9y9pUGT8Z17ey3vfd9+559577n3EeX6SzCKAQ0g/u2zXMTWRh0JjkfALcKgdGZNE95yk3HnfPjVK5KFdEriSQGsYNIZwRAafJi7AhivJuLOd2WOztlNGzvUrl7zBgHUv2H5crWExCAb9IHQE0BRAEsAhK2wxwGsdm3PZ9u/UjwaDJdxI4FIAjRraVnrPVwDmhYBX2jbjSQeKZArQD8A4LxcBxrGxrUYtk7BlpBmZMew8z+5JXRM4TGChFbqA6AkgdT9FUXqxu0wA8DKHe7FjZZc1uPPqZtySBXQuMCmk6w9rPKGpIJpC9Qad/ZB7oVJRwSZBSyDexzRb30GoIXFUQBTABQCuyJzdXgfMHS7sQQC742CP7hfxsCPhLDBZhJ8d8KeQnCTAeWGZ9tR549YfBfEVoe3WIpfkcBKfSaiGyw/yxe4tuNCF2bG79ohayMdIQZMy4c2yG3eOGWhat5be4yulQD0Y908pmVy9Xw9Smq40tW6BFeBR2E2PUxJARenFPLpqb+JWGm+JIe9O+ra7MZyUMbaiVyvTN5tz2ZCvrtZvAM3NsOJClg3X3kiAxf1a8Exqf5lkykj73j5dHrB2HYAmGTrdBk/g0oDlPaWX8USKTlIr9vhbAEzvc6k3c0V1ohS++TCTJBuPFZkujpVMlbn1dsUeuxxQ3wZgsgxZGHNtnyJuYtb4so3KS+ZrHqg7MxscUEPo44Kg1/fnRYws3aHwgHaMLd2t7pR9pH8bb6hjoNFBhE3MboLQVtCaX7QJ9GpYCO/uUk/RfpRJZGfXHSlmUqwbc92A1tzALCuLd/hPQHoaQH62IgDEbcC7/u423LRypQI1JZDz+O0dyZG0MAM7BP7p2CjfpUsCvu8YLRT0h0Htg8+49atKUqVoF+9UJ1h/rdKl/n9GnLNEZc1J79bxXZlIhenNqlhnKjBf0OUAcjIaEgAwd3jHwGhnuLSUySzwBVvjN8h43YZ39Ka7e+2r/DkA7gW4BcaUDu/Ao1nGs+cFW+PX0vAegO1BeBCqjcGaRrXe23d0ZV19Nc3dkniewu0COmWQpyRCQsnozsFV5eXyhgxJq/DMdQo6L+Zs9R+0vjqT6kmgI4kPfRMYPbaYe9yapt+kwgC3r/5FGYVuKITO9rZCsLgG4mubdVHCT75FIgdC1wYJdiCUE7hizBWsrZf7tH4r6+0cx5BMq6T1t47vHK46l5K7tc8CdOHKPndsTgZcJWS1MZ37L30RHwNyApTSj1tcnmSEbfmjXYP9sqHJGnrxi+gvfZpqY/zvlDDXJeTFgibpQdoSCOUcVzJ+m7UMwygOmf0TuwVXT1unC2njt0BpthJeaLnnJ7sYqJUlPDHl4mlO+TQ+H8SloGoh3tYAzMynuod+mw3L5I/qLgkFA28Y4CYLTRUwygPzSeyX0EHgYkAFAG42xHYJHUXsmtQt2G7q54n5AIZldGibB3unD7MeRJLAN04MJW7h7z+Ofw1pp2AihAYCiAEIA5r6XM/wU+PWKTirKxPPr1OTSDTxpQWaSBwR8OzN8PVqEuZ+Eo8BeCRguDkudKHVz9w9ihMFRUi8DGGuiFGpe7QHAc6XsN4CazzqZAyhaXy8MnaKxGpJbsHYLBiRM6b1Dk3Ignl4pQqCjO8HsPqFknD/h5ZH2wVDZqaBSo3h9EZ+8NGyUiZ/Vxl7DsAzFthpLZ70DN4CsFxgFaGHBAz0XAhh+tHqtAUedMLq+7aYE1bEIgA2C3iHgDOUzZl3p/cJDygrV6hsCOMTPogNg/CmyNGwupjEHwBEAD15wg/PLvDiC0A1k+8PoRf4QMIFBPYK6C6g1gBhASFCf7diI0HrI9a+led5ewGtntEnpzfHvhc9kq4RjhO1KNPeXec/bo3tNOfWvAMu6ca8F60gdJNg/kToeceg0kfUg54WOAXAhQJOECgw5CTr+/NovCaAegl4xQAvC1wt6F8ZBU6pMcVhr/UPL+SIJdFNAK4ScQeFJ4DUTOKaZBDCY/PuyHlp3EwFo0WJTr6UDDF+KGaDjR3AgMd8Y2nOnAluCzVOXO3JOjABWZ2I1eV8siijTaOW1hYmESj2YjkbUYCoIpEugMmTEPLBPfNvD2+G63mDKiIu04cDfAFUFYTZ9WCAY77YreKunF1lZTJlZbRuk/PlxybE+vsSSyrhFdZAWVCDy+W5619V1BUZaxr9e2COK550kTl+BiyOPkBphhuMKPYQ9QaBG5TNHeETG63r/+6I5qcGlyt05bbU6HjWz4F0YLcWp6fApsdhvm4PVZYymX3ZuFkIzBrPRP/y2kJ6wYkhE55acRdPZAGm8PQrj17uy7om5ia1FylvtuhvzExirnkFQXyejOPuyhG51SiTKemdGrSQ8npbqvFhcDFYUwhWroLFZMh5etPCWEfPJHNXDslb79b0X6pwvDZaYWCnvz80b2lDICkw7k+v+XULBQx11xJ6GMOwlT5kerqLgQi7cdJKZbEzuf9YP5515wxXJoQ3LjhzsfX5sEfFEsp94fMRPHXjbDVGTmQNiffXDM+d+H0g9WCufz3SW7CVGYU8FInpmryQin2apa6LMz3Vu8duQtsr4T8gP/ahnUF5p3wvGTZJr6mlrhbsIENznYH+mluUW+ZCdf3c09dYj0sMuOOLX+eW1uff98JNRzvKaDvPPb1Q4NAMoH2xpOndJJQ4FZX3qgSnzOnvnLMNZAdsNyS5THRD9ZcwGLvx3rwNJXOVc5SRcUb6C4h1Mb/u5q/GFNb+WBHUg+k0p64oYbUWQIu0BvCMD/vAzrH5b3R+PXJZPKFxkm6D60Ouw2dwOdYEfGuIZTCaWTUqf3NJmQLVrer6e8IfQVwFYFMyYfvuuq/xkazz56rGtKPl8jCEfutZtb0pLst0bdfEgiLWWl9/Zk7+f78dxWiXmQdyTyK3pWcCeTA25ocTB3eOaH7KmWk5I3JpOOi7hngvgA4ZwO8Q8ZHfjG928nxAzmY9A+iSV0/39S2cQqa+GpnOEycsJwGst8IGEjWQzoAuaiwi2AbQVQTapgf4lOnTgKbsvy/PqTXwLOjS4Xz6dHYKZABd9LdoG9ik+1wZ0PAT8hw5k67r9C+71PWhCsmbfHhCo90p1gfD/hSh/OG3doPNzWac6iFxGIVbBFwGnf3tnAGRIHEQQBXE90Gz8NhDudWpJF0E81OBnMvRtH1XYalzhtYyhfKbnG7rEZ1JtoBVc9GFibut1T4TtlUn7y84Xs/R9/efLzYNnv0PhT4iqLnIitEAAAAASUVORK5CYII=',
    //   list: [
    //     {
    //       name: '当日受理',
    //       value: 207,
    //       unit: '条',
    //       valueField: '',
    //       unitField: ''
    //     },
    //     {
    //       name: '当月受理',
    //       value: 6217,
    //       unit: '条',
    //       valueField: '',
    //       unitField: ''
    //     },
    //     {
    //       name: '当月办结率',
    //       value: 98.30,
    //       unit: '%',
    //       valueField: '',
    //       unitField: ''
    //     }
    //   ]
    // },
    // {
    //   code: 'dbhs',
    //   name: '低保户数',
    //   icon: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAaCAYAAACgoey0AAAAAXNSR0IArs4c6QAAAwBJREFUSEvFVktLVVEYXWufczW6EZilZg8wCIKI7N4swYEIgdTAgVGzxEFEg/5AIxWa1qToRYTlILCIEMKkIAyih4+0BwhFozIfqVBe7T7O+eK89Kaee89FzT3aB7691rfW951vb8JniYgiaQ5NShWJawR2AjAB0O8IAAXiBwVndxew28NYKn5JEBEhSfk0JhugYRDAriykHrbY5MBXGNi/t4jTHtZCcj9iW+3guERMkV4AFiDFX62NSyfO3TNaXsR3fqptYiurljTQakDVkKnXwxIlpcezl0AHKKMQhNxkHD4iCWGxAHUurwVZUVnKvucierdTIns1AZadwmYR5X3M+eXWt/u7HKBIn0ssGhip2s6BdBXe/uU3KTcg/V6skNHqbYsVeyJtxV0jEpYktpriWKUTCoKYiBFRwg5PsUDqaWh90BBOmY4KXUHBQEw0I0rwoafYpNSRWj+IcErcWEJJCMO1JYyx43OiEkprI1AGILmga6265s3XDYk0ixf1S5ZYS1RIgC8GjQa2Dxl9oETcrrU6cjWX5ZLF0cM7H1PjADZnUpKWyVzX+mSX/pf4xVoxP3l9IDkKoMj7ZVZTbhrHGC/1JkcAFAMwMkyllcrHckEDMMoLrxLpVvuNw5Ukdqw+/yL+nkAhgFQ2xXRc8a2dOGoyLeusLsBETgrP9EoonIA+Ff+XvCAfjOUhdfMgrd8x0OLpzni9EOvoDg+/U0Iwqce77h7ZOIFmUWhyyVtANNNseParMJTKrw2CQ8Efnno8OwmwIEhXa0odaj2a13OiXbT7J2nZDm/f2JmoMEzzbQCrrbtkiscfzQ4DKAlSYwHPGTA/6FDrTVPskakUVQrmjAa1j5ArAYh1ACM89mB2DMCWIIoDFS97c1l9Nc6aezO5EM9dbz742Uaufa/bxJVtOREvV/Q8cXlrLBfFK0e859YaEZfdmF6bS6L06rQ3qzM9XZdrsXfee4X+ZOHl308I1P7PhwCBp9x0cWaHqRu3BTgcZIgsQ7r99CHwRlFr/AtUxF7lA6L0NAAAAABJRU5ErkJggg==',
    //   list: [
    //     {
    //       name: '农村低保数',
    //       value: 2033,
    //       unit: '',
    //       valueField: '',
    //       unitField: ''
    //     },
    //     {
    //       name: '城市低保数',
    //       value: 1709,
    //       unit: '',
    //       valueField: '',
    //       unitField: ''
    //     }
    //   ]
    // },
    // {
    //   code: 'ylfw',
    //   name: '医疗服务',
    //   icon: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACMAAAAcCAYAAADr9QYhAAAAAXNSR0IArs4c6QAACCZJREFUWEetl31wVNUZxp/n3Lu7CSEa/P6Y4kdBsCiOEGgJKCRFoYiUFrPWOsURLARwlLa02lrlRju2DihIFSa06EBV2kSdYkWxoqF+JKUiVNNkVFpRitqImNgQN7vZe5/Oubs3bJBWp+P9I9k9ez5+93mf9z3nEIc9kgzJ4I1OnUlhJQDmuwQASgGsHDqIjzVK7uFjJwEi6b/RqVkUFgHosuMJGAG+TyweXsY90RqHj48W6muX5NgJX/9QEwLgeQDKA2UBuBRuG34sb7EwlaRt63uittcOaIWIxQDCMdEcBrhg2DF8IVrjU2HqJSdJ+q8e0DgEaspPaKHtxAlBtecd73g739VpjoOkDHwLa4CAAR4690S2v7LfX0ZwCYB0AYwLw4qRx7I5WuMzw+xs17gAIYwNTwQTA/Wz8hOcm3e0ayGge/OQBoAN79dGn8AtL7f7dwn8HoDeAhhjwIpRJ/4fMM3vaLyoFw4Pk6Dbx5/i3NT8nhZIWg2gB4ADIEbxonGncmvTe/4vIV57eJgoThh3Kl/8zMpE5tq6V2McBM8xpwwk+CBKBdxUNdi5/dm9/kICVhn79lYZhwounnRa7OnGvf5PAdwGoQuEIUABxoe5cPJgvvSZDexJxiMDSdz2Fk502JdN6DFg2kXnpafw46feys4JxHUgslCojE3Cqqmnxxqb/qnig8KgoiA0fzjOZOF/9Qy8T1LRGv/TM42NcisrmX10d7aa4I2CPmIum+wfknAhHAzgzCsG9mccjHX80NjwCc4cwqZHXs+McYxZIaBXNtULx4NHC/rFN4e6DdFahUB9qe01yvUqmX3wtWy1A2zM+0BQKHFYLPJmNhL/5hgzNTmM71gFawFaNTe+limnzJMgjou8pvAtwrFRifB94Iorh7sN0ZoRUNgtarz/1Ww1jTZKoQdsWhYB2GbAdT70awJxABmb4gD3BAiSc8+N77Bz/Kqld5JLbIJwVH5sAgjDsliQB+GsfHucRKCAV1w9sj8Q63YoNr+cvat32dBoozVa3pRxATuMcS9ZcB7fX/PXbDKQHorMSKDbGH43k3I2p4vhl6r3OwG4AgqB7RwHXGDGvPNjTatb0iORNU8AODVKd5sYAq9YeP4hoFCZZdvT33AMG/IgvmylBXaKsSlLyvlBfaviyRHM3PlydrqC4PcEOgzM7EDBjwEME5GGsI8wq8GgzqrnGky+fnT85VW7lbhuKNN3v5Q+Jwv8EeDJ1mJhXQL8QLp8ydjEo9bUrG3KLDLEqnxhs2nsGKCx7KjYjEUjeLC+Xk4yST8K5R1/7q3MCI6gnxhgHMADQOAALAuE7TRYH3fQesOY+F+iMdH/ZdtTZ6QDZ4sQhiys3BYq8HHN0gnxdbzp+fQWKfTHUABnA/jIF69TgLby/bFdFiRnPxGkbvxTqsqBm6UJVvmBHvj5xKLl9ucfPtc7Mc7gCRBzOnrjm1dX8qDnyXgewzq16gkl3h3QO9IwuBDkHWFdAnaB2GcANyssYUS9+Nn0zQRuzVdUa9ztK6oSX7ETVdfLaUjSX7w1fRkNGgLgXxJ/FCfeWF4V3277/KAxPTzrYw7J2aD2K+idfPfkge0RkFeveOdxmTYCX4wMLnDuyqr4ffN2KNbxJgLaTl6SmZotqVtF3pzPFmvCF+umFE2IQOY+lT7HpVoQYJdLLaYxHfdclGixsUYtYBVY8FRmtKDhgu4EkOrqTYx6aDo7LKxdEAfSrzCnvs1ImyAL104pWmN/W1vOXk5qlLutktnZm1MewKV9MGTzhmmJiig8Xqvie95Kvy5h34bpRRdYyBFtUBSGqJ9dePbmnjYDfHD/tMREW3EjmJ72dIs1fMEL12y4pKguYuiDSW5KeQIOwQjNDTOLKyJlZmzqGRKHdhPY2vD14oui9oKDUOipMKybUhZ698Mzi6dH/ebVKdZxUk8/GIk1D888Asz0R6wyBTBA8+OziiuimF/yeOcgJ130tA+dKQWPUrxvc/WAptF1ipWeBVl1p9Z/PIHkaYa8x6G2PzareGqkmO138vE9LdAhZQjW/GHWEWAm/67bU2GYgOZnLh8QhsmrBW04Lt6gkqAo9YyBurKB2WaMfrM1OWCvVaPqt6mrDTFW0FwDbXNLB1z65DSmo5exMGVlqf5hkmqe+VbJJ8NU8WB3f88AzU1X5mBC+QugvvxAao5DrSMwKwu9Dbkph/4tEiYKuMxJF7/64lx2hWPtQ8rCJAb2hxFV0/ztI8CMWm9h+odp51UlFfBksDS3c1c3wNgUH7/+48E9kK3YY/Obp/3ZOOAdL1014MbQJ21QOK4WhMdgRL3iRanuFoG24IXZBKBm51WFMJ7cbR6z56yzYeoH09Q6t2R8YZaEYB6Ds+u6RzERpB3fLA8Iu7OfROp5ZXE/XGVa55S2FSoaKdPjWmXUl00SatquKamblGdg9GFIXZcHFqa2mv9+zcDxIxoQK+rIKdM+CO6+JFND1nYtFLgQvqrKzMCwjnSyawxpHpOCGf+Yd1TTkFVKHJ1A0DMI/BLgv9mBxEfs3oXcVhAqQ7Fm9/wCGHhy4TE7eE1XZODoepES8LY5dG+yR0977rPbw0AAXwDwroB/h0dT4hQIZYDeFJgpHJc/ttqxp+fDk7v2gDV7F5TURQyMPpxwb/dSStY30fUivHwdfjTMf7dnpixzB/Gwj3Jn5YC5e9IRn3wfq3J47QE4v/3akrWfgDlmVVctgFsKTvT/bc7Poz2nvjD/w+tL+8GEpjz6ru5yGb/W7uifx2qfOgdB19cNH36/tDVKjP8ANy2UOFBSWNkAAAAASUVORK5CYII=',
    //   list: [
    //     {
    //       name: '医院数量',
    //       value: 38,
    //       unit: '家',
    //       valueField: '',
    //       unitField: ''
    //     },
    //     {
    //       name: '医护人员',
    //       value: 3092,
    //       unit: '人',
    //       valueField: '',
    //       unitField: ''
    //     }
    //   ]
    // },
    // {
    //   code: 'ylfw',
    //   name: '养老服务',
    //   icon: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABwAAAAeCAYAAAA/xX6fAAAAAXNSR0IArs4c6QAABNlJREFUSEullntsFGUUxc+ZmW2BUoQgCIgPDJBYIZWwJIpGSqKIUSwaqa+YQKJsbOozwRhjwqAxisSAwBraYNRQ0LRRoiiKPLpE1DSAhqcoQfAfwAgotF3b7u4c883OlKUUt1u+fya7M3t/99x77p0l+ngkWSS9Q3+rEh6WkBgHQEE4CvjJ8lA9/ko2h8+ae+wLLwyw/7SeJ7BU8kG5scLP7RIemjiMXzVIdhWZKRgY/vDnv3QvpS8BZAKY1S15870N4KzAKZOG87CfaKEKJZGkdp70mgBUBEATuKeTAhABFZ9ylV3TJDkFAcNSfvenbrDSOgCgX9C3S8XxABjlvxaf4MRolKmCgGE5t/2h20hvRx6YURz2si0Fa/yMa3m8IKArWS7pfXtEUc/ydgY17G6Y3NKG987AscbNvIZnegU0fVuUgD2yFIxFmVp/VIOtjLcPwOjelFTQj7PHOlP9/uczTUOD7Dlz4BmjmGfDsjYeSr0r8FkAnQCKLmUaARGKsaob7bqmpjymMbCqKmbW7tWQtO09DioK8bdWWMuHAUXtyDQDGAsgBcECu2bRmMUkGCGw+UiZPdO04n8VhrDavenZlOIkRoUjIOH7yCDnzkyyY7SXsT+HUAYgdGSXWWhhk9fhPByL8mw4Tj2WNIQt25V62bbwprLZmkE2xyNQJGFHsZy70/1heR2peQAfFHA1BA/EUYKrT0+y1y8M1l3YkouAIWxxc2cNiRUA0sEs5W4S850DYSv7RSoXlLPNKIjthjOqBXKn09yHcbV/JY16/1wADGGv/ZCaBmirgHAVXZSYgDQNFNjiFUUq3SiT4diYa1kjaPrf3UznA0kEKXeDBnQO7myWMCHP2jKxzOaICNzS7kTuXzqV/85pkN3YA+gihW6THFOKF7Z1vALgjaCURkG+4+9LkZtZHKk0UNeV5br0YET4dcyO1PmSBuqe3Jgc7TjWbgDDe7G2chPJKhW29C8t9pXOr1Wkbj7SftVcWQfKQKPcz6CiSU5iOtPzvu54VdLrOf3Jp+5CKBHxgG+SLcWVjVXsNKDj98Gui9JUAUYxfdmkZm3QgBK17wkGOXemCoGmQd+9DRlwqw3EAJVK+J0W3vpkVv8EwyZXfpp8gBY/6zbAhcACEWYN+q+k8IQLvMXLeNO6gDMbk+sAPNoLZ+ZNQtnlAIGxCLk9La/Of1kTS/we3r72nyGOVXQQwIgCzZKrwrTBvPlDRR1gJpp4pHT/tHXJZ0gsB1DvA2+pb7uH4MbLKWdACVegI+BYPys9JfHYoFO3rknWILu11vjAmz9MLraolwqYve79OSbhMIi7glk7ZwlP7Jpb8oX5PPmjtmoBcYBZhRPeb90u8I4+9C908z6UDohayeREeRgpateBeQNPTq5VZHeMqZtWt1WDiEus5/Xx1hGRIuyHMLQP/QuBvwxSSbkJHo4ZXFkVgJVwmR5b11JNME6hnmNqW2d4wqa81uv5gRB4cMzJkvLEQmQqFsE2V3+duXLgMn3dqpZqgHHfNCPfa30Rwjt9KKdJIQsUDp44VVJugncpNHcD4Ih4ViGINRy6svUDSnMvCwjsOVUzcJKvKthcfkEC4NAVBmhMg485ZFnr26IW9HEk/L/zAladfa70aTTIRu6rKQBesezcUyTrQKz8D6r3YB7RzJRDAAAAAElFTkSuQmCC',
    //   list: [
    //     {
    //       name: '老年人',
    //       value: 28.7,
    //       unit: '万',
    //       valueField: '',
    //       unitField: ''
    //     },
    //     {
    //       name: '服务设施',
    //       value: 122,
    //       unit: '',
    //       valueField: '',
    //       unitField: ''
    //     },
    //     {
    //       name: '服务预警',
    //       value: 13,
    //       unit: '',
    //       valueField: '',
    //       unitField: ''
    //     }
    //   ]
    // },
    // {
    //   code: 'jyfw',
    //   name: '教育服务',
    //   icon:'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACMAAAAaCAYAAAA9rOU8AAAAAXNSR0IArs4c6QAABa5JREFUSEulV39olWUUfp733u1uM82QRmYRhFqzhklaOFG3yMAp1vBHSmsp/riFpE77I0zwCqkRITPSdLOUghVOa1qaisyLZWksNGMD0WpFiL/th5t3u/e+J853v2/e3e7uNvvgG7v3Pe95n/Oc55zzXqIXj4iYMGCKgbiak5TmyzLY+PE8gBcN8CiAvyDYRcGmYYPYJCJUuyMi/suAzCKdvZkedreozsKATwGoU7U7KxKIXMMEEuUUTAEwCICuJfu5CaABwA7EceCRfN7Qven8pZ7dBYxuqAOMGiVH0nhBCo2xswjOBjA0CYB1HeoeBaWfnf0uwPMWUmdgdozK5ynv8J0ivtQzvA0IiZiJYZji4ltpOHZB8v0WZaC8AKCIgM+hJ3GgvuowHbMeKF0z+kcS9ses8CNj8fmT9/FqJ1th+PRcZZ8KJEQ6Ee4/K4F+gdhE+EwFhVMIDHQ2JaLWnGvUXuQ9SaALcLrABbgGym4rZvvEIThB0noZcSI7/LMMj/lsuRHMpsEw53yBCiUh2ASAbvXVC1TisqNOfK4nEZEfBNwepflk6gO8zj3nYu8bci6AHJeFntLQi7Mzmngs+9zoNOhLYlHFXWeizXFhATWvRAyAv4+puB1wXsCupOAncIBbGyWvX45dEodUCpDPhD689/+mJxWo12ucYmBCBj6KrK8ozFrZKeDNp+Uuv0QrBAxaQYECcmm8HeEmg+hMi7i6I3DBGFRJHEVCTANl2csjszfSU/JMwGp5hZoke2Ak/owVCQKYJIIAE6j6qiW1VyA+VS8Ia4ivAdbA+PZVjuKfGxpj2wCZL8CKFaOzNnRWiIJaE4Zvtda8U0KUdSciw0VYLsI54jY7t0RVW04fSamyLj3GTcU1ELthsW1VUfb36jp0RPyhEsbe/K7jQwDzIKhcVZRd9Z9ydZiqg2meCfH6T+gLyeu4M15iaCusyGQA/d08aCo7Z44kNUICP4L8sN1kffrOOF5yQIgYhGHO9werRzO68mikBuACUlasHZ9zi5kuShOhMrPsaOvgHOZF3hrP63C/W36w7X5kmTKBw9YTEIedBAlEO4CDFNl00gQawiVUBrG8oWNkXGyQRt6uKslt8ZhZ1tBeC2COQJZufCrn3bSNzDNedCiyGBYhS77nN9x7jz/rJ6XXiTIk5uLY6CgxMolW7rUGvyPK/VtLA80KXNEFD0aKhXzVAM+CiDIuD2+ZnNuie/8o6njdiKxNNFUej8Xs7LRgio+IX6Mq3xcJEtjijgKN/5RA9ghN/bDS7CYvjcmsLmqUrBsXO0qNlSU0KHHXKIIbFhhZOzXnl7lfdYyJW3vM7WkaXBaI1zKCmVEfCQpEwUQABBJXGb0OICpAozYqgCeNtVeEDFjI4yRfIlGYGISdFaiNNBIXFNSX5bbo2vS9bdNhWStANoB1nz2X+0ZGMKV1kSDogFH0OqW9anEmttc+PWaSPneWddJ1I2J9KDhQltviMV9ad3OBoRR+OSNvqaYuI5ji2tYgSQ+MRuc93uDTavImuX6nbSF1snuXr4iABeE5Cc04ulvtBIfQGlD/zwhm7MetQUFaMKltPvW2l9qB9ZwIYiw4Pi+3BQomRKugmkaAdTN1LjIzmMe2ty4EUe2mKZmZvgxHD2gbY2bEyQW5v3lgUp2kZyYk/nCIsYdqWl8hsRlw+ocK+HYeJxWiPsSMOLMw99c+gfGMH6xuHWNFDgMYwISI042A7gB6Ylct+Ql+42vPe/rcErZ7DbRXzDhGbscdUv33cImbGggmuJWhq1otCsx7O4Wd7vphgA8igZuVV+bf/U93QBLNL9PjCk2Zyh/UNs3CLgcw1m1WPaWsg+AhGrv+0uIB3zpsa/W4P3vSbe75XrtTfGiGqPo1qgEb20Yb2KkgxglkKED97aR60jReJXAWRJiW9deX3XHaOVR9uBWTKYJ/AR1yvMme/hVzAAAAAElFTkSuQmCC',
    //   list: [
    //     {
    //       name: '学校',
    //       value: 215,
    //       unit: '',
    //       valueField: '',
    //       unitField: ''
    //     },
    //     {
    //       name: '学生数',
    //       value: 15.1,
    //       unit: '万',
    //       valueField: '',
    //       unitField: ''
    //     }
    //   ]
    // }
  ],
  rdwtConfig: {
    series: [
      {
        type: 'wordCloud',
        // shape这个属性虽然可配置，但是在词的数量不太多的时候，效果不明显，它会趋向于画一个椭圆
        shape: 'circle',
        // 这个功能还没用过
        keepAspect: false,
        // 这个是可以自定义背景图片的，词云会按照图片的形状排布，所以有形状限制的时候，最好用背景图来实现，而且，这个背景图一定要放base64的，不然词云画不出来
        // maskImage: maskImage,
        // 下面就是位置的配置
        left: 'center',
        top: 'center',
        width: '70%',
        height: '80%',
        right: null,
        bottom: null,
        // 词的大小，最小12px，最大60px，可以在这个范围调整词的大小
        sizeRange: [12, 60],
        // 每个词旋转的角度范围
        rotationRange: [-90, 90],
        rotationStep: 45,
        // 词间距，数值越小，间距越小，这里间距太小的话，会出现大词把小词套住的情况，比如一个大的口字，中间会有比较大的空隙，这时候他会把一些很小的字放在口字里面，这样的话，鼠标就无法选中里面的那个小字
        gridSize: 8,
        // 允许词太大的时候，超出画布的范围
        drawOutOfBound: false,
        // 布局的时候是否有动画
        layoutAnimation: true,
        // 这是全局的文字样式，相对应的还可以对每个词设置字体样式
        textStyle: {
          fontFamily: 'sans-serif',
          fontWeight: 'bold',
          // 颜色可以用一个函数来返回字符串
          color: function () {
            // Random color
            return (
              'rgb(' +
              [
                Math.round(Math.random() * 160),
                Math.round(Math.random() * 160),
                Math.round(Math.random() * 160)
              ].join(',') +
              ')'
            );
          }
        },
        emphasis: {
          focus: 'self',
          textStyle: {
            textShadowBlur: 10,
            textShadowColor: '#333'
          }
        },

        // 数据必须是一个数组，数组是对象，对象必须有name和value属性
        data: [
          {
            name: 'Farrah Abraham',
            value: 366,
            // 这里就是对每个字体的样式进行设置
            textStyle: {}
          }
        ]
      }
    ]
  },
  //12345热点问题配置
  rdwt12345Cinfig: {
    // //颜色配置
    // color: ['#25a6ff', '#a6daff', '#0edf9d', '#f17272', '#ffd141'],
    // grid: {
    //   show: true,
    //   left: 0,
    //   right: 0,
    //   top: 0,
    //   bottom: 50,
    // },
    // xAxis: {
    //   //字号
    //   fontSize: 14,
    //   // 坐标轴名称
    //   name: '',
    //   //名字颜色
    //   nameColor: '#FFFFFF',
    //   // 文字角度
    //   textAngle: 0,
    //   // 轴反转
    //   reversalX: false,
    //   // 文字角度
    //   textInterval: 0,
    //   // x轴 坐标文字颜色
    //   Xcolor: '#FFFFFF',
    //   //X轴线的颜色
    //   lineColorX: '#FFFFFF',
    //   //是否显示X分割线
    //   isShowSplitLineX: false,
    //   //X分割线颜色
    //   splitLineColorX: '#FFFFFF',
    //   //X轴宽度
    //   xWidth: 1,
    //   //X分割线宽度
    //   xSplitWidth: 1,
    //   //留白策略
    //   boundaryGap: true
    // },
    // yAxis: {
    //   //字号
    //   fontSize: 14,
    //   // 坐标轴是否显示
    //   isShowY: true,
    //   // 坐标轴名称
    //   name: '',
    //   //名字颜色
    //   nameColorY: '#FFFFFF',
    //   // 轴反转
    //   reversalY: false,
    //   // y轴 坐标文字颜色,
    //   Ycolor: '#FFFFFF',
    //   //X轴线的颜色
    //   lineColorY: '#FFFFFF',
    //   //是否显示X分割线
    //   isShowSplitLineY: true,
    //   //X分割线颜色
    //   splitLineColorY: '#FFFFFF',
    //   //Y轴宽度
    //   yWidth: 1,
    //   //Y分割线宽度
    //   ySplitWidth: 1,
    // },
    // legend: {
    //   //是否显示图例
    //   show: true,
    //   //图例组件离容器左侧的距离, 值可以是像 20 这样的具体像素值，可以是像 '20%' 这样相对于容器高宽的百分比，也可以是 'left', 'center', 'right'。
    //   left: 210,
    //   //图例组件离容器上侧侧的距离, 值可以是像 20 这样的具体像素值，可以是像 '20%' 这样相对于容器高宽的百分比，也可以是 'top', 'middle', 'bottom'
    //   top: 20,
    //   //图例宽度
    //   legendWidth: 120,
    //   //图例高度
    //   legendHeight: 134,
    //   //图例列表的布局朝向。 'horizontal' 'vertical'
    //   orient: 'vertical',
    //   //图例标记的图形宽度
    //   itemWidth: 14,
    //   //图例标记的图形高度
    //   itemHeight: 14,
    //   //图例项的 icon。'circle', 'rect', 'roundRect', 'triangle', 'diamond', 'pin', 'arrow', 'none' 可以通过 'image://url' 设置为图片，其中 URL 为图片的链接，或者 dataURI。
    //   icon: 'circle',
    //   //文字颜色
    //   color: '#FFFFFF',
    //   //字号
    //   fontSize: 14,
    //   //最多数目
    //   maxCount: 3
    // },
    // tooltip: {
    //   //触发类型， 'item'、 'axis'、'none'
    //   trigger: 'item',
    //   //是否显示提示框组件。
    //   show: true,
    //   //颜色
    //   color: '#FFFFFF',
    //   //字号
    //   fontSize: 14,
    //   //背景颜色
    //   backgroundColor: 'rgba(34, 62, 97, 0.8)',
    //   // formatter: function () {
    //   //   return `<h1>我显示出来了吧</h1>`
    //   // },
    // },
    // //线配置
    // lineStyle: {
    //   //线颜色
    //   color: null,
    //   //宽度
    //   width: 1,
    // },
    // series: {
    //   //半径
    //   radius: ['60%', '90%'],
    //   center: ['30%', '50%'],
    //   //组件离容器左侧的距离。值可以是像 20 这样的具体像素值，可以是像 '20%' 这样相对于容器高宽的百分比，也可以是 'left', 'center', 'right'
    //   left: 'left',
    //   //组件离容器右侧的距离。 值可以是像 20 这样的具体像素值，可以是像 '20%' 这样相对于容器高宽的百分比。
    //   right: 0,
    //   // 组件离容器上侧的距离。值可以是像 20 这样的具体像素值，可以是像 '20%' 这样相对于容器高宽的百分比，也可以是 'top', 'middle', 'bottom'。
    //   top: 0,
    //   // 组件离容器下侧的距离。值可以是像 20 这样的具体像素值，可以是像 '20%' 这样相对于容器高宽的百分比。
    //   bottom: 0,
    //   //标签配置
    //   label: {
    //     show: false,
    //     position: '',
    //     alignTo: '',
    //     color: '',
    //     fontSize: 14,
    //     fontWeight: '',
    //     textAlign: '',
    //     lineHeight: 14,
    //   },
    // },
  },
  //12345热点问题数据
  rdwt12345Data: [
    {
      name: '企业健康官',
      value: 0
    },
    {
      name: '生态环城局',
      value: 0
    },
    {
      name: '安全生产',
      value: 0
    },
    {
      name: '城市内涝',
      value: 0
    },
    {
      name: '网格员',
      value: 0
    }
  ],
  //一网通办配置
  ywtbConfig: {
    // //颜色配置
    // color: ['#09de2f', '#d1c568'],
    // grid: {
    //   show: false,
    //   left: 80,
    //   right: 30,
    //   top: 20,
    //   bottom: 20,
    // },
    // xAxis: {
    //   //字号
    //   fontSize: 14,
    //   // 坐标轴名称
    //   name: '',
    //   //名字颜色
    //   nameColor: '#FFFFFF',
    //   // 文字角度
    //   textAngle: 0,
    //   // 轴反转
    //   reversalX: false,
    //   // 文字角度
    //   textInterval: 0,
    //   // x轴 坐标文字颜色
    //   Xcolor: '#FFFFFF',
    //   //X轴线的颜色
    //   lineColorX: 'rgba(255, 255, 255, 0.3)',
    //   //是否显示X分割线
    //   isShowSplitLineX: false,
    //   //X分割线颜色
    //   splitLineColorX: 'rgba(255, 255, 255, 0.3)',
    //   //X轴宽度
    //   xWidth: 1,
    //   //X分割线宽度
    //   xSplitWidth: 1,
    // },
    // yAxis: {
    //   //字号
    //   fontSize: 14,
    //   // 坐标轴是否显示
    //   isShowY: true,
    //   // 坐标轴名称
    //   name: '',
    //   //名字颜色
    //   nameColorY: '#FFFFFF',
    //   // 轴反转
    //   reversalY: false,
    //   // y轴 坐标文字颜色,
    //   Ycolor: '#FFFFFF',
    //   //X轴线的颜色
    //   lineColorY: 'rgba(255, 255, 255, 0.3)',
    //   //是否显示X分割线
    //   isShowSplitLineY: true,
    //   //X分割线颜色
    //   splitLineColorY: 'rgba(255, 255, 255, 0.3)',
    //   //Y轴宽度
    //   yWidth: 2,
    //   //Y分割线宽度
    //   ySplitWidth: 2,
    //   //Y分割线样式
    //   ySplitType: 'dashed',
    //   //最小值
    //   min: 10
    // },
    // legend: {
    //   //是否显示图例
    //   show: false,
    //   //图例组件离容器左侧的距离, 值可以是像 20 这样的具体像素值，可以是像 '20%' 这样相对于容器高宽的百分比，也可以是 'left', 'center', 'right'。
    //   left: 'right',
    //   //图例组件离容器上侧侧的距离, 值可以是像 20 这样的具体像素值，可以是像 '20%' 这样相对于容器高宽的百分比，也可以是 'top', 'middle', 'bottom'
    //   top: '5%',
    //   //图例列表的布局朝向。 'horizontal' 'vertical'
    //   orient: 'horizontal',
    //   //图例标记的图形宽度
    //   itemWidth: 6,
    //   //图例标记的图形高度
    //   itemHeight: 6,
    //   //图例项的 icon。'circle', 'rect', 'roundRect', 'triangle', 'diamond', 'pin', 'arrow', 'none' 可以通过 'image://url' 设置为图片，其中 URL 为图片的链接，或者 dataURI。
    //   icon: 'rect',
    //   //文字颜色
    //   color: '#FFFFFF',
    //   //字号
    //   fontSize: 14,
    // },
    // tooltip: {
    //   //是否显示提示框组件。
    //   show: true,
    //   //触发类型， 'item'、 'axis'、'none'
    //   trigger: 'axis',
    //   //颜色
    //   color: '#FFFFFF',
    //   //字号
    //   fontSize: 14,
    //   //背景颜色
    //   backgroundColor: 'rgba(34, 62, 97, 0.8)',
    // },
    // //线配置
    // lineStyle: {
    //   //线颜色
    //   color: null,
    //   //宽度
    //   width: 2,
    // },
    // series: {
    //   symbolSize: 12
    // }
  },
  //一网通办数据
  ywtbData: [
    {
      type: '办件情况',
      name: '05月',
      value: 1230
    },
    {
      type: '办件情况',
      name: '06月',
      value: 2150
    },
    {
      type: '办件情况',
      name: '07月',
      value: 1825
    },
    {
      type: '办件情况',
      name: '08月',
      value: 3626
    },
    {
      type: '办件情况',
      name: '09月',
      value: 1542
    },
    {
      type: '办件情况',
      name: '10月',
      value: 1800
    }
  ]
};

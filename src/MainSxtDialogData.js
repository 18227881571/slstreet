import SxtTool from './SxtTool'
export default {
  //是否需要获取HLS
  videoTotal: 0,
  //视频地址
  hlsList: [],

  //播放视频
  play(cameraIndexCode) {

    SxtTool.getHls(cameraIndexCode).then(data => {
      if (!data) {
        return
      }
      this.playHls(data.cameraIndexCode, data.url)
    })
  },
  //播放视频
  playCameraIndexCode(cameraIndexCode) {

    const data = this.hlsList.find(i => i.code === cameraIndexCode)

    if (!data) {
      this.hlsList.push({
        code: cameraIndexCode,
        hls: null
      })
      this.videoTotal++
    }
  },
  /**
   * 播放HLS
   * @param code 唯一编码
   * @param hls 视频地址
   */
  playHls(code, hls) {
    if (!code || !hls) {
      return
    }
    const data = this.hlsList.find(i => i.code === code)

    if (!data) {
      this.hlsList.push({
        code,
        hls
      })
    }
  },
  /**
   * 关闭摄像头
   */
  closeAll() {
    this.hlsList = []
  },
}
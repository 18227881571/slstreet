import GlobalData from './GlobalData'

export default {
  weatherList: [
    {
      code: 'fx',
      icon: GlobalData.imgUrl + '首页天气-风向.png',
      name: '风向',
      value: '西南',
      valueField: '天气_风向'
    },
    {
      code: 'fs',
      icon: GlobalData.imgUrl + '首页天气-风速.png',
      name: '风速',
      value: '--',
      valueField: '天气_风速'
    },
    {
      code: 'qy',
      icon: GlobalData.imgUrl + '首页天气-气压.png',
      name: '气压',
      value: '--',
      valueField: '天气_气压'
    },
    {
      code: 'sd',
      icon: GlobalData.imgUrl + '首页天气-湿度.png',
      name: '湿度',
      value: '--',
      valueField: '天气_湿度'
    },
    {
      code: 'sj',
      name: '20:28:20',
      value: '2022-10-19'
    },
    {
      code: 'tq',
      name: '--',
      value: '--',
      valueField: '天气_天气描述',
      nameField: '天气_温度'
    }
  ],
  menuList: [
    // {
    //   code: 'jdfzx',
    //   name: '街道分中心',
    //   bg: GlobalData.imgUrl + '街道分中心-按钮.png',
    //   list: []
    // },
    {
      code: 'yycs',
      name: '应用超市',
      bg: GlobalData.imgUrl + '街道分中心-按钮.png',
      list: []
    }
  ]
}
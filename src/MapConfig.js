import GlobalTool from './GlobalTool'
import GlobalData from './GlobalData'
export default {
  Map: null,
  MapView: null,
  TileLayer: null,
  MapImageLayer: null,
  GeoJSONLayer: null,
  GraphicsLayer: null,
  Graphic: null,
  Point: null,
  // baseUrl: 'https://api.cdmbc.cn:4432',
  baseUrl: '',
  // tileLayerUrl: 'https://api.cdmbc.cn:4432/gateway/gis/1/ba537fd05f7b42b8940b01183c354647',
  tileLayerUrl: '/gateway/gis/1/ba537fd05f7b42b8940b01183c354647',
  appKey: '963456798000414720',
  defaultCenter: [103.9270, 30.5706],
  defaultZoom: 6,
  //地图图层
  mapLayers: {

  },
  /**
   * 添加图层
   */
  addLayer(title, url, sublayers) {
    const self = this

    if (sublayers) {
      if (Array.isArray(sublayers)) {
        if (sublayers.length > 0) {
          sublayers.forEach(i => {
            if (i.definitionExpression !== undefined) {
              i.definitionExpression = `TOWN_CODE='${GlobalData.streetConfig[GlobalData.street].code}'`
            }
          })
        }
      } else {
        if (sublayers.definitionExpression !== undefined) {
          sublayers.definitionExpression = `TOWN_CODE='${GlobalData.streetConfig[GlobalData.street].code}'`
        }
      }
    }
    console.log(">>>>>>>>>", sublayers)

    url = (url.startsWith('http://') || url.startsWith('https://')) ? url : (self.baseUrl + url)
    console.log('Map url>>>>',url)
    self.removeLayer(title)

    const layer = new self.MapImageLayer({  // 动态图层，根据当前地图的空间参考、显示范围等信息，服务器动态计算出一张图片返回给前端
      title, // 图层名称
      url,
      customParameters: {
        AppKey: self.appKey,   // AppKey            
      },
      sublayers: sublayers || []
      // [{
      //   id: 0,  // 子图层id
      //   popupTemplate,   // 属性弹窗模板
      //   definitionExpression: "COUTRICT_CODE='510116'",
      // }],
    })
    self.mapLayers[title] = layer
    map.add(layer);
  },

  /**
     * @param data 必须包含coordinates字段和 id字段（点位唯一标记） 
     * @param title 图层名字
     * @param category 点位类别
     * @param url 图标
     * @width 点位宽度
     * @height 点位高度
     */
  addMarker(data, title, category, url, width, height) {

    if (!data || data.length <= 0) {
      return;
    }

    let self = this;

    //移除图层
    self.removeLayer(title)

    let pointGraphicList = [];
    const gLayer = new self.GraphicsLayer();

    data.map(i => {

      if (!i.coordinates || !i.coordinates[0] || !i.coordinates[1]) return

      const pointGraphic = new self.Graphic({
        geometry: {
          type: 'point',
          // 点的位置
          longitude: Math.max(i.coordinates[0], i.coordinates[1]),
          latitude: Math.min(i.coordinates[0], i.coordinates[1]),
        },
        symbol: {
          // 唯一标记
          id: i.id,
          // 图层名字
          title: title,
          // 类别
          category: category,
          // 类型有 图片标记 和 点
          type: 'picture-marker',
          // 图片地址，可以网络路径或本地路径（PS：base64亦可）
          url: i.url || url,
          // 图片的大小
          width: width || '107px',
          height: height || '173px'
        },
        attributes: {
          ...data
        }
      })

      pointGraphicList.push(pointGraphic);

    })

    gLayer.graphics.addMany(pointGraphicList)

    self.mapLayers[title] = gLayer
    map.add(gLayer);

  },

  /**
   * 移动到地图中心
   */
  goToMapCenter(coordinates) {
    const self = this
    mapView.goTo({
      center: [parseFloat(coordinates[0]), parseFloat(coordinates[1])],
      zoom: self.maxZoom
    })
  },


  /**
   * 删除图层
   */
  removeLayer(title) {
    const self = this
    let layer = self.mapLayers[title]

    if (layer) {
      map.remove(layer);
    }
  }
}
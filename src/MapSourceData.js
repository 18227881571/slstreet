import GlobalData from "./GlobalData";
import SxtTool from "./SxtTool";

export default {
  cszylb: [
    {
      name: "城区安全",
      icon: "",
      code: "10001",
      type: "",
      url: "",
      sublayers: [],
      sortOrder: "",
      parentCode: "",
      open: false,
      selected: false,
      children: [
        {
          name: "地质灾害隐患点",
          icon: "",
          code: "10024",
          type: "",
          url: "/gateway/gis/1/fc9cb828c01540a08c98c5b61db38ef0",
          sublayers: [
            {
              id: 0,
              popupTemplate: {},
              definitionExpression: "COUTRICT_CODE='510116'",
            },
          ],
          sortOrder: 10032,
          parentCode: "10001",
          open: false,
          selected: false,
        },
        {
          name: "微型消防站",
          icon: "",
          code: "10053",
          type: "point",
          url: "微型消防站",
          sublayers: [],
          sortOrder: 10053,
          parentCode: "10001",
          open: false,
          selected: false,
        },
      ],
    },
    {
      name: "城区交通",
      icon: "",
      code: "10002",
      type: "",
      url: "",
      sublayers: [],
      sortOrder: "",
      parentCode: "",
      open: false,
      selected: false,
      children: [
        {
          name: "下穿隧道",
          icon: "",
          code: "10063",
          type: "",
          url: "/gateway/api/1/9e47103f898d440f840347625073cfbd",
          sublayers: [
            {
              id: 0,
              popupTemplate: {},
              definitionExpression: "COUTRICT_CODE='510116'",
            },
          ],
          sortOrder: "",
          parentCode: "10002",
          open: false,
          selected: false,
        },
        {
          name: "普通桥",
          icon: "",
          code: "10064",
          type: "",
          url: "/gateway/api/1/4e52bf321ab14f5da4d2bcb9d0b8d074",
          sublayers: [
            {
              id: 0,
              popupTemplate: {},
              definitionExpression: "COUTRICT_CODE='510116'",
            },
          ],
          sortOrder: "",
          parentCode: "10002",
          open: false,
          selected: false,
        },
        {
          name: "公交站亭",
          icon: "",
          code: "10065",
          type: "",
          url: "/gateway/gis/1/89116dcbba3244768a1d34ba5eb0530e",
          sublayers: [
            {
              id: 0,
              popupTemplate: {},
              definitionExpression: "COUTRICT_CODE='510116'",
            },
          ],
          sortOrder: "",
          parentCode: "10002",
          open: false,
          selected: false,
        },
        {
          name: "立交桥",
          icon: "",
          code: "10066",
          type: "",
          url: "/gateway/api/1/2ea65957341047dc8107516978e341a1",
          sublayers: [
            {
              id: 0,
              popupTemplate: {},
              definitionExpression: "COUTRICT_CODE='510116'",
            },
          ],
          sortOrder: "",
          parentCode: "10002",
          open: false,
          selected: false,
        },
        {
          name: "人行天桥",
          icon: "",
          code: "10067",
          type: "",
          url: "/gateway/api/1/85f48498b4ff426b8977546485acede0",
          sublayers: [
            {
              id: 0,
              popupTemplate: {},
              definitionExpression: "COUTRICT_CODE='510116'",
            },
          ],
          sortOrder: "",
          parentCode: "10002",
          open: false,
          selected: false,
        },
        {
          name: "高架桥",
          icon: "",
          code: "10068",
          type: "",
          url: "/gateway/api/1/120a24b4138f4e3095997def0f0818f9",
          sublayers: [
            {
              id: 0,
              popupTemplate: {},
              definitionExpression: "COUTRICT_CODE='510116'",
            },
          ],
          sortOrder: "",
          parentCode: "10002",
          open: false,
          selected: false,
        },
        {
          name: "地铁线路",
          icon: "",
          code: "10021",
          type: "",
          url: "/gateway/gis/1/c30b40a428f1444f9efb550ba5a5ef42",
          sublayers: [
            {
              id: 0,
              popupTemplate: {},
              definitionExpression: "COUTRICT_CODE LIKE '%510116%'",
            },
          ],
          sortOrder: 10003,
          parentCode: "10002",
          open: false,
          selected: false,
        },
        {
          name: "地铁站",
          icon: "",
          code: "10022",
          type: "",
          url: "/gateway/gis/1/d7e7abe5a70447929f2491e97e90c345",
          sublayers: [
            {
              id: 0,
              popupTemplate: {},
              definitionExpression: "COUTRICT_CODE='510116'",
            },
          ],
          sortOrder: 10004,
          parentCode: "10002",
          open: false,
          selected: false,
        },
        {
          name: "规划地铁线及站点",
          icon: "",
          code: "10042",
          type: "",
          url: "/gateway/gis/1/0762cdc0aa3546b79c9caeb296cdbc12",
          sublayers: [
            {
              id: 0,
              popupTemplate: {},
              definitionExpression: "COUTRICT_CODE='510116'",
            },
          ],
          sortOrder: 10005,
          parentCode: "10002",
          open: false,
          selected: false,
        },
        {
          name: "地铁出入口",
          icon: "",
          code: "10019",
          type: "",
          url: "/gateway/gis/1/3c2163aac2674f3aaeabfd203f75d9a1",
          sublayers: [
            {
              id: 0,
              popupTemplate: {},
              definitionExpression: "COUTRICT_CODE='510116'",
            },
          ],
          sortOrder: 10006,
          parentCode: "10002",
          open: false,
          selected: false,
        },
        {
          name: "地铁维修厂",
          icon: "",
          code: "10020",
          type: "",
          url: "/gateway/gis/1/eb831f0cc8184a1b8a9e010ce4a1a2df",
          sublayers: [
            {
              id: 0,
              popupTemplate: {},
              definitionExpression: "COUTRICT_CODE='510116'",
            },
          ],
          sortOrder: 10007,
          parentCode: "10002",
          open: false,
          selected: false,
        },
        {
          name: "地铁车辆段",
          icon: "",
          code: "10018",
          type: "",
          url: "/gateway/gis/1/d9f2e140e63b46678b0242c282c7d83a",
          sublayers: [
            {
              id: 0,
              popupTemplate: {},
              definitionExpression: "COUTRICT_CODE='510116'",
            },
          ],
          sortOrder: 10008,
          parentCode: "10002",
          open: false,
          selected: false,
        },
        {
          name: "客运汽车站",
          icon: "",
          code: "10046",
          type: "",
          url: "/gateway/gis/1/5cb7c5ee1e93443b819db300789eb9f3",
          sublayers: [
            {
              id: 0,
              popupTemplate: {},
              definitionExpression: "COUTRICT_CODE='510116'",
            },
          ],
          sortOrder: 10012,
          parentCode: "10002",
          open: false,
          selected: false,
        },
        {
          name: "公交电子站牌",
          icon: "",
          code: "10029",
          type: "",
          url: "/gateway/gis/1/91bb55dfdc9c44ebb5733962124c899a",
          sublayers: [
            {
              id: 0,
              popupTemplate: {},
              definitionExpression: "COUTRICT_CODE='510116'",
            },
          ],
          sortOrder: 10013,
          parentCode: "10002",
          open: false,
          selected: false,
        },
        {
          name: "公交维修点",
          icon: "",
          code: "10037",
          type: "",
          url: "/gateway/gis/1/c94d8d7571694d98a0ee86db9ed08416",
          sublayers: [
            {
              id: 0,
              popupTemplate: {},
              definitionExpression: "COUTRICT_CODE='510116'",
            },
          ],
          sortOrder: 10014,
          parentCode: "10002",
          open: false,
          selected: false,
        },
        {
          name: "公交线路",
          icon: "",
          code: "10039",
          type: "",
          url: "/gateway/gis/1/b772c159aebf401884bcd98b0c4f9800",
          sublayers: [
            {
              id: 0,
              popupTemplate: {},
              definitionExpression: "COUTRICT_CODE='510116'",
            },
          ],
          sortOrder: 10015,
          parentCode: "10002",
          open: false,
          selected: false,
        },
        {
          name: "公交规划线路",
          icon: "",
          code: "10036",
          type: "",
          url: "/gateway/gis/1/baaf7015bb21498e95db48a406b143ce",
          sublayers: [
            {
              id: 0,
              popupTemplate: {},
              definitionExpression: "COUTRICT_CODE='510116'",
            },
          ],
          sortOrder: 10016,
          parentCode: "10002",
          open: false,
          selected: false,
        },
        {
          name: "公交专用道",
          icon: "",
          code: "10041",
          type: "",
          url: "/gateway/gis/1/2c2533e9d7d449e18f263411b4dda596",
          sublayers: [
            {
              id: 0,
              popupTemplate: {},
              definitionExpression: "COUTRICT_CODE='510116'",
            },
          ],
          sortOrder: 10017,
          parentCode: "10002",
          open: false,
          selected: false,
        },
        {
          name: "公交规划线路",
          icon: "",
          code: "10031",
          type: "",
          url: "/gateway/gis/1/f4b372cae5b848fabfe8ab6ff7f387db",
          sublayers: [
            {
              id: 0,
              popupTemplate: {},
              definitionExpression: "COUTRICT_CODE='510116'",
            },
          ],
          sortOrder: 10018,
          parentCode: "10002",
          open: false,
          selected: false,
        },
        {
          name: "公交站点",
          icon: "",
          code: "10040",
          type: "",
          url: "/gateway/gis/1/9ef6d1a054164d2984e5c2f86a0d8da0",
          sublayers: [
            {
              id: 0,
              popupTemplate: {},
              definitionExpression: "COUTRICT_CODE='510116'",
            },
          ],
          sortOrder: 10019,
          parentCode: "10002",
          open: false,
          selected: false,
        },
        {
          name: "公交场站",
          icon: "",
          code: "10028",
          type: "",
          url: "/gateway/gis/1/527e0d30880f4073838e1049e97c2829",
          sublayers: [
            {
              id: 0,
              popupTemplate: {},
              definitionExpression: "COUTRICT_CODE='510116'",
            },
          ],
          sortOrder: 10020,
          parentCode: "10002",
          open: false,
          selected: false,
        },
        {
          name: "充电桩",
          icon: "",
          code: "10014",
          type: "",
          url: "/gateway/gis/1/f97e5629451b477cb7b9b3e58fbd74b8",
          sublayers: [
            {
              id: 0,
              popupTemplate: {
                title: "{名称}",
                content: [
                  {
                    type: "fields",
                    fieldInfos: [
                      {
                        fieldName: "设备编号",
                        label: "设备编号",
                      },
                      {
                        fieldName: "项目",
                        label: "项目",
                      },
                      {
                        fieldName: "设备应用",
                        label: "设备应用",
                      },
                      {
                        fieldName: "设备类型",
                        label: "设备类型",
                      },
                      {
                        fieldName: "厂商",
                        label: "厂商",
                      },
                      {
                        fieldName: "设备ip",
                        label: "设备ip",
                      },
                      {
                        fieldName: "coutrict",
                        label: "区县名称",
                      },
                      {
                        fieldName: "town",
                        label: "乡镇名称",
                      },
                      {
                        fieldName: "区",
                        label: "区",
                      },
                      {
                        fieldName: "区域",
                        label: "区域",
                      },
                      {
                        fieldName: "街道",
                        label: "街道",
                      },
                      {
                        fieldName: "委办",
                        label: "委办",
                      },
                      {
                        fieldName: "片区",
                        label: "片区",
                      },
                      {
                        fieldName: "小区",
                        label: "小区",
                      },
                      {
                        fieldName: "居委",
                        label: "居委",
                      },
                      {
                        fieldName: "地址",
                        label: "地址",
                      },
                      {
                        fieldName: "f_importtime",
                        label: "入库时间",
                      },
                    ],
                  },
                ],
              },
              definitionExpression: "COUTRICT_CODE='510116'",
            },
          ],
          sortOrder: 10030,
          parentCode: "10002",
          open: false,
          selected: false,
        },
        {
          name: "危货车辆实时定位",
          icon: "",
          code: "10015",
          type: "",
          url: "/gateway/gis/1/8dfd1479511a4c8db91a61407ce92027",
          sublayers: [
            {
              id: 0,
              popupTemplate: {
                title: "{名称}",
                content: [
                  {
                    type: "fields",
                    fieldInfos: [
                      {
                        fieldName: "vehicle_no",
                        label: "车牌号码",
                      },
                      {
                        fieldName: "vehicle_color",
                        label: "车牌颜色",
                      },
                      {
                        fieldName: "date_str",
                        label: "日期",
                      },
                      {
                        fieldName: "time_str",
                        label: "时间",
                      },
                      {
                        fieldName: "vec2",
                        label: "行驶记录速度",
                      },
                      {
                        fieldName: "alarm",
                        label: "报警状态",
                      },
                      {
                        fieldName: "state",
                        label: "车辆状态",
                      },
                      {
                        fieldName: "coutrict",
                        label: "区县",
                      },
                      {
                        fieldName: "town",
                        label: "乡镇",
                      },
                      {
                        fieldName: "f_importtime",
                        label: "入库时间",
                      },
                    ],
                  },
                ],
              },
              definitionExpression: "COUTRICT_CODE='510116'",
            },
          ],
          sortOrder: 10031,
          parentCode: "10002",
          open: false,
          selected: false,
        },
        {
          name: "加油加气站",
          icon: "",
          code: "10054",
          type: "",
          url: "/gateway/gis/1/efc653e1455d4531afbb0757caeebcc2",
          sublayers: [
            {
              id: 0,
              popupTemplate: {},
              definitionExpression: "COUTRICT_CODE='510116'",
            },
          ],
          sortOrder: 10054,
          parentCode: "10002",
          open: false,
          selected: false,
        },
        {
          name: "公交维修点",
          icon: "",
          code: "10038",
          type: "",
          url: "/gateway/api/1/c94d8d7571694d98a0ee86db9ed08416",
          sublayers: [
            {
              id: 0,
              popupTemplate: {},
              definitionExpression: "COUTRICT_CODE='510116'",
            },
          ],
          sortOrder: 10076,
          parentCode: "10002",
          open: false,
          selected: false,
        },
      ],
    },
    {
      name: "城区资源",
      icon: "",
      code: "10003",
      type: "",
      url: "",
      sublayers: [],
      sortOrder: "",
      parentCode: "",
      open: false,
      selected: false,
      children: [
        {
          name: "村社区界",
          icon: "",
          code: "10016",
          type: "",
          url: "/gateway/gis/1/5f651de6590d4583ad2943027c86cb9e",
          sublayers: [],
          sortOrder: 10001,
          parentCode: "10003",
          open: false,
          selected: false,
        },
        {
          name: "村级行政区",
          icon: "",
          code: "10027",
          type: "",
          url: "/gateway/gis/1/2eeb6c6b85194cd6992f273ac0b998c6",
          sublayers: [
            {
              id: 0,
              popupTemplate: {
                title: "{名称}",
                content: [
                  {
                    type: "fields",
                    fieldInfos: [
                      {
                        fieldName: "DISTRICT",
                        label: "市级名称",
                      },
                      {
                        fieldName: "COUTRICT",
                        label: "县级名称",
                      },
                      {
                        fieldName: "TOWN",
                        label: "乡镇街办名称",
                      },
                      {
                        fieldName: "VILLAGE",
                        label: "村社区名称",
                      },
                      {
                        fieldName: "REMARK",
                        label: "备注",
                      },
                    ],
                  },
                ],
              },
              definitionExpression: "COUTRICT_CODE='510116'",
            },
          ],
          sortOrder: 10002,
          parentCode: "10003",
          open: false,
          selected: false,
        },
        {
          name: "酒店",
          icon: "",
          code: "10023",
          type: "",
          url: "/gateway/gis/1/7d9d0cbd5e17418eab5596d76f82efec",
          sublayers: [
            {
              id: 0,
              popupTemplate: {
                title: "{名称}",
                content: [
                  {
                    type: "fields",
                    fieldInfos: [
                      {
                        fieldName: "酒店名称",
                        label: "酒店名称",
                      },
                      {
                        fieldName: "完整地址",
                        label: "完整地址",
                      },
                      {
                        fieldName: "联系电话",
                        label: "联系电话",
                      },
                    ],
                  },
                ],
              },
              definitionExpression: "COUTRICT_CODE='510116'",
            },
          ],
          sortOrder: 10010,
          parentCode: "10003",
          open: false,
          selected: false,
        },
        {
          name: "学校",
          icon: "",
          code: "10026",
          type: "",
          url: "/gateway/gis/1/dea7aba6931c48d2bbe5bcc4544871ae",
          sublayers: [
            {
              id: 0,
              popupTemplate: {
                title: "{名称}",
                content: [
                  {
                    type: "fields",
                    fieldInfos: [
                      {
                        fieldName: "code",
                        label: "编码",
                      },
                      {
                        fieldName: "name",
                        label: "学校名称",
                      },
                      {
                        fieldName: "class1",
                        label: "举办类型",
                      },
                      {
                        fieldName: "class2",
                        label: "办学类型",
                      },
                      {
                        fieldName: "class3",
                        label: "区域类型",
                      },
                      {
                        fieldName: "county_name",
                        label: "所属区县",
                      },
                      {
                        fieldName: "COUTRICT",
                        label: "区县名称",
                      },
                      {
                        fieldName: "TOWN",
                        label: "乡镇名称",
                      },
                      {
                        fieldName: "address",
                        label: "学校地址",
                      },
                    ],
                  },
                ],
              },
              definitionExpression: "COUTRICT_CODE='510116'",
            },
          ],
          sortOrder: 10028,
          parentCode: "10003",
          open: false,
          selected: false,
        },
        {
          name: "医院",
          icon: "",
          code: "10033",
          type: "",
          url: "/gateway/gis/1/6a8072d32ac24873a78d8089d3d575e1",
          sublayers: [
            {
              id: 0,
              popupTemplate: {},
              definitionExpression: "COUTRICT_CODE='510116'",
            },
          ],
          sortOrder: 10029,
          parentCode: "10003",
          open: false,
          selected: false,
        },
        {
          name: "电影院",
          icon: "",
          code: "10010",
          type: "",
          url: "/gateway/api/1/e8b0f0fb575d433aa1546391c8864050",
          sublayers: [
            {
              id: 0,
              popupTemplate: {},
              definitionExpression: "COUTRICT_CODE='510116'",
            },
          ],
          sortOrder: 10040,
          parentCode: "10003",
          open: false,
          selected: false,
        },
        {
          name: "银行",
          icon: "",
          code: "10009",
          type: "",
          url: "/gateway/api/1/883e7b1cffa742a99267c23d0616f437",
          sublayers: [
            {
              id: 0,
              popupTemplate: {},
              definitionExpression: "COUTRICT_CODE='510116'",
            },
          ],
          sortOrder: 10041,
          parentCode: "10003",
          open: false,
          selected: false,
        },
        {
          name: "公园",
          icon: "",
          code: "10008",
          type: "",
          url: "/gateway/api/1/12a404348ec04032b2ac1bdaf7370a91",
          sublayers: [
            {
              id: 0,
              popupTemplate: {},
              definitionExpression: "COUTRICT_CODE='510116'",
            },
          ],
          sortOrder: 10042,
          parentCode: "10003",
          open: false,
          selected: false,
        },
        {
          name: "商业体",
          icon: "",
          code: "10007",
          type: "",
          url: "/gateway/api/1/89efa581810141a388369a6678d83c6c",
          sublayers: [
            {
              id: 0,
              popupTemplate: {},
              definitionExpression: "COUTRICT_CODE='510116'",
            },
          ],
          sortOrder: 10043,
          parentCode: "10003",
          open: false,
          selected: false,
        },
        {
          name: "影像版",
          icon: "",
          code: "10034",
          type: "",
          url: "/gateway/api/1/lzb/bdyq/cdtfyqsj/zs",
          sublayers: [
            {
              id: 0,
              popupTemplate: {},
              definitionExpression: "COUTRICT_CODE='510116'",
            },
          ],
          sortOrder: 10044,
          parentCode: "10003",
          open: false,
          selected: false,
        },
        {
          name: "便民服务中心",
          icon: "",
          code: "10035",
          type: "",
          url: "/gateway/api/1/13b5e0b5840b41d48cf1686cf775790e",
          sublayers: [
            {
              id: 0,
              popupTemplate: {},
              definitionExpression: "COUTRICT_CODE='510116'",
            },
          ],
          sortOrder: 10045,
          parentCode: "10003",
          open: false,
          selected: false,
        },
        {
          name: "成都市四级行政区划",
          icon: "",
          code: "10043",
          type: "",
          url: "/gateway/api/1/ea77647c00c64bd782cb38799e2b88dd",
          sublayers: [
            {
              id: 0,
              popupTemplate: {},
              definitionExpression: "COUTRICT_CODE='510116'",
            },
          ],
          sortOrder: 10060,
          parentCode: "10003",
          open: false,
          selected: false,
        },
        {
          name: "建筑白模-双流区",
          icon: "",
          code: "10045",
          type: "",
          url: "/gateway/api/1/1bb037b836ca483c8c5c93444293b320",
          sublayers: [
            {
              id: 0,
              popupTemplate: {},
              definitionExpression: "COUTRICT_CODE='510116'",
            },
          ],
          sortOrder: 10062,
          parentCode: "10003",
          open: false,
          selected: false,
        },
      ],
    },
    {
      name: "项目规划",
      icon: "",
      code: "10004",
      type: "",
      url: "",
      sublayers: [],
      sortOrder: "",
      parentCode: "",
      open: false,
      selected: false,
      children: [
        {
          name: "轨道项目",
          icon: "",
          code: "10044",
          type: "",
          url: "/gateway/gis/1/0a2ec25716224160bbb613632f5c2954",
          sublayers: [
            {
              id: 0,
              popupTemplate: {},
              definitionExpression: "COUTRICT_CODE='510116'",
            },
          ],
          sortOrder: 10009,
          parentCode: "10004",
          open: false,
          selected: false,
        },
        {
          name: "市政项目",
          icon: "",
          code: "10050",
          type: "",
          url: "/gateway/gis/1/58cc3a1c0487469fbc1123cb961cb7b2",
          sublayers: [
            {
              id: 0,
              popupTemplate: {},
              definitionExpression: "COUTRICT_CODE='510116'",
            },
          ],
          sortOrder: 10023,
          parentCode: "10004",
          open: false,
          selected: false,
        },
        {
          name: "市政项目",
          icon: "",
          code: "10049",
          type: "",
          url: "/gateway/gis/1/29ba71d6088d4774883ae9b29f61fe6b",
          sublayers: [
            {
              id: 0,
              popupTemplate: {},
              definitionExpression: "COUTRICT_CODE='510116'",
            },
          ],
          sortOrder: 10024,
          parentCode: "10004",
          open: false,
          selected: false,
        },
        {
          name: "房建项目",
          icon: "",
          code: "10025",
          type: "",
          url: "/gateway/gis/1/e6c48b3841f6409789f69cc894bd321b",
          sublayers: [
            {
              id: 0,
              popupTemplate: {},
              definitionExpression: "COUTRICT_CODE='510116'",
            },
          ],
          sortOrder: 10025,
          parentCode: "10004",
          open: false,
          selected: false,
        },
        {
          name: "控规地块",
          icon: "",
          code: "10047",
          type: "",
          url: "/gateway/gis/1/b080c03871144647b1d757df689b0981",
          sublayers: [
            {
              id: 0,
              popupTemplate: {},
              definitionExpression: "COUTRICT_CODE='510116'",
            },
          ],
          sortOrder: 10027,
          parentCode: "10004",
          open: false,
          selected: false,
        },
      ],
    },
    {
      name: "便民服务中心",
      icon: "",
      code: "10060",
      type: "",
      url: "",
      sublayers: [],
      sortOrder: "",
      parentCode: "",
      open: false,
      selected: false,
      children: [
        {
          name: "街道办事处",
          icon: "",
          code: "10061",
          type: "",
          url: "/gateway/gis/1/d91f559331e447c2b07b186595df5688",
          sublayers: [
            {
              id: 0,
              popupTemplate: {},
              definitionExpression: "COUTRICT_CODE='510116'",
            },
          ],
          sortOrder: "",
          parentCode: "10060",
          open: false,
          selected: false,
        },
      ],
    },
    {
      name: "公共厕所",
      icon: "",
      code: "10062",
      type: "",
      url: "/gateway/gis/1/88d7455ab093433888aaa064680b6581",
      sublayers: [
        {
          id: 0,
          popupTemplate: {},
          definitionExpression: "COUTRICT_CODE='510116'",
        },
      ],
      sortOrder: "",
      parentCode: "",
      open: false,
      selected: false,
    },
    {
      name: "户外健身设施",
      icon: "",
      code: "10070",
      type: "",
      url: "/gateway/gis/1/4533a55d962346d7a5944681abffa729",
      sublayers: [
        {
          id: 0,
          popupTemplate: {},
          definitionExpression: "COUTRICT_CODE='510116'",
        },
      ],
      sortOrder: "",
      parentCode: "",
      open: false,
      selected: false,
    },
    {
      name: "应急保障",
      icon: "",
      code: "10071",
      type: "",
      url: "",
      sublayers: [],
      sortOrder: "",
      parentCode: "",
      open: false,
      selected: false,
      children: [
        {
          name: "避难场所",
          icon: "",
          code: "10072",
          type: "",
          url: "/gateway/api/1/59714e545c3c4600862181639e8a43c0",
          sublayers: [
            {
              id: 0,
              popupTemplate: {},
              definitionExpression: "COUTRICT_CODE='510116'",
            },
          ],
          sortOrder: "",
          parentCode: "10071",
          open: false,
          selected: false,
        },
      ],
    },
    {
      name: "图书馆",
      icon: "",
      code: "10073",
      type: "",
      url: "/gateway/gis/1/7bbf23808404434796c3251de75ae1f6",
      sublayers: [
        {
          id: 0,
          popupTemplate: {},
          definitionExpression: "COUTRICT_CODE='510116'",
        },
      ],
      sortOrder: "",
      parentCode: "",
      open: false,
      selected: false,
    },
    {
      name: "美食街",
      icon: "",
      code: "10074",
      type: "",
      url: "/gateway/gis/1/17795b48bb0c4237b3f7669461c2ae9b",
      sublayers: [
        {
          id: 0,
          popupTemplate: {},
          definitionExpression: "COUTRICT_CODE='510116'",
        },
      ],
      sortOrder: "",
      parentCode: "",
      open: false,
      selected: false,
    },
    {
      name: "商场/超市",
      icon: "",
      code: "10075",
      type: "",
      url: "",
      sublayers: [],
      sortOrder: "",
      parentCode: "",
      open: false,
      selected: false,
      children: [
        {
          name: "超市",
          icon: "",
          code: "10076",
          type: "",
          url: "/gateway/gis/1/283efcfd31ce445ca8240f0968eb85d8",
          sublayers: [
            {
              id: 0,
              popupTemplate: {},
              definitionExpression: "COUTRICT_CODE='510116'",
            },
          ],
          sortOrder: "",
          parentCode: "10075",
          open: false,
          selected: false,
        },
        {
          name: "商圈",
          icon: "",
          code: "10077",
          type: "",
          url: "/gateway/gis/1/f38c631914554e0f86afd8e0da054282",
          sublayers: [
            {
              id: 0,
              popupTemplate: {},
              definitionExpression: "COUTRICT_CODE='510116'",
            },
          ],
          sortOrder: "",
          parentCode: "10075",
          open: false,
          selected: false,
        },
        {
          name: "商场超市",
          icon: "",
          code: "10078",
          type: "",
          url: "/gateway/gis/1/6ec61f19120a42019ff7424cb9869a0c",
          sublayers: [
            {
              id: 0,
              popupTemplate: {},
              definitionExpression: "COUTRICT_CODE='510116'",
            },
          ],
          sortOrder: "",
          parentCode: "10075",
          open: false,
          selected: false,
        },
        {
          name: "商场",
          icon: "",
          code: "10079",
          type: "",
          url: "/gateway/gis/1/107e2f6afe954b1a8c3c7394d868fd4b",
          sublayers: [
            {
              id: 0,
              popupTemplate: {},
              definitionExpression: "COUTRICT_CODE='510116'",
            },
          ],
          sortOrder: "",
          parentCode: "10075",
          open: false,
          selected: false,
        },
      ],
    },
    {
      name: "集贸市场",
      icon: "",
      code: "10080",
      type: "",
      url: "/gateway/gis/1/56e24eadd32348e0b1f3687cffe8ef67",
      sublayers: [
        {
          id: 0,
          popupTemplate: {},
          definitionExpression: "COUTRICT_CODE='510116'",
        },
      ],
      sortOrder: "",
      parentCode: "",
      open: false,
      selected: false,
    },
    {
      name: "景区",
      icon: "",
      code: "10081",
      type: "",
      url: "/gateway/gis/1/3c7f73dad06843479fc8bc1c8164a490",
      sublayers: [
        {
          id: 0,
          popupTemplate: {},
          definitionExpression: "COUTRICT_CODE='510116'",
        },
      ],
      sortOrder: "",
      parentCode: "",
      open: false,
      selected: false,
    },
    {
      name: "招待所",
      icon: "",
      code: "10082",
      type: "",
      url: "/gateway/gis/1/6b772c9c221c4a6192b5257684032340",
      sublayers: [
        {
          id: 0,
          popupTemplate: {},
          definitionExpression: "COUTRICT_CODE='510116'",
        },
      ],
      sortOrder: "",
      parentCode: "",
      open: false,
      selected: false,
    },
    {
      name: "广场",
      icon: "",
      code: "10083",
      type: "",
      url: "/gateway/gis/1/65db8f4c23454c329b3341d1b8642c33",
      sublayers: [
        {
          id: 0,
          popupTemplate: {},
          definitionExpression: "COUTRICT_CODE='510116'",
        },
      ],
      sortOrder: "",
      parentCode: "",
      open: false,
      selected: false,
    },
    {
      name: "社区养老机构",
      icon: "",
      code: "10084",
      type: "",
      url: "",
      sublayers: [],
      sortOrder: "",
      parentCode: "",
      open: false,
      selected: false,
      children: [
        {
          name: "孤儿院",
          icon: "",
          code: "10085",
          type: "",
          url: "/gateway/gis/1/a2b21993cf2441bda4fb20fda1b029f7",
          sublayers: [
            {
              id: 0,
              popupTemplate: {},
              definitionExpression: "COUTRICT_CODE='510116'",
            },
          ],
          sortOrder: "",
          parentCode: "10084",
          open: false,
          selected: false,
        },
        {
          name: "福利院",
          icon: "",
          code: "10086",
          type: "",
          url: "/gateway/gis/1/cffe0784e7d343adae7affd65cbfeae6",
          sublayers: [
            {
              id: 0,
              popupTemplate: {},
              definitionExpression: "COUTRICT_CODE='510116'",
            },
          ],
          sortOrder: "",
          parentCode: "10084",
          open: false,
          selected: false,
        },
        {
          name: "养老院",
          icon: "",
          code: "10087",
          type: "",
          url: "/gateway/gis/1/7ab4822c85a84af7a9a103112330a410",
          sublayers: [
            {
              id: 0,
              popupTemplate: {},
              definitionExpression: "COUTRICT_CODE='510116'",
            },
          ],
          sortOrder: 10003,
          parentCode: "10084",
          open: false,
          selected: false,
        },
      ],
    },
    {
      name: "行政实体",
      icon: "",
      code: "10089",
      type: "",
      url: "",
      sublayers: [],
      sortOrder: "",
      parentCode: "",
      open: false,
      selected: false,
      children: [
        {
          name: "派出所",
          icon: "",
          code: "10092",
          type: "",
          url: "gateway/gis/1/d0ad47536534467b93eb42f477e5cebb",
          sublayers: [
            {
              id: 0,
              popupTemplate: {},
              definitionExpression: "COUTRICT_CODE='510116'",
            },
          ],
          sortOrder: "",
          parentCode: "10089",
          open: false,
          selected: false,
        },
        {
          name: "村居委办公点",
          icon: "",
          code: "10093",
          type: "",
          url: "/gateway/api/1/2cc7bcb7c8944c42ab92a71419c316df",
          sublayers: [
            {
              id: 0,
              popupTemplate: {},
              definitionExpression: "COUTRICT_CODE='510116'",
            },
          ],
          sortOrder: "",
          parentCode: "10089",
          open: false,
          selected: false,
        },
        {
          name: "供气办公场所",
          icon: "",
          code: "10094",
          type: "",
          url: "/gateway/gis/1/3037a3e2a8cf48d683de7994c064661a",
          sublayers: [
            {
              id: 0,
              popupTemplate: {},
              definitionExpression: "COUTRICT_CODE='510116'",
            },
          ],
          sortOrder: "",
          parentCode: "10089",
          open: false,
          selected: false,
        },
        {
          name: "供电办公场所",
          icon: "",
          code: "10090",
          type: "",
          url: "/gateway/api/1/f356c19291b444149ee3ebbcabacd234",
          sublayers: [
            {
              id: 0,
              popupTemplate: {},
              definitionExpression: "COUTRICT_CODE='510116'",
            },
          ],
          sortOrder: 10001,
          parentCode: "10089",
          open: false,
          selected: false,
        },
        {
          name: "邮政营业场所",
          icon: "",
          code: "10091",
          type: "",
          url: "/gateway/api/1/8b030ad67f83420abfa13892a7495dd5",
          sublayers: [
            {
              id: 0,
              popupTemplate: {},
              definitionExpression: "COUTRICT_CODE='510116'",
            },
          ],
          sortOrder: 10002,
          parentCode: "10089",
          open: false,
          selected: false,
        },
      ],
    },
    {
      name: "酒店",
      icon: "",
      code: "10095",
      type: "",
      url: "",
      sublayers: [],
      sortOrder: "",
      parentCode: "",
      open: false,
      selected: false,
      children: [
        {
          name: "宾馆",
          icon: "",
          code: "10096",
          type: "",
          url: "/gateway/gis/1/f5a1d676dd76463280e757530444850b",
          sublayers: [
            {
              id: 0,
              popupTemplate: {},
              definitionExpression: "COUTRICT_CODE='510116'",
            },
          ],
          sortOrder: "",
          parentCode: "10095",
          open: false,
          selected: false,
        },
        {
          name: "宾馆酒店",
          icon: "",
          code: "10097",
          type: "",
          url: "/gateway/gis/1/72155a6b0bd44fb790438c769b6f520b",
          sublayers: [
            {
              id: 0,
              popupTemplate: {},
              definitionExpression: "COUTRICT_CODE='510116'",
            },
          ],
          sortOrder: "",
          parentCode: "10095",
          open: false,
          selected: false,
        },
      ],
    },
    {
      name: "大型游乐场所",
      icon: "",
      code: "10056",
      type: "",
      url: "",
      sublayers: [],
      sortOrder: 1,
      parentCode: "",
      open: false,
      selected: false,
      children: [
        {
          name: "休闲会所",
          icon: "",
          code: "10057",
          type: "",
          url: "/gateway/gis/1/1ad0f8953bc949778e4e7386b90b29d9",
          sublayers: [
            {
              id: 0,
              popupTemplate: {},
              definitionExpression: "COUTRICT_CODE='510116'",
            },
          ],
          sortOrder: "",
          parentCode: "10056",
          open: false,
          selected: false,
        },
        {
          name: "动物园",
          icon: "",
          code: "10059",
          type: "",
          url: "/gateway/gis/1/8fc6996534ff4ab5adcc71118298069e",
          sublayers: [],
          sortOrder: "",
          parentCode: "10056",
          open: false,
          selected: false,
        },
        {
          name: "游乐场",
          icon: "",
          code: "10058",
          type: "",
          url: "/gateway/gis/1/7ac4409c4fd345a8bdb84f9c845b8d87",
          sublayers: [],
          sortOrder: 0,
          parentCode: "10056",
          open: false,
          selected: false,
        },
      ],
    },
    {
      name: "体育场馆",
      icon: "",
      code: "10088",
      type: "",
      url: "/gateway/gis/1/71e6f9324e374c2e869e0301e65be90a",
      sublayers: [
        {
          id: 0,
          popupTemplate: {},
          definitionExpression: "COUTRICT_CODE='510116'",
        },
      ],
      sortOrder: 10001,
      parentCode: "",
      open: false,
      selected: false,
    },
    {
      name: "智慧大运",
      icon: "",
      code: "10005",
      type: "",
      url: "",
      sublayers: [
        {
          id: 0,
          popupTemplate: {},
          definitionExpression: "COUTRICT_CODE='510116'",
        },
      ],
      sortOrder: 10022,
      parentCode: "",
      open: false,
      selected: false,
      children: [
        {
          name: "双流体育中心",
          icon: "",
          code: "10051",
          type: "",
          url: "/gateway/gis/1/5d3bc80439ea428ba20fa882e86a5b8b",
          sublayers: [
            {
              id: 0,
              popupTemplate: {},
              definitionExpression: "COUTRICT_CODE='510116'",
            },
          ],
          sortOrder: 10034,
          parentCode: "10005",
          open: false,
          selected: false,
        },
        {
          name: "四川川投国际网球中心",
          icon: "",
          code: "10052",
          type: "",
          url: "/gateway/gis/1/37220769f79443a0a8be8925b2421ebe",
          sublayers: [
            {
              id: 0,
              popupTemplate: {},
              definitionExpression: "COUTRICT_CODE='510116'",
            },
          ],
          sortOrder: 10035,
          parentCode: "10005",
          open: false,
          selected: false,
        },
        {
          name: "成都现代五项赛事中心",
          icon: "",
          code: "10012",
          type: "",
          url: "/gateway/gis/1/c106cbc8e23f4e13986a20a49e405607",
          sublayers: [
            {
              id: 0,
              popupTemplate: {},
              definitionExpression: "COUTRICT_CODE='510116'",
            },
          ],
          sortOrder: 10036,
          parentCode: "10005",
          open: false,
          selected: false,
        },
      ],
    },
    {
      name: "保险公司",
      icon: "",
      code: "10055",
      type: "",
      url: "/gateway/gis/1/dbf5bdc378ba47808c8376b81b84fe00",
      sublayers: [
        {
          id: 0,
          popupTemplate: {},
          definitionExpression: "COUTRICT_CODE='510116'",
        },
      ],
      sortOrder: 10055,
      parentCode: "",
      open: false,
      selected: false,
    },
    {
      name: "城市内涝点位",
      icon: "",
      code: "10098",
      type: "point",
      url: "城市内涝点位",
      sublayers: [],
      sortOrder: 10098,
      parentCode: "",
      open: false,
      selected: false,
    },
  ],
  xfPoints: [
    {
      xh: "1",
      dwlx: "国家队",
      zz: "王  宇",
      dw: "成都市双流区航鹰路消防救援站",
      zbdh: "028-85770199",
      dz: "双流区东升街道航鹰东路204号",
      ry: "27",
      cl: "7",
      jd: "30.836391",
      wd: "103.946039",
    },
    {
      xh: "2",
      dwlx: "政府专职消防队",
      zz: "龙  秋",
      dw: "成都市双流区公兴政府专职消防队",
      zbdh: "028-62156119",
      dz: "双流区黄龙大道二段2112号",
      ry: "60",
      cl: "14",
      jd: "30.454316",
      wd: "103.987756",
    },
    {
      xh: "3",
      dwlx: "政府专职消防队",
      zz: "苏  李",
      dw: "成都市双流区西航港政府专职消防队",
      zbdh: "028-85830019",
      dz: "双流区西航港街道福通路8号",
      ry: "35",
      cl: "6",
      jd: "30.564837",
      wd: "103.979895",
    },
    {
      xh: "4",
      dwlx: "政府专职消防队",
      zz: "税正洪",
      dw: "成都市双流区黄龙溪镇政府专职消防队",
      zbdh: "028-84266119",
      dz: "成都市双流区黄龙溪镇仿清街88-94号",
      ry: "25",
      cl: "8",
      jd: "30.317183",
      wd: "103.968064",
    },
    {
      xh: "5",
      dwlx: "政府专职消防队",
      zz: "柳河林",
      dw: "成都市双流区九江镇政府专职消防队",
      zbdh: "028-60656119",
      dz: "成都市双流区蛟龙港东升路36号",
      ry: "21",
      cl: "2",
      jd: "30.614985",
      wd: "103.908659",
    },
    {
      xh: "6",
      dwlx: "政府专职消防队",
      zz: "吴永强",
      dw: "成都市双流区彭镇政府专职消防队",
      zbdh: "028-60615119",
      dz: "无",
      ry: "15",
      cl: "5",
      jd: "30.38.0",
      wd: "103.52.37",
    },
    {
      xh: "7",
      dwlx: "一级微型消防站",
      zz: "李  伟",
      dw: "花园社区微型消防站",
      zbdh: "15882338128",
      dz: "东升镇花园社区居委会",
      ry: "5",
      cl: "2",
      jd: "30.588249",
      wd: "103.899134",
    },
    {
      xh: "8",
      dwlx: "一级微型消防站",
      zz: "黄  虎",
      dw: "龙桥社区微型消防站",
      zbdh: "15002861733",
      dz: "东升镇龙桥社区居委会",
      ry: "6",
      cl: "2",
      jd: "30.611291",
      wd: "103.947261",
    },
    {
      xh: "9",
      dwlx: "一级微型消防站",
      zz: "王  涛",
      dw: "三里坝社区微型消防站",
      zbdh: "15828558645",
      dz: "东升镇三里坝社区居委会",
      ry: "5",
      cl: "2",
      jd: "30.561223",
      wd: "103.930496",
    },
    {
      xh: "10",
      dwlx: "二级微型消防站",
      zz: "尚哨兵",
      dw: "白鹤社区微型消防站",
      zbdh: "18030896613",
      dz: "东升镇白鹤社区停车场",
      ry: "5",
      cl: "2",
      jd: "30.575705",
      wd: "103.933304",
    },
    {
      xh: "11",
      dwlx: "二级微型消防站",
      zz: "余怀明",
      dw: "普贤社区微型消防站",
      zbdh: "18981917753",
      dz: "东升镇普贤社区居委会",
      ry: "6",
      cl: "2",
      jd: "30.600949",
      wd: "103.924903",
    },
    {
      xh: "12",
      dwlx: "二级微型消防站",
      zz: "邓乐全",
      dw: "双桂社区微型消防站",
      zbdh: "13550069122",
      dz: "东升镇双桂社区居委会",
      ry: "5",
      cl: "2",
      jd: "30.564461",
      wd: "103.918327",
    },
    {
      xh: "13",
      dwlx: "二级微型消防站",
      zz: "张国坤",
      dw: "大井社区微型消防站",
      zbdh: "85759110",
      dz: "九江镇大井社区居委会",
      ry: "5",
      cl: "2",
      jd: "30.647896",
      wd: "103.932175",
    },
    {
      xh: "14",
      dwlx: "二级微型消防站",
      zz: "郑  建",
      dw: "龙池社区微型消防站",
      zbdh: "13730661080",
      dz: "九江镇龙池社区居委会",
      ry: "6",
      cl: "2",
      jd: "30.652095",
      wd: "103.906899",
    },
    {
      xh: "15",
      dwlx: "二级微型消防站",
      zz: "陈江其",
      dw: "蛟龙港微型消防站",
      zbdh: "13980932594",
      dz: "九江镇蛟龙港城中村",
      ry: "6",
      cl: "2",
      jd: "30.608974",
      wd: "103.909773",
    },
    {
      xh: "16",
      dwlx: "二级微型消防站",
      zz: "王仕亮",
      dw: "草坪社区微型消防站",
      zbdh: "18980762877",
      dz: "公兴湾河街100号",
      ry: "4",
      cl: "2",
      jd: "30.489939",
      wd: "103.996691",
    },
    {
      xh: "17",
      dwlx: "二级微型消防站",
      zz: "钟  斌",
      dw: "三江社区微型消防站",
      zbdh: "13458681995",
      dz: "协和三江悦府9栋旁边",
      ry: "4",
      cl: "2",
      jd: "30.525571",
      wd: "104.021884",
    },
    {
      xh: "18",
      dwlx: "二级微型消防站",
      zz: "李登科",
      dw: "长顺社区微型消防站",
      zbdh: "13551355850",
      dz: "长顺大道一段长顺家园7栋2单元",
      ry: "5",
      cl: "1",
      jd: "30.505777",
      wd: "104.030564",
    },
    {
      xh: "19",
      dwlx: "二级微型消防站",
      zz: "荣洪平",
      dw: "骑龙社区微型消防站",
      zbdh: "15351217310",
      dz: "华府大道二段32号",
      ry: "6",
      cl: "2",
      jd: "30.522584",
      wd: "104.033853",
    },
    {
      xh: "20",
      dwlx: "一级微型消防站",
      zz: "付  强",
      dw: "九龙湖社区微型消防站",
      zbdh: "13880337543",
      dz: "双华路三段华创路口",
      ry: "4",
      cl: "1",
      jd: "30.527318",
      wd: "104.006243",
    },
    {
      xh: "21",
      dwlx: "二级微型消防站",
      zz: "王  杰",
      dw: "常乐社区微型消防站",
      zbdh: "13558829931",
      dz: "常乐社区北六街33栋",
      ry: "11",
      cl: "2",
      jd: "30.585458",
      wd: "103.988144",
    },
    {
      xh: "22",
      dwlx: "一级微型消防站",
      zz: "赖  伟",
      dw: "西航港微型消防站",
      zbdh: "13982182769",
      dz: "光电大道原文星中学内",
      ry: "11",
      cl: "2",
      jd: "30.533212",
      wd: "104.006558",
    },
    {
      xh: "23",
      dwlx: "一级微型消防站",
      zz: "邬平波",
      dw: "机场路社区微型消防站",
      zbdh: "17381991197",
      dz: "临港路四段88号",
      ry: "10",
      cl: "2",
      jd: "30.576888",
      wd: "104.010142",
    },
    {
      xh: "24",
      dwlx: "一级微型消防站",
      zz: "李  秋",
      dw: "金桥社区微型消防站",
      zbdh: "13881977389",
      dz: "金桥镇镇政府内",
      ry: "6",
      cl: "2",
      jd: "30.583089",
      wd: "103.836026",
    },
    {
      xh: "25",
      dwlx: "一级微型消防站",
      zz: "赵中林",
      dw: "红石社区微型消防站",
      zbdh: "13882060539",
      dz: "金桥镇红石社区居委会",
      ry: "5",
      cl: "2",
      jd: "30.551975",
      wd: "103.861935",
    },
    {
      xh: "26",
      dwlx: "二级微型消防站",
      zz: "王顺成",
      dw: "金马村微型消防站",
      zbdh: "13980804775",
      dz: "金桥镇金马村居委会",
      ry: "改造升级",
      cl: "2",
      jd: "30.592512",
      wd: "103.848633",
    },
    {
      xh: "27",
      dwlx: "二级微型消防站",
      zz: "马贵泉",
      dw: "金湾社区微型消防站",
      zbdh: "13882017849",
      dz: "彭镇金湾社区居委会",
      ry: "5",
      cl: "2",
      jd: "30.603556",
      wd: "103.887249",
    },
    {
      xh: "28",
      dwlx: "二级微型消防站",
      zz: "何  军",
      dw: "兴福社区微型消防站",
      zbdh: "85895328",
      dz: "彭镇兴福社区居委会",
      ry: "4",
      cl: "2",
      jd: "30.631378",
      wd: "103.884002",
    },
    {
      xh: "29",
      dwlx: "一级微型消防站",
      zz: "蒋兴其",
      dw: "羊坪社区微型消防站",
      zbdh: "13458538686",
      dz: "彭镇羊坪社区居委会",
      ry: "4",
      cl: "2",
      jd: "30.604585",
      wd: "103.860646",
    },
    {
      xh: "30",
      dwlx: "一级微型消防站",
      zz: "刘  涛",
      dw: "长沟社区微型消防站",
      zbdh: "15881069007",
      dz: "黄水镇长沟社区居委会",
      ry: "6",
      cl: "2",
      jd: "30.562469",
      wd: "103.912253",
    },
    {
      xh: "31",
      dwlx: "一级微型消防站",
      zz: "张显伟",
      dw: "红桥社区微型消防站",
      zbdh: "13908200571",
      dz: "黄水镇红桥社区居委会",
      ry: "6",
      cl: "2",
      jd: "30.534123",
      wd: "103.895685",
    },
    {
      xh: "32",
      dwlx: "二级微型消防站",
      zz: "赵  建",
      dw: "文武社区微型消防站",
      zbdh: "13980443461",
      dz: "黄水镇杨工社区居委会",
      ry: "6",
      cl: "1",
      jd: "30.529657",
      wd: "103.847389",
    },
    {
      xh: "33",
      dwlx: "二级微型消防站",
      zz: "林  刚",
      dw: "桃荚社区微型消防站",
      zbdh: "18084833637",
      dz: "黄水镇桃荚社区居委会",
      ry: "6",
      cl: "1",
      jd: "30.524066",
      wd: "103.886446",
    },
    {
      xh: "34",
      dwlx: "二级微型消防站",
      zz: "赵庆民",
      dw: "应天寺社区微型消防站",
      zbdh: "15982898848",
      dz: "黄水镇应天寺社区居委会",
      ry: "6",
      cl: "1",
      jd: "30.536842",
      wd: "103.916452",
    },
    {
      xh: "35",
      dwlx: "一级微型消防站",
      zz: "杨先乾",
      dw: "白塔社区微型消防站",
      zbdh: "13981846513",
      dz: "黄水镇白塔社区旁",
      ry: "6",
      cl: "1",
      jd: "30.505231",
      wd: "103.915327",
    },
    {
      xh: "36",
      dwlx: "二级微型消防站",
      zz: "周  吉",
      dw: "巡逻大队微型消防站",
      zbdh: "15928637735",
      dz: "永安镇太平街7号",
      ry: "10",
      cl: "2",
      jd: "30.415285",
      wd: "103.989692",
    },
    {
      xh: "37",
      dwlx: "二级微型消防站",
      zz: "温由忠",
      dw: "双坝村微型消防站",
      zbdh: "13551279161",
      dz: "永安镇双坝村居民议事委员会",
      ry: "10",
      cl: "2",
      jd: "30.364113",
      wd: "103.980247",
    },
    {
      xh: "38",
      dwlx: "二级微型消防站",
      zz: "史云锋",
      dw: "双华社区微型消防站",
      zbdh: "13548051323",
      dz: "西航港大道230号",
      ry: "5",
      cl: "1",
      jd: "30.536823",
      wd: "103.977625",
    },
    {
      xh: "39",
      dwlx: "一级微型消防站",
      zz: "罗传金",
      dw: "八角社区微型消防站",
      zbdh: "13980047600",
      dz: "黄甲三叶街32号八角居内",
      ry: "10",
      cl: "2",
      jd: "30.495502",
      wd: "103.959286",
    },
    {
      xh: "40",
      dwlx: "一级微型消防站",
      zz: "罗  波",
      dw: "双兴社区微型消防站",
      zbdh: "13032861500",
      dz: "双兴二社区综合楼大门口",
      ry: "11",
      cl: "2",
      jd: "30.461416",
      wd: "103.994095",
    },
    {
      xh: "41",
      dwlx: "一级微型消防站",
      zz: "付  淋",
      dw: "青云寺市场微型消防站",
      zbdh: "13730898032",
      dz: "青云寺村四组142号村委会旁",
      ry: "10",
      cl: "1",
      jd: "30.456392",
      wd: "103.960482",
    },
    {
      xh: "42",
      dwlx: "二级微型消防站",
      zz: "杜成利",
      dw: "文庙社区微型消防站",
      zbdh: "13808015166",
      dz: "黄甲文庙社区居委会",
      ry: "7",
      cl: "1",
      jd: "30.504931",
      wd: "103.915487",
    },
    {
      xh: "43",
      dwlx: "一级微型消防站",
      zz: "樊梓柯",
      dw: "嘉禾村微型消防站",
      zbdh: "18384268690",
      dz: "黄龙溪嘉禾村广场",
      ry: "5",
      cl: "2",
      jd: "30.337292",
      wd: "103.961715",
    },
    {
      xh: "44",
      dwlx: "一级微型消防站",
      zz: "徐  东",
      dw: "东岳村微型消防站",
      zbdh: "15928123176",
      dz: "双流区特色农产品展示交易中心",
      ry: "5",
      cl: "2",
      jd: "30.382644",
      wd: "103.972282",
    },
  ],
  csnlPoints: [
    {
      xh: "1",
      纬度: "30.561398",
      经度: "103.926583",
      地址: "长顺路四段119号",
      所在社区: "广都社区",
      所属街道: "东升街道",
    },
    {
      xh: "2",
      纬度: "30.58563",
      经度: "103.940324",
      地址: "商都路",
      所在社区: "迎春桥社区",
      所属街道: "东升街道",
    },
    {
      xh: "3",
      纬度: "30.589486",
      经度: "103.94719",
      地址: "广巍路与成双大道十字路口",
      所在社区: "迎春桥社区",
      所属街道: "东升街道",
    },
    {
      xh: "4",
      纬度: "30.570994",
      经度: "103.916038",
      地址: "棠湖南路二、三段，电视塔路一段城中村",
      所在社区: "城塔社区",
      所属街道: "东升街道",
    },
    {
      xh: "5",
      纬度: "30.545044",
      经度: "103.932524",
      地址: "长兴社区八组（原长兴3社）",
      所在社区: "长兴社区",
      所属街道: "东升街道",
    },
    {
      xh: "6",
      纬度: "30.576865",
      经度: "103.922005",
      地址: "东升街道紫东阁社区城北下街",
      所在社区: "紫东阁社区",
      所属街道: "东升街道",
    },
    {
      xh: "7",
      纬度: "30.567752",
      经度: "103.932394",
      地址: "东升街道白鹤社区（原东升二社）",
      所在社区: "白鹤社区",
      所属街道: "东升街道",
    },
    {
      xh: "8",
      纬度: "30.565033",
      经度: "103.927667",
      地址: "东升街道白鹤社区（原东升四社）",
      所在社区: "白鹤社区",
      所属街道: "东升街道",
    },
    {
      xh: "9",
      纬度: "30.564427",
      经度: "103.928752",
      地址: "东升街道长顺路三段194号",
      所在社区: "白鹤社区",
      所属街道: "东升街道",
    },
    {
      xh: "10",
      纬度: "30.587513",
      经度: "103.893196",
      地址: "东升街道花园社区双江路",
      所在社区: "花园社区",
      所属街道: "东升街道",
    },
    {
      xh: "11",
      纬度: "30.574841",
      经度: "103.920676",
      地址: "西安路一段21号新春小区四合院耙耙菜",
      所在社区: "花月街社区",
      所属街道: "东升街道",
    },
    {
      xh: "12",
      纬度: "30.576339",
      经度: "103.916832",
      地址: "五洞桥社区吴家坝南街21-25号",
      所在社区: "五洞桥社区",
      所属街道: "东升街道",
    },
    {
      xh: "13",
      纬度: "30.60305",
      经度: "103.928242",
      地址: "星空路三段与双楠大道十字路口",
      所在社区: "葛陌社区",
      所属街道: "东升街道",
    },
    {
      xh: "14",
      纬度: "30.550838",
      经度: "103.99054",
      地址: "光明社区长城路一段",
      所在社区: "光明社区",
      所属街道: "西航港街道",
    },
    {
      xh: "15",
      纬度: "30.564081",
      经度: "104.024173",
      地址: "川大路东延线与大同路、成雅高速桥下",
      所在社区: "成白路社区",
      所属街道: "西航港街道",
    },
    {
      xh: "16",
      纬度: "30.544778",
      经度: "103.985766",
      地址: "空港一路与老双中路交汇处江安丽苑大门口",
      所在社区: "蜀星社区",
      所属街道: "西航港街道",
    },
    {
      xh: "17",
      纬度: "30.572268",
      经度: "103.985353",
      地址: "锦华路三段（博达路口）",
      所在社区: "常乐社区",
      所属街道: "西航港街道",
    },
    {
      xh: "18",
      纬度: "30.567573",
      经度: "103.999684",
      地址: "红英小区华山路与泰山路交汇处",
      所在社区: "机场路社区",
      所属街道: "西航港街道",
    },
    {
      xh: "19",
      纬度: "30.571116",
      经度: "104.027679",
      地址: "黄荆路与成雅高速桥下交汇处",
      所在社区: "桂花堰社区",
      所属街道: "西航港街道",
    },
    {
      xh: "20",
      纬度: "30.534082",
      经度: "103.973513",
      地址: "双华路与西航港大道交汇口（邻里中心）",
      所在社区: "龙港社区",
      所属街道: "西航港街道",
    },
    {
      xh: "21",
      纬度: "30.559587",
      经度: "104.013639",
      地址: "川大路与珠江路交汇处",
      所在社区: "花红社区",
      所属街道: "西航港街道",
    },
    {
      xh: "22",
      纬度: "30.672943",
      经度: "104.051796",
      地址: "半边路油碾巷黄堰河段",
      所在社区: "白家场社区",
      所属街道: "西航港街道",
    },
    {
      xh: "23",
      纬度: "30.521719",
      经度: "103.990935",
      地址: "空港三路段、龙湖家园后门",
      所在社区: "九龙湖社区",
      所属街道: "西航港街道",
    },
    {
      xh: "24",
      纬度: "30.453287",
      经度: "103.952492",
      地址: "双永路铁路大桥下",
      所在社区: "青云寺社区",
      所属街道: "黄甲街道",
    },
    {
      xh: "25",
      纬度: "30.511275",
      经度: "104.021039",
      地址: "柏顺路棠湖泊林城外围路",
      所在社区: "三江社区",
      所属街道: "怡心街道",
    },
    {
      xh: "26",
      纬度: "30.488626",
      经度: "104.032909",
      地址: "地铁5号线南湖立交A口",
      所在社区: "高峰社区",
      所属街道: "怡心街道",
    },
    {
      xh: "27",
      纬度: "30.574376",
      经度: "103.886724",
      地址: "党校口",
      所在社区: "光荣社区",
      所属街道: "彭镇",
    },
    {
      xh: "28",
      纬度: "30.626385",
      经度: "103.942963",
      地址: "草金路绕城高速段",
      所在社区: "金岛社区",
      所属街道: "九江街道",
    },
    {
      xh: "29",
      纬度: "30.633122",
      经度: "103.943553",
      地址: "成新蒲江安立交",
      所在社区: "马家寺社区",
      所属街道: "九江街道",
    },
    {
      xh: "30",
      纬度: "30.63519",
      经度: "103.929068",
      地址: "大井东街与成新蒲交接处",
      所在社区: "万家社区",
      所属街道: "九江街道",
    },
  ],
  cszyList: [
    // { "code": 10018, "icon": "", open: false, selected: false, type: 'layer',  "name": "行政区划", "url": "/gateway/gis/1/ea77647c00c64bd782cb38799e2b88dd" },
    // { "code": 10019, "icon": "", open: false, selected: false, "name": "村社区界", "url": "/gateway/gis/1/5f651de6590d4583ad2943027c86cb9e" },
    // { "code": 10035, "icon": "", open: false, selected: false, "name": "村级行政区", "url": "/gateway/gis/1/2eeb6c6b85194cd6992f273ac0b998c6" },
    // {
    //     "code": 10007, "icon": "", open: false, selected: false, "name": "地铁线路", "url": "/gateway/gis/1/c30b40a428f1444f9efb550ba5a5ef42", popupTemplate: { // 属性弹窗模板
    //         title: '{名称}',      // 弹窗标题
    //         content: [{           // 弹窗内容
    //             type: 'fields',     // 根据图层属性字段信息构建弹窗内容
    //             fieldInfos: [{      // 图层属性字段信息
    //                 fieldName: '酒店名称', // 属性字段
    //                 label: '酒店名称',     // 属性字段别名（显示名称）
    //             }, {
    //                 fieldName: '完整地址',
    //                 label: '完整地址',
    //             }, {
    //                 fieldName: '联系电话',
    //                 label: '联系电话',
    //             }]
    //         }]
    //     }
    // },
    // {
    //     "code": 10004, "icon": "", open: false, selected: false, "name": "地铁站", "url": "/gateway/gis/1/d7e7abe5a70447929f2491e97e90c345", popupTemplate: { // 属性弹窗模板
    //         title: '{名称}',      // 弹窗标题
    //         content: [{           // 弹窗内容
    //             type: 'fields',     // 根据图层属性字段信息构建弹窗内容
    //             fieldInfos: [{      // 图层属性字段信息
    //                 fieldName: '酒店名称', // 属性字段
    //                 label: '酒店名称',     // 属性字段别名（显示名称）
    //             }, {
    //                 fieldName: '完整地址',
    //                 label: '完整地址',
    //             }, {
    //                 fieldName: '联系电话',
    //                 label: '联系电话',
    //             }]
    //         }]
    //     }
    // },
    // { "code": 10013, "icon": "", open: false, selected: false, "name": "规划地铁线及站点", "url": "/gateway/gis/1/0762cdc0aa3546b79c9caeb296cdbc12" },
    // {
    //     "code": 10010, "icon": "", open: false, selected: false, "name": "地铁出入口", "url": "/gateway/gis/1/3c2163aac2674f3aaeabfd203f75d9a1", popupTemplate: { // 属性弹窗模板
    //         title: '{名称}',      // 弹窗标题
    //         content: [{           // 弹窗内容
    //             type: 'fields',     // 根据图层属性字段信息构建弹窗内容
    //             fieldInfos: [{      // 图层属性字段信息
    //                 fieldName: '酒店名称', // 属性字段
    //                 label: '酒店名称',     // 属性字段别名（显示名称）
    //             }, {
    //                 fieldName: '完整地址',
    //                 label: '完整地址',
    //             }, {
    //                 fieldName: '联系电话',
    //                 label: '联系电话',
    //             }]
    //         }]
    //     }
    // },
    // {
    //     "code": 10011, "icon": "", open: false, selected: false, "name": "地铁维修厂", "url": "/gateway/gis/1/eb831f0cc8184a1b8a9e010ce4a1a2df", popupTemplate: { // 属性弹窗模板
    //         title: '{名称}',      // 弹窗标题
    //         content: [{           // 弹窗内容
    //             type: 'fields',     // 根据图层属性字段信息构建弹窗内容
    //             fieldInfos: [{      // 图层属性字段信息
    //                 fieldName: '酒店名称', // 属性字段
    //                 label: '酒店名称',     // 属性字段别名（显示名称）
    //             }, {
    //                 fieldName: '完整地址',
    //                 label: '完整地址',
    //             }, {
    //                 fieldName: '联系电话',
    //                 label: '联系电话',
    //             }]
    //         }]
    //     }
    // },
    // { "code": 10016, "icon": "", open: false, selected: false, "name": "地铁车辆段", "url": "/gateway/gis/1/d9f2e140e63b46678b0242c282c7d83a" },
    // { "code": 10022, "icon": "", open: false, selected: false, "name": "轨道项目", "url": "/gateway/gis/1/0a2ec25716224160bbb613632f5c2954" },
    // {
    //     "code": 10001, "icon": "", open: false, selected: false, "name": "酒店", "url": "/gateway/gis/1/7d9d0cbd5e17418eab5596d76f82efec", popupTemplate: { // 属性弹窗模板
    //         title: '{酒店名称}',      // 弹窗标题
    //         content: [{           // 弹窗内容
    //             type: 'fields',     // 根据图层属性字段信息构建弹窗内容
    //             fieldInfos: [{      // 图层属性字段信息
    //                 fieldName: '酒店名称', // 属性字段
    //                 label: '酒店名称',     // 属性字段别名（显示名称）
    //             }, {
    //                 fieldName: '等级',
    //                 label: '等级',
    //             }, {
    //                 fieldName: '分类',
    //                 label: '分类',
    //             }, {
    //                 fieldName: '区域',
    //                 label: '区域',
    //             }, {
    //                 fieldName: '完整地址',
    //                 label: '完整地址',
    //             }, {
    //                 fieldName: '联系电话',
    //                 label: '联系电话',
    //             }, {
    //                 fieldName: '开放时间',
    //                 label: '开放时间',
    //             }, {
    //                 fieldName: '封面',
    //                 label: '封面',
    //             }]
    //         }]
    //     }
    // },
    // {
    //     "code": 10002, "icon": "", open: false, selected: false, "name": "抵离交通枢纽", "url": "/gateway/gis/1/bbb0ea9e1dde4db2aa2568d3a487eb3b", popupTemplate: { // 属性弹窗模板
    //         title: '{名称}',      // 弹窗标题
    //         content: [{           // 弹窗内容
    //             type: 'fields',     // 根据图层属性字段信息构建弹窗内容
    //             fieldInfos: [{      // 图层属性字段信息
    //                 fieldName: '酒店名称', // 属性字段
    //                 label: '酒店名称',     // 属性字段别名（显示名称）
    //             }, {
    //                 fieldName: '完整地址',
    //                 label: '完整地址',
    //             }, {
    //                 fieldName: '联系电话',
    //                 label: '联系电话',
    //             }]
    //         }]
    //     }
    // },
    // {
    //     "code": 10003, "icon": "", open: false, selected: false, "name": "客运汽车站", "url": "/gateway/gis/1/5cb7c5ee1e93443b819db300789eb9f3", popupTemplate: { // 属性弹窗模板
    //         title: '{名称}',      // 弹窗标题
    //         content: [{           // 弹窗内容
    //             type: 'fields',     // 根据图层属性字段信息构建弹窗内容
    //             fieldInfos: [{      // 图层属性字段信息
    //                 fieldName: '酒店名称', // 属性字段
    //                 label: '酒店名称',     // 属性字段别名（显示名称）
    //             }, {
    //                 fieldName: '完整地址',
    //                 label: '完整地址',
    //             }, {
    //                 fieldName: '联系电话',
    //                 label: '联系电话',
    //             }]
    //         }]
    //     }
    // },
    // {
    //     "code": 10005, "icon": "", open: false, selected: false, "name": "公交电子站牌", "url": "/gateway/gis/1/91bb55dfdc9c44ebb5733962124c899a", popupTemplate: { // 属性弹窗模板
    //         title: '{名称}',      // 弹窗标题
    //         content: [{           // 弹窗内容
    //             type: 'fields',     // 根据图层属性字段信息构建弹窗内容
    //             fieldInfos: [{      // 图层属性字段信息
    //                 fieldName: '酒店名称', // 属性字段
    //                 label: '酒店名称',     // 属性字段别名（显示名称）
    //             }, {
    //                 fieldName: '完整地址',
    //                 label: '完整地址',
    //             }, {
    //                 fieldName: '联系电话',
    //                 label: '联系电话',
    //             }]
    //         }]
    //     }
    // },
    // {
    //     "code": 10006, "icon": "", open: false, selected: false, "name": "公交维修点", "url": "/gateway/gis/1/c94d8d7571694d98a0ee86db9ed08416", popupTemplate: { // 属性弹窗模板
    //         title: '{名称}',      // 弹窗标题
    //         content: [{           // 弹窗内容
    //             type: 'fields',     // 根据图层属性字段信息构建弹窗内容
    //             fieldInfos: [{      // 图层属性字段信息
    //                 fieldName: '酒店名称', // 属性字段
    //                 label: '酒店名称',     // 属性字段别名（显示名称）
    //             }, {
    //                 fieldName: '完整地址',
    //                 label: '完整地址',
    //             }, {
    //                 fieldName: '联系电话',
    //                 label: '联系电话',
    //             }]
    //         }]
    //     }
    // },
    // {
    //     "code": 10008, "icon": "", open: false, selected: false, "name": "公交线路", "url": "/gateway/gis/1/b772c159aebf401884bcd98b0c4f9800", popupTemplate: { // 属性弹窗模板
    //         title: '{名称}',      // 弹窗标题
    //         content: [{           // 弹窗内容
    //             type: 'fields',     // 根据图层属性字段信息构建弹窗内容
    //             fieldInfos: [{      // 图层属性字段信息
    //                 fieldName: '酒店名称', // 属性字段
    //                 label: '酒店名称',     // 属性字段别名（显示名称）
    //             }, {
    //                 fieldName: '完整地址',
    //                 label: '完整地址',
    //             }, {
    //                 fieldName: '联系电话',
    //                 label: '联系电话',
    //             }]
    //         }]
    //     }
    // },
    // {
    //     "code": 10009, "icon": "", open: false, selected: false, "name": "公交规划线路", "url": "/gateway/gis/1/baaf7015bb21498e95db48a406b143ce", popupTemplate: { // 属性弹窗模板
    //         title: '{名称}',      // 弹窗标题
    //         content: [{           // 弹窗内容
    //             type: 'fields',     // 根据图层属性字段信息构建弹窗内容
    //             fieldInfos: [{      // 图层属性字段信息
    //                 fieldName: '酒店名称', // 属性字段
    //                 label: '酒店名称',     // 属性字段别名（显示名称）
    //             }, {
    //                 fieldName: '完整地址',
    //                 label: '完整地址',
    //             }, {
    //                 fieldName: '联系电话',
    //                 label: '联系电话',
    //             }]
    //         }]
    //     }
    // },
    // {
    //     "code": 10012, "icon": "", open: false, selected: false, "name": "公交专用道", "url": "/gateway/gis/1/2c2533e9d7d449e18f263411b4dda596", popupTemplate: { // 属性弹窗模板
    //         title: '{名称}',      // 弹窗标题
    //         content: [{           // 弹窗内容
    //             type: 'fields',     // 根据图层属性字段信息构建弹窗内容
    //             fieldInfos: [{      // 图层属性字段信息
    //                 fieldName: '酒店名称', // 属性字段
    //                 label: '酒店名称',     // 属性字段别名（显示名称）
    //             }, {
    //                 fieldName: '完整地址',
    //                 label: '完整地址',
    //             }, {
    //                 fieldName: '联系电话',
    //                 label: '联系电话',
    //             }]
    //         }]
    //     }
    // },
    // { "code": 10014, "icon": "", open: false, selected: false, "name": "公交规划线路", "url": "/gateway/gis/1/f4b372cae5b848fabfe8ab6ff7f387db" },
    // { "code": 10015, "icon": "", open: false, selected: false, "name": "公交站点", "url": "/gateway/gis/1/9ef6d1a054164d2984e5c2f86a0d8da0" },
    // { "code": 10017, "icon": "", open: false, selected: false, "name": "公交场站", "url": "/gateway/gis/1/527e0d30880f4073838e1049e97c2829" },
    // {
    //     "code": 10032, "icon": "", open: false, selected: false, "name": "大运场馆", "children": [
    //         { "code": "10032-0001", "icon": "", open: false, selected: false, "name": "双流体育中心", "url": "/gateway/gis/1/5d3bc80439ea428ba20fa882e86a5b8b" },
    //         { "code": "10032-0002", "icon": "", open: false, selected: false, "name": "四川川投国际网球中心", "url": "/gateway/gis/1/37220769f79443a0a8be8925b2421ebe" },
    //         { "code": "10032-0003", "icon": "", open: false, selected: false, "name": "成都现代五项赛事中心", "url": "/gateway/gis/1/c106cbc8e23f4e13986a20a49e405607" },
    //     ]
    // },
    // { "code": 10020, "icon": "", open: false, selected: false, "name": "建筑白模", "url": "/gateway/gis/1/1bb037b836ca483c8c5c93444293b320" },
    // { "code": 10023, "icon": "", open: false, selected: false, "name": "市政项目", "url": "/gateway/gis/1/58cc3a1c0487469fbc1123cb961cb7b2" },
    // { "code": 10031, "icon": "", open: false, selected: false, "name": "市政项目", "url": "/gateway/gis/1/29ba71d6088d4774883ae9b29f61fe6b" },
    // { "code": 10021, "icon": "", open: false, selected: false, "name": "房建项目", "url": "/gateway/gis/1/e6c48b3841f6409789f69cc894bd321b" },
    // { "code": 10030, "icon": "", open: false, selected: false, "name": "房建项目-竣工", "url": "/gateway/gis/1/f30b1aaa460c41c7a7d8a101a768d71e" },
    // { "code": 10024, "icon": "", open: false, selected: false, "name": "控规地块", "url": "/gateway/gis/1/b080c03871144647b1d757df689b0981" },
    // { "code": 10026, "icon": "", open: false, selected: false, "name": "学校", "url": "/gateway/gis/1/dea7aba6931c48d2bbe5bcc4544871ae" },
    // { "code": 10028, "icon": "", open: false, selected: false, "name": "医院", "url": "/gateway/gis/1/6a8072d32ac24873a78d8089d3d575e1" },
    // { "code": 10036, "icon": "", open: false, selected: false, "name": "充电桩", "url": "/gateway/gis/1/f97e5629451b477cb7b9b3e58fbd74b8" },
    // { "code": 10025, "icon": "", open: false, selected: false, "name": "危货车辆实时定位", "url": "/gateway/gis/1/8dfd1479511a4c8db91a61407ce92027" },
    // { "code": 10027, "icon": "", open: false, selected: false, "name": "地质灾害隐患点", "url": "/gateway/gis/1/fc9cb828c01540a08c98c5b61db38ef0" },
    // { "code": 10029, "icon": "", open: false, selected: false, "name": "经典黑", "url": "/gateway/gis/1/f67a4a985e4a496891b2f3ac445a05cc" },
    // // { "code": 10037, "icon": "", open: false, selected: false, "name": "便民服务中心", "url": "/gateway/gis/1/13b5e0b5840b41d48cf1686cf775790e" },
    // // { "code": 10038, "icon": "", open: false, selected: false, "name": "电影院", "url": "/gateway/gis/1/e8b0f0fb575d433aa1546391c8864050" },
    // // { "code": 10039, "icon": "", open: false, selected: false, "name": "银行", "url": "/gateway/gis/1/883e7b1cffa742a99267c23d0616f437" },
    // // { "code": 10040, "icon": "", open: false, selected: false, "name": "公园", "url": "/gateway/gis/1/12a404348ec04032b2ac1bdaf7370a91" },
    // // { "code": 10041, "icon": "", open: false, selected: false, "name": "商业体", "url": "/gateway/gis/1/89efa581810141a388369a6678d83c6c" },
    // // { "code": 10042, "icon": "", open: false, selected: false, "name": "核酸检测点分布", "url": "/gateway/gis/1/bce618b6a3654f46bbd79f418489a21a" },
  ],
  pointData: {
    摄像头: [],
    微型消防站: [],
    城市内涝点位: [],
  },
  /**
   * 初始化数据
   */
  init() {
    //加载城市资源
    this.loadMapCszy();
    //加载点位数据
    this.loadPointData();
  },
  /**
   * 加载城市资源
   */
  loadMapCszy() {
    const self = this;
    const param = {};
    // OU.api('sjj_cszylb', param).then(res => {
    //     self.cszyList = (res || []).filter(item => item.name !== '核酸检测点分布')
    // })
    self.cszyList = this.cszylb.filter(
      (item) => item.name !== "核酸检测点分布"
    );
  },
  /**
   * 加载点位数据
   */
  loadPointData() {
    //加载摄像头
    this.loadSxtList();
    //加载微型消防站点位数据
    this.loadWxxfz();
    //加载城市内涝点位
    this.loadCsnldw();
  },
  /**
   * 加载摄像头列表
   */
  loadSxtList() {
    const self = this;
    SxtTool.getList([], 1).then((list) => {
      console.log("摄像头：", list);
      self.pointData.摄像头 = list.map((item) => {
        item.id = item.cameraIndexCode;
        if (item.latitude && item.longitude) {
          item.coordinates = [item.latitude, item.longitude];
        } else {
          item.coordinates = null;
        }
        item.url = `${GlobalData.imgUrl}撒点-微型消防站.png`;

        return item;
      });
    });
  },
  /**
   * 加载微型消防站点位数据
   */
  loadWxxfz() {
    const self = this;
    self.pointData.微型消防站 = self.xfPoints.map((item) => {
      item.id = item.xh;
      item.coordinates = [item.wd, item.jd];
      item.url = `${GlobalData.imgUrl}撒点-微型消防站.png`;

      return item;
    });

    // OU.api('sjj_1nr4scpqh00_slqwxxfzd').then(body => {
    //     self.pointData.微型消防站 = body.map(item => {
    //         item.id = item.xh
    //         item.coordinates = [item.wd, item.jd]
    //         item.url = `${GlobalData.imgUrl}撒点-微型消防站.png`

    //         return item
    //     })
    // })
  },
  /**
   * 加载城市内涝点位
   */
  loadCsnldw() {
    const self = this;
    self.pointData.城市内涝点位 = self.csnlPoints.map((item) => {
      item.id = item.xh;
      item.coordinates = [item.纬度, item.经度];
      item.url = `${GlobalData.imgUrl}撒点-城市内涝.png`;
      return item;
    });
    // OU.api('sjj_1nr4scpqh00_sd_csnldw').then(body => {
    //     self.pointData.城市内涝点位 = body.map(item => {
    //         item.id = item.xh
    //         item.coordinates = [item.纬度, item.经度]
    //         item.url = `${GlobalData.imgUrl}撒点-城市内涝.png`

    //         return item
    //     })
    // })
  },
};

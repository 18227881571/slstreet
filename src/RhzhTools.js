import GlobalData from './GlobalData'
import md5 from 'js-md5'
import request from './utils/index'
// import {rhzhLogin} from './utils/api'
const rhurl = '/ps/data/login'
export default {

  // 登录Token
  login_token: '',
  // 节点 Number
  nodeNumber: '',
  //Number
  number: '',
  // 客户端Id
  clientIp: '',

  // 登录
  login(callback) {
    const timeStamp_ = dayjs().format('YYYY-MM-DD HH:mm:ss')
    const str = 'PTYT' + timeStamp_ + '110000' + '110000'
    const param = {
      timeStamp: timeStamp_,
      number: '110000',
      encrypt: md5(str)
    }
    console.log('登录传参验证',param)
    const self = this
    // 验证登录
    let login = (param) =>{
      return request({
        method: "post",
        url: '/ps/data/login',
        param,
      });
    }
    login(param).then(res=>{
      if (res.code === 0) {
        self.login_token = res.data.token
        self.nodeNumber = res.data.nodeNumber
        self.number = res.data.number
        self.clientIp = res.data.clientIp
        if (callback) {
          callback(res.data)
        }
      } else {
        self.login_token = ''
      }
      console.log('融合指挥登录成功', self.login_token)
    })
    // request({
    //   method: 'post',
    //   url: rhurl,
    //   param
    // }).then((res) => {
    //   if (res.code === 0) {
    //     self.login_token = res.data.token
    //     self.nodeNumber = res.data.nodeNumber
    //     self.number = res.data.number
    //     self.clientIp = res.data.clientIp
    //     if (callback) {
    //       callback(res.data)
    //     }
    //   } else {
    //     self.login_token = ''
    //   }
    //   console.log('融合指挥登录成功', self.login_token)
    // })
    // OU.api('sjj_jd_rhzh_yzdl', param).then(res => {
    //   if (res.code === 0) {
    //     self.login_token = res.data.token
    //     self.nodeNumber = res.data.nodeNumber
    //     self.number = res.data.number
    //     self.clientIp = res.data.clientIp
    //     if (callback) {
    //       callback(res.data)
    //     }
    //   } else {
    //     self.login_token = ''
    //   }
    // })
  },
  // 组装头部信息
  get_Headers() {
    const headers = {
      token: this.login_token
    }
    return headers
  },

  //登录
  logout(number) {
    const action = '/ps/data/logout/' + number
    const header_ = this.get_Headers()
    // OU.api('sjj_rhzh_tyjk', { _url: action }, header_).then(res => { })
  },

  //1 节点详细信息
  nodeInfo(nodeNumber, callback) {
    const action = '/ps/data/nodeInfo/' + nodeNumber
    const header_ = this.get_Headers()
    // OU.api('sjj_jd_rhzh_tyjk', { _url: action }, header_).then(res => {
    //   if (callback) {
    //     callback(res.data)
    //   }
    // })
  },


  //2 节点子组件列表信息
  nodeSubGroupList(nodeNumber) {
    return new Promise((resovle, reject) => {
      const action ='/ps/data/nodeSubGroupList/' + nodeNumber
      const header_ = this.get_Headers()
      request({
        method: 'get',
        url: action,
        headers: header_
      }).then(res => {
        // 过滤分组信息
        // GlobalData.street
        const nodeSubGroupList = res.data || []
        const dataList = nodeSubGroupList.filter(i => i.name === GlobalData.street)
        resovle(dataList)
        // callback(res.data)
      })
      // OU.api('sjj_jd_rhzh_tyjk', { _url: action }, header_).then(res => {

      //   // 过滤分组信息
      //   // GlobalData.street
      //   const nodeSubGroupList = res.data || []
      //   const dataList = nodeSubGroupList.filter(i => i.name === GlobalData.street)
      //   resovle(dataList)
      //   // callback(res.data)
      // })
    })
  },

  //3 节点下所有子对象列表查询
  nodeSubDataList(nodeNumber) {
    return new Promise((resovle, reject) => {
      const action = '/ps/data/nodeSubDataList/' + nodeNumber
      const header_ = this.get_Headers()
      // OU.api('sjj_jd_rhzh_tyjk', { _url: action }, header_).then(res => {
      //   resovle(res.data)
      // })
    })
  },

  // //3 节点下所有子对象列表查询
  // nodeSubDataList(nodeNumber, callback) {
  //   const action = '/ps/data/nodeSubDataList/' + nodeNumber
  //   const header_ = this.get_Headers()
  //   OU.api('sjj_rhzh_tyjk', { _url: action }, header_).then(res => {
  //     if (callback) {
  //       debugger
  //       callback(res.data)
  //     }
  //   })
  // }
}
import request from './utils/index'
export default {

  //token
  token: null,
  tokenOutDate: null,

  //请求数据
  postData(action, param, access_token) {
    const url = `/artemis/${action}`
    const headers = {
      'Content-Type': 'application/json',
      access_token
    }
    debugger
    param = param || {}
    // return new Promise((resovle, reject) => {
    // OU.api('sjj_1nr4scpqh00_spzs', { url, param, headers }).then(res => {
    //   if (res && res.length > 0 && res[0] && res[0].res) {
    //     resovle(JSON.parse(res[0].res))
    //   } else {
    //     reject()
    //   }
    // })
    // })
    return request({
      method: 'POST',
      url: url,
      data: {},
      headers: headers,
    }).then(res => {
      if (res && res.length > 0 && res[0] && res[0].res) {
        return(JSON.parse(res[0].res))
      }
    })
  },
  //获取Token
  getToken() {
    return new Promise((resovle, reject) => {
      if (this.token && this.tokenOutDate && dayjs().diff(this.tokenOutDate, 'second') < 0) {
        resovle(this.token)
        return
      }
      const action = 'oauth/token?client_id=25060665&client_secret=z9gRId796NdDuTmb53j7'
      this.postData(action, {}).then(data => {
        try {
          const access_token = data.access_token
          const expires_in = +data.expires_in

          if (access_token && expires_in > 10) {
            this.token = access_token
            this.tokenOutDate = dayjs().add(expires_in - 30, 'second')

            resovle(this.token)
          }
        } catch (err) {
          reject()
        }
      })
    })
  },
  /**
   * 获取摄像头列表
   * @param list 已获取到的数据
   * @pageNo 页码，从1开始
   */
  getList(list, pageNo) {
    list = list || []
    pageNo = pageNo || 1
    const pageSize = 1000

    return new Promise((resovle, reject) => {
      const param = {
        pageNo,
        pageSize
      }
      const action = 'api/resource/v1/cameras'
      this.postData(action, param, '').then(data => {
        if (data && data.data) {
          const total = data.data.total || 0
          list = list.concat(data.data.list || [])

          if (list.length > 0 && data.data.list.length > 0 && list.length < total) {
            this.getList([...list], pageNo + 1).then(res => {
              resovle(res)
            }).catch(err => {
              resovle(list)
            })
          } else {
            resovle(list)
          }
        } else {
          resovle([])
        }
      }).catch(err => {
        reject
      })
      // this.getToken().then(access_token => {
      //   this.postData(action, param, access_token).then(data => {
      //     if (data && data.data) {
      //       const total = data.data.total || 0
      //       list = list.concat(data.data.list || [])

      //       if (list.length > 0 && data.data.list.length > 0 && list.length < total) {
      //         this.getList([...list], pageNo + 1).then(res => {
      //           resovle(res)
      //         }).catch(err => {
      //           resovle(list)
      //         })
      //       } else {
      //         resovle(list)
      //       }
      //     } else {
      //       resovle([])
      //     }
      //   }).catch(err => {
      //     reject(err)
      //   })
      // }).catch(err => {
      //   reject(err)
      // })
    })
  },
  /**
   * 获取摄像头Hls
   * @param cameraIndexCode 通道编码
   */
  getHls(cameraIndexCode) {
    return new Promise((resovle, reject) => {
      const param = {
        cameraIndexCode,
        streamType: 0,
        protocol: 'hls',
        transmode: 0
      }
      const action = '/api/video/v1/cameras/previewURLs'
      this.getToken().then(access_token => {
        this.postData(action, param, access_token).then(data => {
          if (data && data.data && data.data.url) {
            resovle({ cameraIndexCode, url: data.data.url })
            return
          }
          reject()
        }).catch(err => {
          reject(err)
        })
      }).catch(err => {
        reject(err)
      })
    })
  }
}
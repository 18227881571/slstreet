export default {
    statisList: [{
      name: "分类统计",
      id: "01",
      unit: '件',
      value: '0',
      children: [
        {
            "mainTypeCode": "12005007",
            "finishCount": 3467,
            "mainTypeName": "市容环境",
            "handleCount": 4420,
            "name": "市容环境",
            "processed": 3467,
            "inprocess": 4420
        },
        {
            "mainTypeCode": "18009",
            "finishCount": 41,
            "mainTypeName": "消防安全",
            "handleCount": 7411,
            "name": "消防安全",
            "processed": 41,
            "inprocess": 7411
        },
        {
            "mainTypeCode": "12005011",
            "finishCount": 1825,
            "mainTypeName": "街面秩序",
            "handleCount": 2898,
            "name": "街面秩序",
            "processed": 1825,
            "inprocess": 2898
        },
        {
            "mainTypeCode": "23003",
            "finishCount": 14,
            "mainTypeName": "环境污染",
            "handleCount": 3198,
            "name": "环境污染",
            "processed": 14,
            "inprocess": 3198
        },
        {
            "mainTypeCode": "19005",
            "finishCount": 3402,
            "mainTypeName": "安全生产",
            "handleCount": 1810,
            "name": "安全生产",
            "processed": 3402,
            "inprocess": 1810
        },
        {
            "mainTypeCode": "99001",
            "finishCount": 3414,
            "mainTypeName": "其他大类",
            "handleCount": 1578,
            "name": "其他大类",
            "processed": 3414,
            "inprocess": 1578
        },
        {
            "mainTypeCode": "12001009",
            "finishCount": 0,
            "mainTypeName": "房屋管理",
            "handleCount": 1463,
            "name": "房屋管理",
            "processed": 0,
            "inprocess": 1463
        },
        {
            "mainTypeCode": "14006",
            "finishCount": 96,
            "mainTypeName": "食品药品监管",
            "handleCount": 1004,
            "name": "食品药品监管",
            "processed": 96,
            "inprocess": 1004
        }
    ]
    }, {
      name: "超期未办结",
      id: "02",
      unit: '件',
      value: '0',
      children: [{
        name: "网络理政紧急件",
        id: "02-1",
        unit: '件',
        processed: '0',
        inprocess: '0'
      }]
    }, {
      name: "市级下派",
      id: "03",
      unit: '件',
      value: '0',
      children: [{
        name: "多跨事件",
        id: "03-1",
        unit: '件',
        processed: '0',
        inprocess: '0'
      }, {
        name: "重点事件",
        id: "03-2",
        unit: '件',
        processed: '0',
        inprocess: '0'
      }]
    }]
  }
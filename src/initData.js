import MainLeftFxyjData from './MainLeftFxyjData'
import MainLeftZbtxData from './MainLeftZbtxData'
import MainLeftGgaqData from './MainLeftGgaqData'
import MainLeftAqsjData from './MainLeftAqsjData'
import MainRightGgglData from './MainRightGgglData'
import MainRightGgfwData from './MainRightGgfwData'
import MapSourceData from './MapSourceData'
import MainTopData from './MainTopData'
import MainLeftRdyqData from './MainLeftRdyqData'
import MainEventData from './MainEventData'
import MainEventListData from './MainEventListData'
import GlobalTool from './GlobalTool'
import GlobalData from './GlobalData'
import MainLeftSjzxData from './MainLeftSjzxData'

import {
    selectStreetEventTrend,
    streetTypeRatio,
    streetEventSummary
} from './utils/api'
export default {
    /**
   * 获取地址栏参数
   */
    getQueryVariable(name) {
        const href = window.location.href

        if (!href) {
            return null
        }
        const param =
            (new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(href) || ['', ''])[1] || ''

        return decodeURIComponent(param.replace(/\+/g, '%20')) || null
    },
    /**
     * 初始化
     */
    init() {
        GlobalData.street = this.getQueryVariable('street') || '东升街道'
        const configList = [
            //加载配置数据
            this.loadConfig(),
            //加载街道配置
            this.loadJdConfig()
        ]
        Promise.all(configList).then(res => {
            GlobalData.loadConfigFinished = true
            this.loadData()
            //加载城市资源
            this.loadMapCszy()
            // //加载热点舆情
            // this.loadRdyq()
            // //加载安全事件统计
            // this.loadAqsjtj()
            // //加载12345热点问题-本月热点诉求统计
            // this.load12345rdwtByrdsqtj()
            // //一网通办-近6月办件情况统计
            // this.loadYwtbJ6ybjqktj()
            //加载外部系统接入
            this.loadWbxtjr()
            //加载事件列表
            this.loadEventList()
            //初始事件筛选值
            this.loadEventDictList()
        })

    },
    /**
     * 加载配置数据
     */
    loadConfig() {
        const self = this

        return new Promise((resovle, reject) => {
            const param = {
                street: GlobalData.street
            }
            // OU.api('sjj_jd_hqtzpz', param).then(res => {
            //     (res || []).forEach(item => {
            //         switch (item.code) {
            //             case 'code_fxyj': //风险预警
            //                 MainLeftFxyjData.fxyjList = JSON.parse(item.config) || []
            //                 break
            //             case 'code_zbtx': //值班体系
            //                 MainLeftZbtxData.zbtxList = JSON.parse(item.config) || []
            //                 break
            //             case 'code_ggaq': //公共安全
            //                 MainLeftGgaqData.tzzbList = JSON.parse(item.config) || []
            //                 break
            //             case 'code_aqsjtj_cd': //安全事件统计菜单
            //                 MainLeftAqsjData.aqsjTabList = JSON.parse(item.config) || []
            //                 break
            //             case 'code_aqsjtbtj': //安全事件统计
            //                 MainLeftAqsjData.barConfig = JSON.parse(item.config) || {}
            //                 break
            //             case 'code_gggl': //公共管理
            //                 MainRightGgglData.ggglList = JSON.parse(item.config) || []
            //                 break
            //             case 'code_ggfw': //公共服务
            //                 MainRightGgfwData.ggfwList = JSON.parse(item.config) || []
            //                 break
            //             case 'code_ggfw_12345rdwt': //12345热点问题
            //                 MainRightGgfwData.rdwt12345Cinfig = JSON.parse(item.config) || {}
            //                 break
            //             case 'code_ggfw_ywtbpz': //一网通办配置
            //                 MainRightGgfwData.ywtbConfig = JSON.parse(item.config) || {}
            //                 break
            //         }
            //     })

            //     resovle()
            // })
        })
    },
    //加载街道配置
    loadJdConfig() {
        const self = this

        // return new Promise((resovle, reject) => {
        //     OU.api('sjj_jd_jdpz', {}).then(res => {
        //         (res || []).forEach(item => {
        //             const data = GlobalData.streetConfig[item.name] || {}
        //             data.code = item.areaCode
        //             data.center = [item.zxdjd, item.zxdwd]
        //             data.zoom = item.mapZoom || 4
        //             data.mapAreaCode = item.mapAreaCode
        //             GlobalData.streetConfig[item.name] = data
        //         })
        //         resovle()
        //     })
        // })
    },
    /**
    * 加载数据
    */
    loadData() {
        const self = this
        //加载天气数据
        self.loadTq()
        //加载风险预警
        self.loadFxyj()
        //加载值班体系
        self.loadZbtx()
        //加载公共安全
        self.loadGgaq()
        //加载公共管理
        self.loadGggl()
        //加载公共服务
        self.loadGgfw()
    },
    //加载天气数据
    loadTq() {
        const param = { street: GlobalData.street }
        // sjj_1orgre6rh00_sytq
        // sjj_jd_sy_tqsj
        // OU.api('sjj_1orgre6rh00_sytq', param).then(res => {
        //     if (!res || res.length <= 0) {
        //         return
        //     }
        //     const data = res[0] || {}

        //     //天气
        //     MainTopData.weatherList.forEach(i => {
        //         if (i.valueField && data[i.valueField] != undefined) {
        //             i.value = data[i.valueField]
        //         }

        //         if (i.nameField && data[i.nameField] != undefined) {
        //             i.name = data[i.nameField]
        //         }
        //     })
        // })
    },
    //加载风险预警
    loadFxyj() {
        const self = this
        const param = { street: GlobalData.street }

        // OU.api('sjj_jd_sy_fxyj', param).then(res => {

        //     if (!res || res.length <= 0) {
        //         return
        //     }
        //     const data = {}
        //     res.forEach(i => {
        //         const key = `${i.indexName}_${i.subitem1}`
        //         data[key] = i.num
        //     })

        //     MainLeftFxyjData.fxyjList.forEach(item => {
        //         if (!item.list) {
        //             return
        //         }
        //         item.list.forEach(i => {
        //             if (!i.valueField || data[i.valueField] === undefined) {
        //                 return
        //             }
        //             i.value = data[i.valueField]
        //         })
        //     })
        // })
    },
    /**
     * 加载值班体系
     */
    loadZbtx() {
        const self = this
        const param = { street: GlobalData.street }

        // OU.api('sjj_jd_sy_zbtx', param).then(res => {

        //     if (!res || res.length <= 0) {
        //         return
        //     }
        //     const data = {}
        //     res.forEach(i => {
        //         if (!data[i.indexName]) {
        //             data[i.indexName] = {name: i.num, tel: i.tel}
        //         } else if (Array.isArray(data[i.indexName])) {
        //             data[i.indexName].push({name: i.num, tel: i.tel})
        //         } else {
        //             data[i.indexName] = [data[i.indexName], {name: i.num, tel: i.tel}]
        //         }
        //     })
        //     //值班体系
        //     MainLeftZbtxData.zbtxList.forEach(i => {
        //         if (!i.nameField || !data[i.nameField]) {
        //             return
        //         }
        //         if (i.nameType === 'Array') {
        //             i.name = Array.isArray(data[i.nameField]) ? data[i.nameField].map(i => i.name) : [data[i.nameField].name]
        //             i.tel = Array.isArray(data[i.tel]) ? data[i.nameField].map(i => i.tel) : [data[i.nameField].tel]
        //             i.tel = i.tel.filter(i => i)
        //         } else {
        //             if (Array.isArray(data[i.nameField])) {
        //                 i.name = data[i.nameField].map(i => i.name).join('、')
        //                 i.tel = data[i.nameField].map(i => i.tel).join('、')
        //             } else {
        //                 i.name = data[i.nameField].name
        //                 i.tel = data[i.nameField].tel
        //             }
        //         }
        //     })            
        // })
    },
    /**
     * 加载公共安全
     */
    loadGgaq() {
        const self = this
        const param = { street: GlobalData.street }

        // OU.api('sjj_jd_sy_ggaq', param).then(res => {

        //     if (!res || res.length <= 0) {
        //         return
        //     }
        //     const data = {}
        //     res.forEach(i => {
        //         const key = `${i.indexName}_${i.subitem1}`
        //         data[key] = i.num
        //     })
        //     //     //公共安全
        //     MainLeftGgaqData.tzzbList.forEach(i => {
        //         if (!i.list || !Array.isArray(i.list) || i.list.length <= 0) {
        //             return
        //         }
        //         i.list.forEach(t => {
        //             if (t.valueField && data[t.valueField] != undefined) {
        //                 t.value = data[t.valueField]
        //             }

        //             if (t.unitField && data[t.unitField] != undefined) {
        //                 t.unit = data[t.unitField]
        //             }
        //         })
        //     })
        // })
    },
    /**
     * 加载公共管理
     */
    loadGggl() {
        const self = this
        const param = { street: GlobalData.street }

        // OU.api('sjj_jd_sy_gggl', param).then(res => {

        //     if (!res || res.length <= 0) {
        //         return
        //     }
        //     const data = {}
        //     res.forEach(i => {
        //         const keyList = []

        //         if (i.indexName) {
        //             keyList.push(i.indexName)
        //         }

        //         if (i.subitem1) {
        //             keyList.push(i.subitem1)
        //         }

        //         if (i.subitem2) {
        //             keyList.push(i.subitem2)
        //         }

        //         const key = keyList.join('_')
        //         data[key] = i.num
        //     })
        //     //公共管理
        //     MainRightGgglData.ggglList.forEach(i => {
        //         if (!i.list || !Array.isArray(i.list) || i.list.length <= 0) {
        //             return
        //         }
        //         i.list.forEach(t => {
        //             if (t.valueField && data[t.valueField] != undefined) {
        //                 t.value = data[t.valueField]
        //             }

        //             if (t.unitField && data[t.unitField] != undefined) {
        //                 t.unit = data[t.unitField]
        //             }
        //             if (!t.valueList || !Array.isArray(t.valueList) || t.valueList.length <= 0) {
        //                 return
        //             }
        //             t.valueList.forEach(j => {
        //                 if (j.valueField && data[j.valueField] != undefined) {
        //                     j.value = data[j.valueField]
        //                 }

        //                 if (j.unitField && data[j.unitField] != undefined) {
        //                     j.unit = data[j.unitField]
        //                 }
        //             })
        //         })
        //     })
        // })
    },
    /**
     * 加载公共服务
     */
    loadGgfw() {
        const self = this
        const param = { street: GlobalData.street }

        // OU.api('sjj_jd_sy_ggfw', param).then(res => {

        //     if (!res || res.length <= 0) {
        //         return
        //     }
        //     const data = {}
        //     res.forEach(i => {
        //         const keyList = []

        //         if (i.indexName) {
        //             keyList.push(i.indexName)
        //         }

        //         if (i.subitem1) {
        //             keyList.push(i.subitem1)
        //         }

        //         if (i.subitem2) {
        //             keyList.push(i.subitem2)
        //         }

        //         const key = keyList.join('_')
        //         data[key] = i.num
        //     })
        //     //公共服务
        //     MainRightGgfwData.ggfwList.forEach(i => {
        //         if (!i.list || !Array.isArray(i.list) || i.list.length <= 0) {
        //             return
        //         }
        //         i.list.forEach(t => {
        //             if (t.valueField && data[t.valueField] != undefined) {
        //                 t.value = data[t.valueField]
        //             }

        //             if (t.unitField && data[t.unitField] != undefined) {
        //                 t.unit = data[t.unitField]
        //             }
        //         })
        //     })
        // })
    },

    /**
     * 加载城市资源
     */
    loadMapCszy() {
        const self = this
        const param = { street: GlobalData.street }
        // OU.api('sjj_jd_cszylb', param).then(res => {
        //     const cszyList = (res || [])
        //     cszyList.forEach(i => {
        //         i.selected = false
        //         i.open = false
        //     })
        //     MapSourceData.cszyList = cszyList
        // })
    },
    // /**
    //  * 加载热点舆情数据
    //  */
    // loadRdyq() {
    //     const self = this
    //     const param = { street: GlobalData.street }
    //     OU.api('sjj_rdyqlb', param).then(res => {
    //         MainLeftRdyqData.rdyqList = res || []
    //     })
    // },
    // /**
    //  * 加载安全事件统计
    //  */
    // loadAqsjtj() {
    //     const self = this
    //     const param = { street: GlobalData.street }
    //     OU.api('sjj_aqsjtj', param).then(res => {
    //         const barData = {}

    //         res.forEach(i => {
    //             const list = barData[i.type] || []
    //             list.push(i)
    //             barData[i.type] = list
    //         })
    //         MainLeftAqsjData.barData = barData
    //     })
    // },
    // /**
    //  * 加载12345热点问题-本月热点诉求统计
    //  */
    // load12345rdwtByrdsqtj() {
    //     const self = this
    //     const param = { street: GlobalData.street }
    //     OU.api('sjj_12345rdwt_byrdsqtj', param).then(res => {
    //         MainRightGgfwData.rdwt12345Data = res || []
    //     })
    // },
    // /**
    //  * 一网通办-近6月办件情况统计
    //  */
    // loadYwtbJ6ybjqktj() {
    //     const self = this
    //     const param = { street: GlobalData.street }
    //     OU.api('sjj_ywtb_j6ybjqktj', param).then(res => {
    //         MainRightGgfwData.ywtbData = res || []
    //     })
    // },
    /**
     * 加载外部系统接入
     */
    loadWbxtjr() {
        const self = this
        const param = { street: GlobalData.street }
        // OU.api('sjj_jd_wbxtjrpz', param).then(res => {
        //     const menuList = []

        //     res.forEach(item => {
        //         let positionInfo = menuList.find(i => i.code === item.position)
        //         let isAdd = false
        //         if (!positionInfo) {
        //             positionInfo = {}
        //             isAdd = true
        //         }
        //         positionInfo.code = item.position
        //         positionInfo.name = item.positionName
        //         const list = positionInfo.list || []
        //         list.push(item)
        //         positionInfo.list = list

        //         if (isAdd) {
        //             menuList.push(positionInfo)
        //         }
        //     })
        //     MainTopData.menuList = menuList
        // })
    },
    /**
     * 获取事件参数
     */
    getEventParam(type) {
        switch (type) {
            case 'xtsj': // 
                return {
                    "eventSrcCode": [
                        "16513087312to9I8X2D6QOTOht5hmH"
                    ]
                }
        }

    },
    /**
     * 加载事件列表
     */
    loadEventList(append) {
        const self = this

        MainEventData.tabList.forEach(i => {
            i.list = []
            const param = {
                pageSize: 20,
                currentPage: 1,
                startTime: dayjs().format('YYYY-01-01 00:00:00'),
                endTime: dayjs().format('YYYY-MM-DD 23:59:59'),
                label: '',
                districtCode: GlobalData.streetConfig[GlobalData.street].code
            }

            switch (i.id) {
                case 'EVENT': //重点工单, 查询重点工单相关的事件，这个条件必填 eventTypeCode: ["collaborative"]，这样返回的就是重点工单，并且条件搜索需要禁用事件类型条件，不能人为修改，与其他条件搜索正常组合
                    param.eventTypeCode = ['collaborative']
                    break
                case 'xtsj': //协同事件,查询协同事件 distributeDeptCode传固定值，每次必传，eventSrcCode传固定值 每次必传，其他查询条件任意组合 ，需要禁用事件来源的查询条件，不可人为修改  
                    param.eventSrcCode = ['16513087312to9I8X2D6QOTOht5hmH']  //市城运中心
                    param.distributeDeptCode = '202210201729431032707199748681728'//市平台
                    delete param.eventSrcCode
                    break
            }

            // OU.api('sjj_jd_sjzs_zhcx', param).then(res => {
            //     console.log("事件列表", i.id, param, res)
            //     const data = res.data || {}
            //     i.list = data.records || []
            // })
        })
    },
    /**
     * 初始事件筛选值
     */
    loadEventDictList() {

        MainEventListData.filterConfigList.forEach(item => {
            if (!item.dataSetCode) {
                return
            }
            // OU.api(item.dataSetCode, {}).then(res => {
            //     switch (item.field) {
            //         case 'state': //状态', widgetType: 'select', type: 'fixed', options: [], dataSetCode: 'sjj_sjzs_hqsjztlb'},
            //             item.options = (res.data || []).map(i => {
            //                 return { id: i.code, name: i.name }
            //             })
            //             break
            //         case 'eventLevelCode': //紧急程度', widgetType: 'select', type: 'fixed', options: [], dataSetCode: 'sjj_sjzs_hqjjcdlb'},
            //             item.options = (res.data || []).map(i => {
            //                 return { id: i.code, name: i.name }
            //             })
            //             break
            //         // case 'eventTypeCode': //事件类型', widgetType: 'select', type: 'more', options: [], dataSetCode: 'sjj_sjzs_hqsjlxlb'},
            //         //     item.options = (res.data || []).map(i => {
            //         //         return { id: i.code, name: i.name }
            //         //     })
            //         //     break
            //         case 'smallTypeCode': //问题类型', widgetType: 'select', type: 'more', options: [], dataSetCode: 'sjj_sjzs_hqwtlblb'},
            //             item.options = res.data || []
            //             GlobalTool.dealTree(item.options)
            //             break
            //         case 'eventSrcCode': //事件来源', widgetType: 'select', type: 'more', options: [], dataSetCode: 'sjj_sjzs_hqsjly_qdlb'},
            //             item.options = (res.data || []).map(i => {
            //                 return { id: i.code, name: i.name }
            //             })
            //             break
            //         case 'deptCode': //处置单位', widgetType: 'select', type: 'more', options: [], dataSetCode: 'sjj_sjzs_hqzzjgsxjg'},
            //             //TODO
            //             break
            //         case 'districtCode': //区域', widgetType: 'select', type: 'more', options: [], dataSetCode: 'sjj_sjzs_hqhzqysxjg'},
            //             item.options = res.data || []
            //             GlobalTool.dealTree(item.options)
            //             break
            //     }
            // })
        })
    },
    /**
     * 获取事件中心的数据
     */
    loadSjzx(beginTime, endTime) {
        const self = this
        const param = {
            beginTime,
            endTime,
            streetCode: GlobalData.streetConfig[GlobalData.street].code
        }
        streetEventSummary(param).then(res => {
            const data = res.data || {}
            data.total = Number(data.finnishCount || 0) + Number(data.handlingCount || 0) + Number(data.waitSignCount || 0)
            MainLeftSjzxData.configData.sjglConfig.forEach(i => {
                if (i.valueField) {
                    i.value = data[i.valueField] !== undefined ? data[i.valueField] : '-'
                }
            })
        })
        // OU.api('sjj_jd_sjzs_sjjdtj', param).then(res => {
        //     const data = res.data || {}
        //     data.total = Number(data.finnishCount || 0) + Number(data.handlingCount || 0) + Number(data.waitSignCount || 0)
        //     MainLeftSjzxData.configData.sjglConfig.forEach(i => {
        //         if (i.valueField) {
        //             i.value = data[i.valueField] !== undefined ? data[i.valueField] : '-'
        //         }
        //     })
        // })
    },
    /**
     * 事件中心-办件趋势
     */
    loadSjzxBjqs(beginTime, endTime) {
        const self = this
        const param = {
            beginTime,
            endTime,
            streetCode: GlobalData.streetConfig[GlobalData.street].code
        }
        selectStreetEventTrend(param).then(res => {
            const data = res.data || []
            const map = {}

            data.forEach(i => {
                map[i.month] = Number(i.num)
            })
            const list = []

            for (let i = 1; i <= 12; i++) {
                list.push({
                    type: '办件趋势',
                    name: `${i}月`,
                    value: map[i] || 0
                })
            }
            MainLeftSjzxData.configData.bjqsData = list
        })
    },
    /**
    * 事件中心-分类占比
    */
    loadSjzxFlzb(beginTime, endTime) {
        const self = this
        const param = {
            beginTime,
            endTime,
            streetCode: GlobalData.streetConfig[GlobalData.street].code
        }
        streetTypeRatio(param).then(res => {
            const data = res.data || []
            const list = data.map(i => {
                return {
                    name: i.statName,
                    value: Number(i.num)
                }
            })
            MainLeftSjzxData.configData.flzbData = list
        })
    }
}
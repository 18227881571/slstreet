import Vue from 'vue'
import App from './app.vue'
import * as echarts from 'echarts'
import dayjs from 'dayjs'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import * as md5 from './md5'
import "./globalless.less"
import _ from 'lodash-es'
// 引入arcgis config 
import esriConfig from '@arcgis/core/config'
import './map.css'
esriConfig.assetsPath = "./assets"
// 全局函数
window.echarts = echarts
window.dayjs = dayjs
window.md5 = md5
window._ = _
console.log('lodash引入',_)
Vue.config.productionTip = false
Vue.use(ElementUI)
Vue.prototype.$echarts = echarts
Vue.prototype.$dayjs = dayjs

new Vue({
  render: h => h(App)
}).$mount('#contain')

import request from "./index";
import mysql from 'mysql';
import { reject } from "lodash-es";
import { AxiosError } from "axios";
// export const getEventList = (data) => {
//   return request({
//     method: "post",
//     url: "/api/eventCenter/recordInfo/v1/pageEventRecord",
//     data,
//   });
// };

export const queryMainEventProcessInfo = (data) => {
  return request({
    method: "post",
    url: "/api/eventCenter/recordInfo/v1/queryMainEventProcessInfo",
    data,
  });
};

export const queryHandleEventProcessInfo = (data) => {
  return request({
    method: "post",
    url: "/api/eventCenter/recordInfo/v1/queryHandleEventProcessInfo",
    data,
  });
};

export const getSmallQuestionMsg = (data) => {
  return request({
    method: "get",
    url: "/api/eventCenter/duty/v1/getSmallQuestionMsg",
    params: data,
  });
};
export const listEventMedia = (data) => {
  return request({
    method: "post",
    url: "/api/eventCenter/media/v1/listEventMedia",
    data,
  });
};
export const selectEventRecordInfo = (data) => {
  return request({
    method: "post",
    url: "/api/eventCenter/recordInfo/v1/selectEventRecordInfo",
    data,
  });
};
export const selectStreetEventTrend = (data)=>{
  return request({
    method: "post",
    url: "/api/eventcenter-extend-mobile/mobileQuery/v1/countByYearMonth",
    data,
  });
}
export const streetTypeRatio = (data)=>{
  return request({
    method: "post",
    url: "/api/eventcenter-extend-mobile/mobileQuery/v1/countByTopType",
    data,
  });
}
export const streetEventSummary = (data)=>{
  return request({
    method: "post",
    url: "/api/eventcenter-extend-mobile/mobileQuery/v1/townStreetEventStateCount",
    data,
  });
}

// 顶部数据
export const topSummary = (data)=>{
  return request({
    method: "get",
    url: "/api/eventcenter-extend-mobile/screenQuery/v1/eventTotalCount",
    data,
  });
}
export const topInProcessSummary = (data)=>{
  return request({
    method: "get",
    url: "/api/eventcenter-extend-mobile/screenQuery/v1/eventMainTypeStat",
    data,
  });
}
export const cityDivided = (data)=>{
  return request({
    method: "get",
    url: "/api/eventcenter-extend-mobile/screenQuery/v1/cityEventTotalCount",
    data,
  });
}
export const cityEventStat = (data)=>{
  return request({
    method: "get",
    url: "/api/eventcenter-extend-mobile/screenQuery/v1/cityEventStat",
    data,
  });
}

// 融合指挥
export const rhzhLogin = (data)=>{
  return request({
    method: "get",
    url: "10.39.235.53/ps/data/login",
    data,
  });
}

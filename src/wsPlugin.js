export default {

    initPlugin() {
      try {
        var wsImpl = window.Websocket || window.MozWebSocket;// create a new websocket and connect
        let port = 1024
        if (port > 65535) port = 1024
        window.ws = new wsImpl("ws://localhost:" + port)
        console.log(window.ws)
        // when data is comming from the server, this metod is called 
        // when the connection is established, this method is called
        ws.onopen = function () {
          console.log("ws open")
          ws.onmessage = function (evt) {
            console.info("ws message: ", evt);
          }
        }
      } catch (error) {
        console.info('wsPlugin error ', error)
      }
    }
  }
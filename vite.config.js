import { createVuePlugin } from "vite-plugin-vue2";
export default {
  plugins: [createVuePlugin()],
  // requireTransform({
  //   fileRegex: /.js$|.vue$/,
  //   importPrefix :'_vite_plugin_require_transform_',
  // })],
  // base: "/",
  resolve: {
    extensions: [".vue", ".js"],
    // alias:{
    //   "/":"/"
    // }
  },
  css: {
    preprocessorOptions: {
      less: {
        additionalData: '@import "./src/globalless.less";',
        charset: false,
        javascriptEnabled: true,
      },
    },
  },
  server: {
    host:'127.0.0.1',
    proxy: {
      "/api": {
        target: "http://10.39.235.53:31620",
        changeOrigin: true,
        rewrite: (path) => path.replace(/^\/api/, "/api"),
      },
      "/gateway": {
        target: "https://10.1.213.3:4432",
        changeOrigin: true,
        rewrite: (path) => path.replace(/^\/gateway/, "/gateway"),
      },
      "/ps": {
        target: "http://10.39.35.53:8098",
        changeOrigin: true,
        rewrite: (path) => path.replace(/^\/ps/, "/ps"),
      },
      "/artemis": {
        target: "https://10.39.235.59:443",
        changeOrigin: true,
        rewrite: (path) => path.replace(/^\/artemis/, "/artemis"),
      },
      "/@arcgis/core": {
        target: " https://js.arcgis.com/4.24",
        changeOrigin: true,
        rewrite: (path) => path.replace(/^\/@arcgis^\/core/, "/@arcgis/core"),
      },
    },
  },
};
